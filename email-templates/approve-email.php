<?php
class ApproveEmail {
	
	public static function init() {
        $approveemail = __CLASS__;
        new $approveemail;
    }

    public function __construct() {
	   //construct what you see fit here...
    }
	 
	public function message_to_client( $message_params ){ 
		if( $message_params ){ 
			extract( $message_params ); 
			ob_start();
			?>
			<table border="0" style="width: 600px;">
				<tr>
					<td>From: </td><td><?php echo $admin_email; ?></td>
				</tr>
				<tr>
					<td>Date: </td><td><?php echo $final_date;?></td>
				</tr>
				<tr>
					<td>Subject: </td><td>Booking Approved <?php echo $admin_company_name.' '.$reference_number;?></td>
				</tr>
				<tr>
					<td>To: </td><td><?php echo $booker_email;?></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><br /><br /></td>
				</tr> 
				<tr>
					<td colspan="2" align="left"><b>BOOKING APPROVED</b></td>
				</tr>
				<tr>
					<td colspan="2">Thanks for booking with <?php echo $admin_company_name;?> Below you will find your booking details:</td>
				</tr>
				<tr>
					<td>Booking reference: </td><td><?php echo $reference_number;?></td>
				</tr>
				<tr>
					<td>Agent/User Name: </td><td><?php echo $agency_reference;?></td>
				</tr>
				<tr>
					<td>First Name: </td><td><?php echo $booker_name;?></td>
				</tr>
				<tr>
					<td>Contact No: </td><td><?php echo $booker_contact_no;?></td>
				</tr>
				<tr>
					<td>Email: </td><td><?php echo $booker_email;?></td>
				</tr>
				<tr>
					<td>Country: </td><td><?php echo $booker_country;?></td>
				</tr>
				<tr>
					<td>Service description: </td><td><?php echo $service_description;?></td>
				</tr>
				<tr>
					<td>Occupancy: </td><td><?php echo $summary_occupancy;?></td>
				<tr>
					<td>Booking dates: </td><td><?php echo $check_in.' - '.$check_out;?></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr> 
				<tr>
					<td colspan="2" align="left"><b>CANCELLATION CHARGES</b></td>
				</tr> 
				<tr>
					<td colspan="2"><?php echo $service_description.' from '.$cancellation_date;?> the cancellation charges may apply.</td>
				</tr>
				<tr>
					<td colspan="2">Date and time is calculated based on local time of destination.</td>
				</tr>
				<tr>
					<td colspan="2">Please do not reply to this email. This is an automated message. If you want to check or modify your booking, please visit our website. If you have any questions, please do not hesitate to contact us.</td>
				</tr> 
				<?php
				if( $admin_company_name ){
					?>
					<tr>
						<td colspan="2"><br /><br /></td>
					</tr> 
					<tr>
						<td colspan="2" align="left"><b><?php echo $admin_company_name;?> INFORMATION</b></td>
					</tr>
					<tr>
						<td>Address: </td><td><?php echo $admin_address.', '.$admin_postcode;?></td>
					</tr> 
					<tr>
						<td>Country: </td><td><?php echo $admin_country;?></td>
					</tr> 
					<tr>
						<td>Email: </td><td><?php echo $admin_email;?></td>
					</tr> 
					<tr>
						<td>Telephone: </td><td><?php echo $admin_phone_number;?></td>
					</tr> 
					<tr>
						<td>Fax Number: </td><td><?php echo $admin_fax_number;?></td>
					</tr> 
					<tr>
						<td>Website: </td><td><?php echo $admin_website;?></td>
					</tr> 
					<?php
				}	
				?>
			</table>
			<?php
			$message = ob_get_contents();
			ob_end_clean();
			
			return $message;
		}
	}
	 
	public function message_to_admin( $message_params ){ 
		if( $message_params ){ 
			extract( $message_params ); 
			ob_start();
			?>
			<table border="0" style="width: 600px;">
				<tr>
					<td>From: </td><td><?php echo $admin_email; ?></td>
				</tr>
				<tr>
					<td>Date: </td><td><?php echo $final_date;?></td>
				</tr>
				<tr>
					<td>Subject: </td><td>Booking Approved <?php echo $admin_company_name.' '.$reference_number;?></td>
				</tr>
				<tr>
					<td>To: </td><td><?php echo $booker_email;?></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><br /><br /></td>
				</tr> 
				<tr>
					<td colspan="2" align="left"><b>BOOKING APPROVED</b></td>
				</tr>
				<tr>
					<td colspan="2">Thanks for booking with <?php echo $admin_company_name;?> Below you will find your booking details:</td>
				</tr>
				<tr>
					<td>Booking reference: </td><td><?php echo $reference_number;?></td>
				</tr>
				<tr>
					<td>Agent/User Name: </td><td><?php echo $agency_reference;?></td>
				</tr>
				<tr>
					<td>First Name: </td><td><?php echo $booker_name;?></td>
				</tr>
				<tr>
					<td>Contact No: </td><td><?php echo $booker_contact_no;?></td>
				</tr>
				<tr>
					<td>Email: </td><td><?php echo $booker_email;?></td>
				</tr>
				<tr>
					<td>Country: </td><td><?php echo $booker_country;?></td>
				</tr>
				<tr>
					<td>Service description: </td><td><?php echo $service_description;?></td>
				</tr>
				<tr>
					<td>Occupancy: </td><td><?php echo $summary_occupancy;?></td>
				<tr>
					<td>Booking dates: </td><td><?php echo $check_in.' - '.$check_out;?></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr> 
				<tr>
					<td colspan="2" align="left"><b>CANCELLATION CHARGES</b></td>
				</tr> 
				<tr>
					<td colspan="2"><?php echo $service_description.' from '.$cancellation_date;?> the cancellation charges may apply.</td>
				</tr>
				<tr>
					<td colspan="2">Date and time is calculated based on local time of destination.</td>
				</tr>
				<tr>
					<td colspan="2">Please do not reply to this email. This is an automated message. If you want to check or modify your booking, please visit our website. If you have any questions, please do not hesitate to contact us.</td>
				</tr> 
				<?php
				if( $admin_company_name ){
					?>
					<tr>
						<td colspan="2"><br /><br /></td>
					</tr> 
					<tr>
						<td colspan="2" align="left"><b><?php echo $admin_company_name;?> INFORMATION</b></td>
					</tr>
					<tr>
						<td>Address: </td><td><?php echo $admin_address.', '.$admin_postcode;?></td>
					</tr> 
					<tr>
						<td>Country: </td><td><?php echo $admin_country;?></td>
					</tr> 
					<tr>
						<td>Email: </td><td><?php echo $admin_email;?></td>
					</tr> 
					<tr>
						<td>Telephone: </td><td><?php echo $admin_phone_number;?></td>
					</tr> 
					<tr>
						<td>Fax Number: </td><td><?php echo $admin_fax_number;?></td>
					</tr> 
					<tr>
						<td>Website: </td><td><?php echo $admin_website;?></td>
					</tr> 
					<?php
				}	
				?>
			</table>
			<?php
			$message = ob_get_contents();
			ob_end_clean();
			
			return $message;
		}
	}
}