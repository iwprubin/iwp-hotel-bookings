<?php
class PDFAttachment {
	
	public static function init(){
        $pdfattachment = __CLASS__;
        new $pdfattachment;
    }
 
    public function __construct(){
	   //construct what you see fit here...
    }
	 
	public function voucher_template( $voucher_params ){
		if( $voucher_params ){
			extract( $voucher_params ); 
			ob_start();	
			?>
			<html lang="en">
				<head>
				</head> 
				<body style="font-family: Arial; font-size: 14px;"> 
				<div style="width:100%;float:left;border:1px solid #CCC;">
					<h1 style="text-align:center;margin:0 0 20px 0;padding:5px 0;background-color:#6A050D;color:#FFF;">Voucher - Accommodation</h1>
					<table cellpadding="3" cellspacing="0" style="width:99%;float:left;height:90px;border:0;">
						<tr>
							<td width="50%" style="border-right:1px solid #CCC;"> 
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr> 
										<td style="text-align:center;">
											<p>Booking confirmed and guaranteed - Voucher - Hotel</p> 
										</td>
									</tr>
								</table> 
							</td>
							<td width="50%">
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr>
										<td style="text-align:right;">
											<img src="<?php echo $logo_right; ?>" alt="<?php echo $logo_right; ?>" style="width:128px;height:50px;" />
										</td> 
									</tr>
									<tr>
										<td style="text-align:right;">
											<p>Guaranteed by</p> 
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<br /><br />
				<div style="width:100%;float:left;border:1px solid #CCC;">
					<h3 style="width:96%;float:left;margin:0 0 20px 0;padding:3px 2%;background-color:#6A050D;color:#FFF;">Booking Details</h3>
					<table cellpadding="3" cellspacing="0" style="width:100%;float:left;max-height:90px"> 
						<tr>
							<td width="40%" style="background-color:#9FA2A2;"> 
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr>
										<td style="text-align:center;margin-bottom:0;line-height:8px;">Reference number:</td>  
									</tr>
									<tr >
										<td style="text-align:center;"><h1 style="line-height:8px;margin-top:0;margin-bottom:30px;"><?php echo $reference_number; ?></h1></td> 
									</tr>
									<tr>
										<td style="text-align:center;">Valid for the Hotel</td> 
									</tr>
								</table> 
							</td>
							<td width="60%"style="background-color:#C5C9C9;"> 
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr>
										<td style="text-align:left;"><strong><?php echo $hotel_name;?>&nbsp;&nbsp;<?php echo $hotel_category; ?></strong></td> 
									</tr>
									<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
										<tr>
											<td style="text-align:left;">Pax name:</td>  
											<td style="text-align:left;"><?php echo $pax_name; ?></td>  
										</tr> 
										<tr>
											<td style="text-align:left;">Booking date:</td>  
											<td style="text-align:left;"><?php echo $booking_date; ?></td>  
										</tr> 
										<tr>
											<td style="text-align:left;">Agent/User Name:</td>  
											<td style="text-align:left;"><?php echo $agency_ref; ?></td>  
										</tr> 
									</table>
								</table>
							</td>
						</tr> 	 
					</table>
				</div> 
				<br /><br />
				<div style="width:100%;float:left; #CCC;">
					<h2 style=" text-align:center;margin:0 0 20px 0;padding:3px 0;background-color:#6A050D;color:#FFF;">Services</h2>
					<br /><br />
					<div style="width:100%;float:left;height:210px;border:1px solid #CCC;">
						<h3 style="width:100%;float:left;margin:0 0 20px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Accommodation</h3>
						<table cellpadding="3" cellspacing="0" style="width:100%;float:left;"> 
							<tr>
								<td style="width:15%;"><b>From: </b><</td>
								<td style="width:35%;"><?php echo $check_in; ?></td> 
								<td style="width:20%;"><b>To: </b></td>
								<td style="width:30%;"><?php echo $check_out; ?></td>
							</tr>
						</table>
						<table cellpadding="3" cellspacing="0" style="width:100%;float:left;"> 
							<tr>
								<th width="7%" style="border-bottom: 1px dashed #ccc;">Units</th>
								<th width="35%" style="border-bottom: 1px dashed #ccc;text-align:center;">Room Type</th>
								<th width="10%" style="border-bottom: 1px dashed #ccc;text-align:center;">Adults</th>
								<th width="8%" style="border-bottom: 1px dashed #ccc;text-align:center;">Children</th>
								<th width="15%" style="border-bottom: 1px dashed #ccc;text-align:center;">Children Ages</th> 
								<th width="25%" style="border-bottom: 1px dashed #ccc;text-align:center;">Board</th>
							</tr>
							<?php  
							if( $hotel_availableroom ){
								 echo $hotel_availableroom;
							}
							?>
							<!--<tr>
								<td style="border-bottom: 1px dashed #ccc;"><?php echo $units; ?></td>
								<td style="border-bottom: 1px dashed #ccc;"><?php echo $room_type; ?></td>
								<td style="border-bottom: 1px dashed #ccc;text-align:center;"><?php echo $adults; ?></td>
								<td style="border-bottom: 1px dashed #ccc;text-align:center;"><?php echo $children; ?></td> 
								<td style="border-bottom: 1px dashed #ccc;text-align:center;"><?php echo $children_ages; ?></td> 
								<td style="border-bottom: 1px dashed #ccc;"><?php echo $board; ?></td>  
							</tr>-->
							<tr>
								<td colspan="6">
									<h5 style="margin:5px 0 0 0;">Remarks</h5>
									<p style="text-align:justify;font-size:11px;margin:0;">Any name change, adding sharer will not be accepted after booking is confirmed..<p>
								</td>
							</tr>  
						</table> 
					</div>
				</div> 
				<br /><br />
				<div style="width:100%;float:left;border:1px solid #CCC;">
					<h3 style="width:96%;float:left;margin:0 0 20px 0;padding:3px 2%;background-color:#6A050D;color:#FFF;">Hotel Details</h3>
					<table cellpadding="3" cellspacing="0" style="width:100%;float:left;"> 
						<tr>
							<td width="50%"> 
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr> 
										<td style="text-align:center;"><?php echo $google_map;?></td>     
									</tr> 
								</table>
							</td>
							<td width="50%"> 
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr>
										<td  colspan="2" style="text-align:left;"><strong><?php echo $hotel_name?>&nbsp;&nbsp;<?php echo $hotel_category; ?></strong></td> 
									</tr> 
									<tr>
										<td style="text-align:left; " valign="top";>Address:</td>  
										<td style="text-align:left;"><?php echo $hotel_address; ?></td>  
									</tr> 
									<?php
									if( $hotel_telephone ){
										?>
										<tr>
											<td style="text-align:left;">Telephone:</td>  
											<td style="text-align:left;"><?php echo $hotel_telephone; ?></td>  
										</tr> 
										<?php
									}
									if( $hotel_fax ){
										?>	
										<tr>
											<td style="text-align:left;">Fax:</td>  
											<td style="text-align:left;"><?php echo $hotel_fax; ?></td>  
										</tr> 
										<?php
									}
									if( $hotel_email ){
										?>	
										<tr>
											<td style="text-align:left;">Email:</td>  
											<td style="text-align:left;"><?php echo $hotel_email; ?></td>  
										</tr> 
										<?php
									}
									if( $service_ref ){
										?>	
										<tr>
											<td style="text-align:left;">Ref:</td>  
											<td style="text-align:left;"><?php echo $service_ref; ?></td>  
										</tr> 
										<?php
									}
									?>	
								</table>
							</td>
						</tr>  
					</table>
				</div> 
				<br /><br />
				<div style="width:100%;float:left;height:90px;border:1px solid #CCC;">
					<h3 style="width:96%;float:left;margin:0 0 20px 0;padding:3px 2%;background-color:#6A050D;color:#FFF;">Telephone</h3>
					<table cellpadding="3" cellspacing="0" style="width:100%;float:left;"> 
						<tr>
							<td width="10%"> 
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr>
										<td style="text-align:center;"><img src="http://cribsandtrips.com/wp-content/uploads/2016/02/phone-icon-orange.jpg" width="50px" height="50px"></td>  
									</tr> 
								</table> 
							</td>
							<td width="90%" > 
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr>
										<td style="text-align:left;">Emergency number</td> 
									</tr> 	
									<tr>
										<td style="text-align:left;"><?php echo $admin_emergency_number; ?></td> 
									</tr> 
								</table>
							</td>
						</tr>  
					</table>
				</div>
				<br />
				<div style="width:100%;float:left;border:1px solid #CCC;">
					<!--<h5 style="width:96%;float:left;margin:0 0 20px 0;padding:3px 2%;text-align:center;">Bookable and Payable by J-Leisure</h5>--> 
					<h5 style="width:96%;float:left;margin:0 0 20px 0;padding:3px 2%;text-align:center;"><?php echo $bookable_details; ?></h5>
					 
				</div> 
				</body>
			</html>
			<?php
			$html = ob_get_contents();
			ob_end_clean();
			
			return $html;
		}
	}
	
	public function receipt_template( $receipt_params ){
		
		if( $receipt_params ){
			extract( $receipt_params ); 
			ob_start();	 
			?>
			<html lang="en">
				<head>
				</head>

				<body style="font-family: Arial; font-size: 14px;">
				<div style="width:100%;float:left;border:1px solid #CCC;">
					<h1 style="text-align:center;margin:0 0 20px 0;padding:5px 0;background-color:#6A050D;color:#FFF;">Receipt</h1>
					<table cellpadding="3" cellspacing="0" style="width:99%;float:left;height:120px;border:0;">
						<tr>
							<td width="50%" style="border-right:1px solid #CCC;">
								<?php if(!empty($logo_left)){ ?>
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr>
										<td width="100%" valign="top">
											<img src="<?php echo $logo_left; ?>" alt="<?php echo $logo_left; ?>" style= "width: 320; height: 100;float:left" />
										</td>
									</tr>
								</table>
								<?php } ?> 
							</td>
							<td width="50%">
								<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
									<tr>
										<td width="100%" valign="top">
											<p><?php echo $logo_right_name; ?></p>
											<address><?php echo $logo_right_address;?></address>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<br /><br />
				<h3 style="width:96%;float:left;margin:0 0 20px 0;padding:3px 2%;background-color:#6A050D;color:#FFF;">Receipt No: <?php echo $receipt_no; ?></h3>
				<div style="width:100%;float:left;border:1px solid #CCC;">
					<h3 style="width:96%;float:left;margin:0 0 20px 0;padding:3px 2%;background-color:#6A050D;color:#FFF;">Booking Details</h3>
					<table cellpadding="3" cellspacing="0" style="width:100%;float:left;">
					<?php if(!empty($booking_reference)){ ?>
						<tr>
							<td style="width:20%;"><b>Reference:</b></td>
							<td style="width:30%;"><?php echo $booking_reference; ?></td>
							<td style="width:20%;"><b>Confirmation Date:</b></td>
							<td style="width:30%;"><?php echo $booking_confirmation_date; ?></td>
						</tr>
						<tr>
							<td><b>Pax Name:</b></td>
							<td><?php echo $pax_name; ?></td>
							<td><b>Payment Type:</b></td>
							<td><?php echo $payment_type; ?></td>
						</tr>
						<tr>
							<td><b>Agency Ref.:</b></td>
							<td><?php echo $agency_ref; ?></td>
							<td></td>
							<td></td>
						</tr>
					<?php } ?> 
					</table>
				</div>
				<br /><br />
				<div style="width:96%;float:left;border:1px solid #CCC;padding: 10px 2%;">
					<h2 style="text-align:center;margin:0 0 20px 0;padding:3px 0;background-color:#6A050D;color:#FFF;">Services</h2>
					<br /><br />
					<div style="width:100%;float:left;border:1px solid #CCC;">
						<h3 style="width:92%;float:left;margin:0 0 20px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Accommodation</h3>
						<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
						<?php if(!empty($hotel_name)){ ?>
							<tr>
								<td colspan="4">
									<b><?php echo $hotel_name; ?></b>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<b><?php echo $hotel_address; ?></b>
								</td>
							</tr>
							<tr>
								<td style="width:20%;"><b>From: </b><</td>
								<td style="width:30%;"><?php echo $check_in; ?></td>
								<td style="width:20%;"><b>To: </b></td>
								<td style="width:30%;"><?php echo $check_out; ?></td>
							</tr>
							<tr>
								<td style="border-bottom: 1px dashed #ccc; font-weight: bold;padding-left: 4px;"><br/>Room type</td>
								<td style="border-bottom: 1px dashed #ccc; font-weight: bold;"><br/>Board</td>
								<td style="border-bottom: 1px dashed #ccc; font-weight: bold;"><br/>Occupancy</td>
								<td style="border-bottom: 1px dashed #ccc; font-weight: bold;"><br/></td>
							</tr>
							<?php  
							if( $hotel_availableroom ){
								 echo $hotel_availableroom;
							}
							?>
							<!--<tr>
								<td style="border-bottom: 1px dashed #ccc; width: 180px;"><?php echo $room_type; ?></td>
								<td style="border-bottom: 1px dashed #ccc; width: 150px;"><?php echo $board; ?></td>
								<td style="border-bottom: 1px dashed #ccc; width: 150px;"><?php echo $occupancy; ?></td>
								<td style="border-bottom: 1px dashed #ccc; width: 150px; text-align: right; padding-right: 10px;"><?php echo $price; ?></td>
							</tr>-->
							<tr>
								<td colspan="4">
									<h5 style="margin:15px 0 0 0;">Contract Remarks</h5>
									<p style="text-align:justify;font-size:11px;margin:0;"><?php echo $contract_remarks; ?><p>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<h3 style="text-align:right;margin: 0;">Total: <b><?php echo number_format( $room_price_total,2,'.',',' ); ?></b></h3>
								</td>
							</tr>
						<?php }?>
						</table>
						<?php if( $tour_package ){ ?>
							<h3 style="width:92%;float:left;margin:0 0 0px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Tickets and Excursions</h3>
							<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
							
								<tr>
									<td colspan="4">
										<b><?php echo $tour_package; ?></b>
									</td>
								</tr>
								<tr>
									<td style="width:20%;"><b>From: </b></td>
									<td style="width:30%;"><?php echo $tour_from_date; ?></td>
									<td style="width:20%;"><b>To: </b></td>
									<td style="width:30%;"><?php echo $tour_to_date; ?></td>
								</tr>
								<tr>
									<td colspan="2">
										<h4 style="margin-bottom: 0;">Tickets</h4>
									</td>
								</tr>
								<tr>
									<td colspan="3"> 
										<p style="text-align:justify; margin: 0;"><?php echo $ticket_details; ?><p>
									</td>
									<td colspan= "1"> 
										<p style="text-align:right;margin: 0;"><b><?php echo $ticket_price; ?></b><p>
									</td> 
								</tr>
								<tr>
									<td colspan="4">
										<h5 style="margin:15px 0 0 0;">Contract Remarks</h5>
										<p style="text-align:justify;font-size:11px;margin:0;"><?php echo $ticket_contract_remarks; ?><p>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<h3 style="text-align:right;margin: 0;">Total: <b><?php echo $total_ticket_price; ?></b></h3>
									</td>
								</tr> 
							</table>
						<?php }?> 
						<?php if( $arrival_hotel_type ){ ?>
							<h3 style="width:92%;float:left;margin:0 0 0px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Arrival Transfer</h3>
							<table cellpadding="3" cellspacing="0" style="width:99%;float:left;"> 
								<tr>
									<td colspan="4">
										<b><?php echo $arrival_hotel_type; ?></b>
									</td>
								</tr>
								<tr>
									<td style="width:20%;"><b>Pick-up Date: </b></td>
									<td style="width:30%;"><?php echo $pickup_date; ?></td>
									<td style="width:20%;"><b>Arrival Time: </b></td>
									<td style="width:30%;"><?php echo $arrival_time; ?></td>
								</tr>
								<tr>
									<td colspan="4">
										<h4 style="margin-bottom: 0;">Arrival Transfer</h4>
									</td>
								</tr>
								<tr>
									<td colspan="3"> 
										<p style="text-align:justify; margin: 0;"><?php echo $arrival_transfer; ?><p>
									</td>
									<td> 
										<p style="text-align:right;margin: 0;"><b><?php echo $arrival_price; ?></b><p>
									</td> 
								</tr>
								<tr>
									<td colspan="4">
										<h3 style="text-align:right;margin: 0;">Total: <b><?php echo $island_price; ?></b></h3>
									</td>
								</tr> 
							</table>
						<?php }?>
						<?php if( $departure_hotel_type ){ ?>
							<h3 style="width:92%;float:left;margin:0 0 0px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Departure Transfer</h3>
							<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
						
								<tr>
									<td colspan="4">
										<b><?php echo $departure_hotel_type; ?></b>
									</td>
								</tr>
								<tr>
									<td style="width:20%;"><b>Pick-up Date: </b></td>
									<td style="width:30%;"><?php echo $departure_pickup_date; ?></td>
									<td style="width:20%;"><b>Arrival Time: </b></td>
									<td style="width:30%;"><?php echo $departure_arrival_time; ?></td>
								</tr>
								<tr>
									<td colspan="4">
										<h4 style="margin-bottom: 0;">Departure Transfer</h4>
									</td>
								</tr>
								<tr>
									<td colspan="3"> 
										<p style="text-align:justify; margin: 0;"><?php echo $departure_arrival_transfer; ?><p>
									</td>
									<td> 
										<p style="text-align:right;margin: 0;"><b><?php echo $departure_arrival_price; ?></b><p>
									</td> 
								</tr>
								<tr>
									<td colspan="4">
										<h3 style="text-align:right;margin: 0;">Total: <?php echo $departure_island_price; ?></h3>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<h3 style="text-align:right;margin: 0;">Final price total: <?php echo $total_departure_price; ?></h3>
									</td>
								</tr> 
							</table>
						<?php }?>
					</div>
				</div>
				<br /><br />
				<?php if( $hotel_name ){ ?>
					<div style="width:100%;float:left;border:1px solid #CCC;">
						<h3 style="width:92%;float:left;margin:0 0 20px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Agency commission</h3>
						<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
						
							<tr>
								<td><b>Services</b></td>
								<td><b>Assessment</b></td>
							</tr>
							<tr>
								<td style="border-bottom: 1px dashed #ccc;"><?php echo $hotel_name; ?></td>
								<td style="border-bottom: 1px dashed #ccc;"><?php echo $price; ?></td>
							</tr> 
							<tr style= " padding-right: 10px">
								<td><b><br>Tax</b></td>
								<td><br><b><?php echo number_format( $tax_value,2,'.',',' ); ?></b></td>
							</tr>
							<tr style= " padding-right: 10px">
								<td><b><br>Total Web Admin Fee</b></td>
								<td><br><b><?php echo number_format( $web_admin_fee,2,'.',',' ); ?></b></td>
							</tr>
							<tr style= " padding-right: 10px">
								<td><b>Total Service Charge</b></td>
								<td><b><?php echo number_format( $service_charge,2,'.',',' ); ?></b></td>
							</tr>
							<tr style= " padding-right: 10px">
								<td><b>Total net amount to be charged</b></td>
								<td><b><?php echo number_format( $total_net_amount,2,'.',',' ); ?></b></td> 
							</tr>
						</table>
					</div> 
				<?php }?>
				<br /><br />
				<div style="width:100%;float:left;border:1px solid #CCC;">
					<h3 style="width:92%;float:left;margin:0 0 20px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Cancellation Charges</h3>
					<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
					<?php if( $cancellation_concept ){ ?>
						<tr>
							<td style="border-bottom: 1px dashed #ccc;"><b>Concept</b></td>
							<td style="border-bottom: 1px dashed #ccc;"><b>From</b></td>
							<td style="border-bottom: 1px dashed #ccc;"><b>Units</b></td>
							<td style="border-bottom: 1px dashed #ccc;"><b>Value</b></td>
						</tr>
						<tr>
							<td style="border-bottom: 1px dashed #ccc;"><?php echo $cancellation_concept; ?></td>
							<td style="border-bottom: 1px dashed #ccc;"><?php echo $cancellation_concept_from; ?></td>
							<td style="border-bottom: 1px dashed #ccc;"><?php echo $cancellation_units; ?></td>
							<td style="border-bottom: 1px dashed #ccc;"><?php echo $cancellation_value; ?></td>
						</tr>
						<tr>
							<td colspan="4">
								<p><b>Date and time is calculated based on local time of destination.</b></p>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<p><b>PAYMENT DEADLINE: Please ensure you pay for this booking before <?php echo $cancellation_concept_from; ?> at 19:59 PM (local time) on your booking will be cancelled automatically.</b></p>
							</td>
						</tr>
					<?php }?>
					</table>
				</div>
				<br /><br />

				</body>
			</html>
			<?php
			$html = ob_get_contents();
			ob_end_clean(); 
					
			return $html;
		}
	}
	
	public function invoice_template( $invoice_params ){
		
		if( $invoice_params ){
			extract( $invoice_params ); 
			
			ob_start();	 
			?> 
			<html lang="en">
				<head>
				</head> 
				<body style="font-family: Arial; font-size: 14px;">
					<div style="width:100%;float:left;border:1px solid #CCC;">
						<h1 style="text-align:center;margin:0 0 20px 0;padding:5px 0;background-color:#6A050D;color:#FFF;">Invoice</h1>
						<table cellpadding="3" cellspacing="0" style="width:99%;float:left;height:120px;border:0;">
							<tr>
								<td width="50%" style="border-right:1px solid #CCC;">
									<?php if(!empty($logo_left)){ ?>
									<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
										<tr>
											<td width="100%" valign="top">
												<img src="<?php echo $logo_left; ?>" alt="<?php echo $logo_left; ?>" style= "width: 320; height: 100;float:left"/>
											</td>
										</tr>
									</table>
									<?php } ?> 
								</td>
								<td width="50%">
									<table cellpadding="0" cellspacing="0" style="width:100%;float:left;">
										<tr>
											<td valign="top">
												<p><?php echo $logo_right_name; ?></p>
												<address><?php echo $logo_right_address;?></address>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<br /><br />
					<h3 style="width:96%;float:left;margin:0 0 20px 0;padding:3px 2%;background-color:#6A050D;color:#FFF;">Invoice No: <?php echo $invoice_no; ?></h3>
					<div style="width:100%;float:left;border:1px solid #CCC;">
						<h3 style="width:96%;float:left;margin:0 0 20px 0;padding:3px 2%;background-color:#6A050D;color:#FFF;">Booking Details</h3>
						<table cellpadding="3" cellspacing="0" style="width:100%;float:left;">
						<?php if(!empty($booking_reference)){ ?>
							<tr>
								<td style="width:20%;"><b>Reference:</b></td>
								<td style="width:30%;"><?php echo $booking_reference; ?></td>
								<td style="width:20%;"><b>Confirmation Date:</b></td>
								<td style="width:30%;"><?php echo $booking_confirmation_date; ?></td>
							</tr>
							<tr>
								<td><b>Pax Name:</b></td>
								<td><?php echo $pax_name; ?></td>
								<td><b>Payment Type:</b></td>
								<td><?php echo $payment_type; ?></td>
							</tr>
							<tr>
								<td><b>Agency Ref.:</b></td>
								<td><?php echo $agency_ref; ?></td>
								<td></td>
								<td></td>
							</tr>
						<?php } ?> 
						</table>
					</div>
					<br /><br />
					<div style="width:96%;float:left;border:1px solid #CCC;padding: 10px 2%;">
						<h2 style="text-align:center;margin:0 0 20px 0;padding:3px 0;background-color:#6A050D;color:#FFF;">Services</h2>
						<br /><br />
						<div style="width:100%;float:left;border:1px solid #CCC;">
							<h3 style="width:92%;float:left;margin:0 0 20px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Accommodation</h3>
							<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
							<?php if(!empty($hotel_name)){ ?>
								<tr>
									<td colspan="4">
										<b><?php echo $hotel_name; ?></b>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<b><?php echo $hotel_address; ?></b>
									</td>
								</tr>
								<tr>
									<td style="width:20%;"><b>From: </b><</td>
									<td style="width:30%;"><?php echo $check_in; ?></td>
									<td style="width:20%;"><b>To: </b></td>
									<td style="width:30%;"><?php echo $check_out; ?></td>
								</tr>
								<tr>
									<td style="border-bottom: 1px dashed #ccc; font-weight: bold;padding-left: 4px;"><br/>Room type</td>
									<td style="border-bottom: 1px dashed #ccc; font-weight: bold;"><br/>Board</td>
									<td style="border-bottom: 1px dashed #ccc; font-weight: bold;"><br/>Occupancy</td>
									<td style="border-bottom: 1px dashed #ccc; font-weight: bold;"><br/></td>
								</tr>
								<?php  
								if( $hotel_availableroom ){
									 echo $hotel_availableroom;
								}
								?>
								<!-- <tr>
									<td style="border-bottom: 1px dashed #ccc; width: 180px;"><?php echo $room_type; ?></td>
									<td style="border-bottom: 1px dashed #ccc; width: 150px;"><?php echo $board; ?></td>
									<td style="border-bottom: 1px dashed #ccc; width: 150px;"><?php echo $occupancy; ?></td>
									<td style="border-bottom: 1px dashed #ccc; width: 150px; text-align: right; padding-right: 10px;"><?php echo $price; ?></td>
								</tr> -->
								<tr>
									<td colspan="4">
										<h5 style="margin:15px 0 0 0;">Contract Remarks</h5>
										<p style="text-align:justify;font-size:11px;margin:0;"><?php echo $contract_remarks; ?><p>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<h3 style="text-align:right;margin: 0;">Total: <b><?php echo number_format( $room_price_total,2,'.',',' ); ?></b></h3>
									</td>
								</tr>
							<?php }?>
							</table>
							<?php if( $tour_package ){ ?>
								<h3 style="width:92%;float:left;margin:0 0 0px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Tickets and Excursions</h3>
								<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
								
									<tr>
										<td colspan="4">
											<b><?php echo $tour_package; ?></b>
										</td>
									</tr>
									<tr>
										<td style="width:20%;"><b>From: </b></td>
										<td style="width:30%;"><?php echo $tour_from_date; ?></td>
										<td style="width:20%;"><b>To: </b></td>
										<td style="width:30%;"><?php echo $tour_to_date; ?></td>
									</tr>
									<tr>
										<td colspan="2">
											<h4 style="margin-bottom: 0;">Tickets</h4>
										</td>
									</tr>
									<tr>
										<td colspan="3"> 
											<p style="text-align:justify; margin: 0;"><?php echo $ticket_details; ?><p>
										</td>
										<td colspan= "1"> 
											<p style="text-align:right;margin: 0;"><b><?php echo $ticket_price; ?></b><p>
										</td> 
									</tr>
									<tr>
										<td colspan="4">
											<h5 style="margin:15px 0 0 0;">Contract Remarks</h5>
											<p style="text-align:justify;font-size:11px;margin:0;"><?php echo $ticket_contract_remarks; ?><p>
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<h3 style="text-align:right;margin: 0;">Total: <b><?php echo $total_ticket_price; ?></b></h3>
										</td>
									</tr> 
								</table>
							<?php }?> 
							<?php if( $arrival_hotel_type ){ ?>
								<h3 style="width:92%;float:left;margin:0 0 0px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Arrival Transfer</h3>
								<table cellpadding="3" cellspacing="0" style="width:99%;float:left;"> 
									<tr>
										<td colspan="4">
											<b><?php echo $arrival_hotel_type; ?></b>
										</td>
									</tr>
									<tr>
										<td style="width:20%;"><b>Pick-up Date: </b></td>
										<td style="width:30%;"><?php echo $pickup_date; ?></td>
										<td style="width:20%;"><b>Arrival Time: </b></td>
										<td style="width:30%;"><?php echo $arrival_time; ?></td>
									</tr>
									<tr>
										<td colspan="4">
											<h4 style="margin-bottom: 0;">Arrival Transfer</h4>
										</td>
									</tr>
									<tr>
										<td colspan="3"> 
											<p style="text-align:justify; margin: 0;"><?php echo $arrival_transfer; ?><p>
										</td>
										<td> 
											<p style="text-align:right;margin: 0;"><b><?php echo $arrival_price; ?></b><p>
										</td> 
									</tr>
									<tr>
										<td colspan="4">
											<h3 style="text-align:right;margin: 0;">Total: <b><?php echo $island_price; ?></b></h3>
										</td>
									</tr> 
								</table>
							<?php }?>
							<?php if( $departure_hotel_type ){ ?>
								<h3 style="width:92%;float:left;margin:0 0 0px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Departure Transfer</h3>
								<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
							
									<tr>
										<td colspan="4">
											<b><?php echo $departure_hotel_type; ?></b>
										</td>
									</tr>
									<tr>
										<td style="width:20%;"><b>Pick-up Date: </b></td>
										<td style="width:30%;"><?php echo $departure_pickup_date; ?></td>
										<td style="width:20%;"><b>Arrival Time: </b></td>
										<td style="width:30%;"><?php echo $departure_arrival_time; ?></td>
									</tr>
									<tr>
										<td colspan="4">
											<h4 style="margin-bottom: 0;">Departure Transfer</h4>
										</td>
									</tr>
									<tr>
										<td colspan="3"> 
											<p style="text-align:justify; margin: 0;"><?php echo $departure_arrival_transfer; ?><p>
										</td>
										<td> 
											<p style="text-align:right;margin: 0;"><b><?php echo $departure_arrival_price; ?></b><p>
										</td> 
									</tr>
									<tr>
										<td colspan="4">
											<h3 style="text-align:right;margin: 0;">Total: <?php echo $departure_island_price; ?></h3>
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<h3 style="text-align:right;margin: 0;">Final price total: <?php echo $total_departure_price; ?></h3>
										</td>
									</tr> 
								</table>
							<?php }?>
						</div>
					</div>
					<br /><br />
					<?php if( $hotel_name ){ ?>
						<div style="width:100%;float:left;border:1px solid #CCC;">
							<h3 style="width:92%;float:left;margin:0 0 20px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Agency commission</h3>
							<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
							
								<tr>
									<td><b>Services</b></td>
									<td><b>Assessment</b></td>
								</tr>
								<tr>
									<td style="border-bottom: 1px dashed #ccc;"><?php echo $hotel_name; ?></td>
									<td style="border-bottom: 1px dashed #ccc;"><?php echo $price; ?></td>
								</tr> 
								<tr style= " padding-right: 10px">
									<td><b><br>Tax</b></td>
									<td><br><b><?php echo number_format( $tax_value,2,'.',',' ); ?></b></td>
								</tr>
								<tr style= " padding-right: 10px">
									<td><b><br>Total Web Admin Fee</b></td>
									<td><br><b><?php echo number_format( $web_admin_fee,2,'.',',' ); ?></b></td>
								</tr>
								<tr style= " padding-right: 10px">
									<td><b>Total Service Charge</b></td>
									<td><b><?php echo number_format( $service_charge,2,'.',',' ); ?></b></td>
								</tr>
								<tr style= " padding-right: 10px">
									<td><b>Total net amount to be charged</b></td>
									<td><b><?php echo $total_net_amount; ?></b></td> 
								</tr>
							</table>
						</div> 
					<?php }?>
					<br /><br />
					<div style="width:100%;float:left;border:1px solid #CCC;">
						<h3 style="width:92%;float:left;margin:0 0 20px 0;padding:3px 4%;background-color:#6A050D;color:#FFF;">Cancellation Charges</h3>
						<table cellpadding="3" cellspacing="0" style="width:99%;float:left;">
						<?php if( $cancellation_concept ){ ?>
							<tr>
								<td style="border-bottom: 1px dashed #ccc;"><b>Concept</b></td>
								<td style="border-bottom: 1px dashed #ccc;"><b>From</b></td>
								<td style="border-bottom: 1px dashed #ccc;"><b>Units</b></td>
								<td style="border-bottom: 1px dashed #ccc;"><b>Value</b></td>
							</tr>
							<tr>
								<td style="border-bottom: 1px dashed #ccc;"><?php echo $cancellation_concept; ?></td>
								<td style="border-bottom: 1px dashed #ccc;"><?php echo $cancellation_concept_from; ?></td>
								<td style="border-bottom: 1px dashed #ccc;"><?php echo $cancellation_units; ?></td>
								<td style="border-bottom: 1px dashed #ccc;"><?php echo $cancellation_value; ?></td>
							</tr>
							<tr>
								<td colspan="4">
									<p><b>Date and time is calculated based on local time of destination.</b></p>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<p><b>PAYMENT DEADLINE: Please ensure you pay for this booking before <?php echo $cancellation_concept_from; ?> at 19:59 PM (local time) on your booking will be cancelled automatically.</b></p>
								</td>
							</tr>
						<?php }?>
						</table>
					</div>
					<br /><br /> 
				</body>
			</html>
			<?php
			$html = ob_get_contents();
			ob_end_clean();
					
			return $html; 
		}
	} 
}	 