=== Custom List Table Example ===
Contributors: Rubin Klain iWP, Charlene iWP
Donate link: http://iwebprovider.com/
Tags: example, table, data, WP_List_Table, admin, plugin, list table
Requires at least: 3.5
Tested up to: 4.3
Stable tag: 1.4.1

A highly documented plugin that demonstrates how to create custom admin list-tables using official WordPress techniques.

== Description ==

This plugin serves as a highly documented example of using WordPress's built-in WP_List_Table class for plugin development. Previously, WordPress plugins
had to be created from scratch and were often wildly inconsistent with the rest of the WordPress admin. This example serves to help clarify use of the
WP_List_Table class for quickly and easily generating consistent, standardized, feature-rich list-tables for WordPress admin screens.

One new admin menu is created: "Hotel Booking". This page includes a list table of records from bedsonline booking.  

For more information, please visit the <a href="http://codex.wordpress.org/Class_Reference/WP_List_Table">WP_List_Tables</a> page in the WordPress Codex.

== Changelog ==

= 1.0 =
* Demonstrates usage of WP_List_Table
* Demonstrates pagination
* Demonstrates column sorting
