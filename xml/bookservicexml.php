<?php
class BookServiceXML {

    public static function init() {
        $service = __CLASS__;
        new $service;
    }

    public function __construct() {
	   //construct what you see fit here...
    }
	 
	public function get_service_add_hotel( $availtoken, $contract_name, $contract_incoming_office, $contract_classification, $contract_classification_code, $checkindate, $checkoutdate, $currency, $accomodation_hotel_code, $destination_code, $Destination_Type, $board_type, $board_code, $board_shortname, $accomodation_board, $room_type_type, $room_type_code, $room_type_characteristic, $accomodation_room_type,  $rooms, $adultcount, $childcount, $children_ages = 0, $available_room_shrui, $available_room_availcount, $available_room_onrequest ){
		
		global $wpdb;  
		global $theme_dir;   
		global $session;
		global $bedsonline_username;
		global $bedsonline_password;
		   
		$session_id = $_SESSION['session_id'];
		$booking_token = $_SESSION['booking_token'];
		  
		$search_result = pods( 'searchresult' );
		$admin_language = $search_result->field('language'); 
		
		$checkindate = date( "Ymd", strtotime( $checkindate ) );
		$checkoutdate = date( "Ymd", strtotime( $checkoutdate ) ); 
		$xml = '<ServiceAddRQ echoToken="'.$booking_token.'" version="2013/12" xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages ServiceAddRQ.xsd">
				  <Language>'.$admin_language.'</Language>
				  <Credentials>
					<User>'.$bedsonline_username.'</User>
					<Password>'.$bedsonline_password.'</Password>
				  </Credentials>
				  <Service availToken="'.$availtoken.'" xsi:type="ServiceHotel">
					<ContractList>
						 <Contract>
							<Name>'.$contract_name.'</Name>
							<IncomingOffice code="'.$contract_incoming_office.'"></IncomingOffice>
							<Classification code="'.$contract_classification_code.'">'.$contract_classification.'</Classification>
						</Contract>
					</ContractList>
					<DateFrom date="'.$checkindate.'"/>
					<DateTo date="'.$checkoutdate.'"/>
					 <Currency code="'.$currency.'"/>
					<HotelInfo xsi:type="ProductHotel">
					  <Code>'.$accomodation_hotel_code.'</Code>
					  <Destination code="'.$destination_code.'" type="'.$Destination_Type.'"/>
					</HotelInfo>';
					
						for( $x = 1; $x <= $rooms; $x++ ){   
							$new_room_count = $rooms/$rooms;  
							$key= $x - 1;	
							 
							$adult = $adultcount[$key]; 
							$children = $childcount[$key]; 
							  
							if( $children_ages ){
								foreach($children_ages as $keys=>$col){
									if( $col ){
										foreach($col as $keyss=>$val){     
											if( $keys == $key ){
												if( $keyss == '0' ){ 
												$children_ages1 = $val; 
											 
												} elseif( $keyss == '1' ){ 
													$children_ages2 = $val;
													 
												} elseif( $keyss == '2' ){ 
													$children_ages3 = $val;
												 
												} elseif( $keyss == '3' ){ 
													$children_ages4 = $val;
													 
												}
											}
										} 
									} 
								}
							}  
							$xml .= '<AvailableRoom><HotelOccupancy>   
									<RoomCount>'.$new_room_count.'</RoomCount>
									<Occupancy>
										<AdultCount>'.$adult.'</AdultCount>
										<ChildCount>'.$children.'</ChildCount>';   
										if( $children_ages ){
											if( $children != "0"){		 
												$xml .= '<GuestList>'; 
													
												if( $children == "1"){	
													$xml .= '<Customer type="CH">
														<Age>'.$children_ages1.'</Age>
													</Customer>';
												} elseif( $children == "2"){	
													$xml .= '<Customer type="CH">
														<Age>'.$children_ages1.'</Age>
													</Customer>
													<Customer type="CH">
														<Age>'.$children_ages2.'</Age>
													</Customer>';
												} elseif( $children == "3"){
													$xml .= '<Customer type="CH">
														<Age>'.$children_ages1.'</Age>
													</Customer>
													<Customer type="CH">
														<Age>'.$children_ages2.'</Age>
													</Customer>
													<Customer type="CH">
														<Age>'.$children_ages3.'</Age>
													</Customer>';
												} elseif( $children == "4"){
													$xml .= '<Customer type="CH">
														<Age>'.$children_ages1.'</Age>
													</Customer>
													<Customer type="CH">
														<Age>'.$children_ages2.'</Age>
													</Customer>
													<Customer type="CH">
														<Age>'.$children_ages3.'</Age>
													</Customer>
													<Customer type="CH">
														<Age>'.$children_ages4.'</Age>
													</Customer>';
												}
												$xml .= '</GuestList>';
											}
										}
								$xml .= '</Occupancy>
							</HotelOccupancy>'; 
						$xml .= '<HotelRoom SHRUI="'.$available_room_shrui.'" availCount="'.$available_room_availcount.'" onRequest="'.$available_room_onrequest.'" >';
							$xml .= '<Board type="'.$board_type.'" code="'.$board_code.'" shortname="'.$board_shortname.'">'.$accomodation_board.'</Board>';
							$xml .= '<RoomType type="'.$room_type_type.'" code="'.$room_type_code.'" characteristic="'.$room_type_characteristic.'">'.$accomodation_room_type.'</RoomType>';
						$xml .= '</HotelRoom>
					</AvailableRoom>';
					}
				$xml .= '</Service>
			</ServiceAddRQ>';
	  
		$contents = get_xml_curl_request( $xml );
  
		// $suffix = $suffix ? $suffix : date('Ymd_His');
		$suffix = date( 'Y-m-d' );
		$path = get_template_directory().'-child/inc/xml-logs/';
		$file_name_rq = 'getServiceAdd_rq';
		$file_name_rs = 'getServiceAdd_rs';
		$full_file_path = $path.$file_name_rs.'_'.$suffix.'.xml';
		 
		iwp_create_file( $xml, $path, $file_name_rq, 'xml', $suffix );		
		iwp_create_file( $contents, $path, $file_name_rs, 'xml', $suffix );		
		  
		return $contents;	 
	} 
	
	public function get_service_remove( $purchasetoken, $spui ){ 
		 
		global $wpdb;  
		global $theme_dir;   
		global $session;
		global $bedsonline_username;
		global $bedsonline_password;
		   
		$session_id = $_SESSION['session_id'];
		$booking_token = $_SESSION['booking_token'];
		  
		$search_result = pods( 'searchresult' );
		$admin_language = $search_result->field('language'); 
		    
		$xml = '<ServiceRemoveRQ purchaseToken="'.$purchasetoken.'" SPUI="'.$spui.'" xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance ../xsd/ServiceRemoveRQ.xsd" version="2013/12">
			<Language>ENG</Language>
			<Credentials>
				<User>'.$bedsonline_username.'</User>
				<Password>'.$bedsonline_password.'</Password>
			</Credentials>
		</ServiceRemoveRQ>';
 
		$contents = get_xml_curl_request( $xml );
		
		$suffix = date( 'Y-m-d' );
		$path = get_template_directory().'-child/inc/xml-logs/';
		$file_name_rq = 'getServiceRemove_rq';
		$file_name_rs = 'getServiceRemove_rs';
		$full_file_path = $path.$file_name_rs.'_'.$suffix.'.xml';
		 
		iwp_create_file( $xml, $path, $file_name_rq, 'xml', $suffix );		
		iwp_create_file( $contents, $path, $file_name_rs, 'xml', $suffix );		
		   
		return $contents;	 
	}
} 
?>