<?php
class BookPurchaseXML {

    public static function init() {
        $purchase = __CLASS__;
        new $purchase;
    }

    public function __construct() {
	   //construct what you see fit here...
    }
	
	private function create_xml_logs( $request, $response, $file_name_rs, $file_name_rq, $format='Y-m-d' ){
		
		$suffix = date( $format );
		$path = get_template_directory().'-child/inc/xml-logs/'; 
		$full_file_path = $path.$file_name_rs.'_'.$suffix.'.xml';
		 
		iwp_create_file( $request, $path, $file_name_rq, 'xml', $suffix );		
		iwp_create_file( $response, $path, $file_name_rs, 'xml', $suffix );	

		return $full_file_path;
	}
	 
	public function purchase_confirm_service_hotel_xml( $purchase_token, $passenger_adult_first_name_datails_array, $passenger_adult_last_name_datails_array, $passenger_child_first_name_datails_array, $passenger_child_last_name_datails_array, $room_count_summary, $adult_count_summary, $child_count_summary, $agency_reference,$service_type, $spui, $customer_list_type_array, $customer_list_istravel_agent_array, $customer_list_customer_id_array, $customer_list_age_array, $remarks ){  
	 
		global $wpdb;  
		global $theme_dir;   
		global $session;
		global $bedsonline_username;
		global $bedsonline_password;
		   
		$session_id = $_SESSION['session_id'];
		$booking_token = $_SESSION['booking_token'];
		
		if( empty( $booking_token ) ){
			$booking_token = "dummytocken";
		} 
		$search_result = pods( 'searchresult' );
		$admin_language = $search_result->field('language'); 
		 
		$passenger_first_name ='';
		$passenger_last_name =''; 
		
		for ( $y = 1; $y <= $adult_count_summary; $y++ ){    
			$row = 0;	 
			if( isset( $passenger_adult_first_name_datails_array[$row] ) ){
				$passenger_first_name = $passenger_adult_first_name_datails_array[$row]; 
			}
			if( isset( $passenger_adult_first_name_datails_array[$row] ) ){
				$passenger_last_name = $passenger_adult_last_name_datails_array[$row];  
			} 
		}		
		 
	 	$xml = '<PurchaseConfirmRQ echoToken = "'.$booking_token.'" xmlns = "http://www.hotelbeds.com/schemas/2005/06/messages" version = "2013/12" xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation = "http://www.hotelbeds.com/schemas/2005/06/messages PurchaseConfirmRQ.xsd">
				<Language>'.$admin_language.'</Language>
				<Credentials>
					<User>'.$bedsonline_username.'</User>
					<Password>'.$bedsonline_password.'</Password>
				</Credentials>
				<ConfirmationData purchaseToken="'.$purchase_token.'">
					<Holder type="AD">
						<Name>'.$passenger_first_name.'</Name>
						<LastName>'.$passenger_last_name.'</LastName>
					</Holder>
					<AgencyReference>'.$agency_reference.'</AgencyReference>
					<ConfirmationServiceDataList>
						<ServiceData SPUI="'.$spui.'" xsi:type = "ConfirmationServiceDataHotel">
							<CustomerList>';
								for ( $x = 1; $x <= $adult_count_summary; $x++ ){   
								
									$key = $x - 1;	
									
									if( !empty( $customer_list_type_array[$key] ) ){
										$customer_list_type = $customer_list_type_array[$key];  
									} else {
										$customer_list_type = '';
									}
									
									if( !empty( $customer_list_istravel_agent_array[$key] ) ){
										$customer_list_istravel_agent = $customer_list_istravel_agent_array[$key];  
									} else {
										$customer_list_istravel_agent = '';
									}
									
									if( !empty( $customer_list_customer_id_array[$key] ) ){
										$customer_list_customer_id = $customer_list_customer_id_array[$key];  
									} else {
										$customer_list_customer_id = '';
									}
									
									if( !empty( $customer_list_age_array[$key] ) ){
										$customer_list_age = $customer_list_age_array[$key];  
									} else {
										$customer_list_age = '';
									}
									
									if( !empty( $passenger_adult_first_name_datails_array[$key] ) ){
										$passenger_first_name = $passenger_adult_first_name_datails_array[$key];  
									} else {
										$passenger_first_name = '';
									}
									
									if( !empty( $passenger_adult_last_name_datails_array[$key] ) ){
										$passenger_last_name = $passenger_adult_last_name_datails_array[$key];  
									} else {
										$passenger_last_name = '';
									}
									
									if( !empty( $passenger_child_first_name_datails_array[$key] ) ){
										$passenger_child_first_name = $passenger_child_first_name_datails_array[$key];  
									} else {
										$passenger_child_first_name = '';
									}
									
									if( !empty( $passenger_child_last_name_datails_array[$key] ) ){
										$passenger_child_last_name = $passenger_child_last_name_datails_array[$key];  
									} else {
										$passenger_child_last_name = '';
									} 
									 
									if( $passenger_first_name || $passenger_last_name ){
										if( $customer_list_type && $customer_list_customer_id &&  $customer_list_age  ){ 
											
											if( $customer_list_type == 'AD'){ 
												$xml .= '<Customer type="'.$customer_list_type.'">
															<CustomerId>'.$customer_list_customer_id.'</CustomerId>
															<Age>'.$customer_list_age.'</Age>
															<Name>'.$passenger_first_name.'</Name>
															<LastName>'.$passenger_last_name.'</LastName> 
														 </Customer>';
											} elseif( $customer_list_type == 'CH'){
												$xml .= '<Customer type="'.$customer_list_type.'">
															<CustomerId>'.$customer_list_customer_id.'</CustomerId>
															<Age>'.$customer_list_age.'</Age>
															<Name>'.$passenger_child_first_name.'</Name>
															<LastName>'.$passenger_child_last_name.'</LastName> 
														 </Customer>';
											}
										}
									} else {
										if( $customer_list_type && $customer_list_customer_id &&  $customer_list_age  ){ 
											$xml .= '<Customer type="'.$customer_list_type.'">
														<CustomerId>'.$customer_list_customer_id.'</CustomerId>
														<Age>'.$customer_list_age.'</Age>
													</Customer>'; 
										}
									} 
								} 
							$xml .= '</CustomerList>'; 
							if( $remarks ){  
									$xml .= '<CommentList>';
									foreach( $remarks as $selected ){
										$xml .= '<Comment type="SERVICE">'.$selected.'</Comment> 
												 <Comment type="INCOMING">'.$selected.'</Comment>';  
										} 
								$xml .= '</CommentList>';
							}
						$xml .= '</ServiceData>
					</ConfirmationServiceDataList>
				</ConfirmationData>
			</PurchaseConfirmRQ>';
	  
		
		$contents = get_xml_curl_request( $xml ); 
		   
		$file_name_rq = 'getPurchaseConfirmRQ';
		$file_name_rs = 'getPurchaseConfirmRS'; 
		
		PurchaseXML::create_xml_logs( $xml, $contents, $file_name_rs, $file_name_rq, 'Ymd_His' );
		  
		return $contents;	  
	}
	
	public function cancel_error_purchase_xml( $filenumber, $incomingoffice ){
		 
		global $wpdb;  
		global $theme_dir;    
		global $bedsonline_username;
		global $bedsonline_password;
		   
	 
		$search_result = pods( 'searchresult' );
		$admin_language = $search_result->field('language'); 
		 
		$xml = '<PurchaseCancelRQ xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance ../xsd/PurchaseCancelRQ.xsd" version="2013/12" type="C">
					<Language>'.$admin_language.'</Language>
					<Credentials>
						<User>'.$bedsonline_username.'</User>
						<Password>'.$bedsonline_password.'</Password>
					</Credentials>
					<PurchaseReference>
						<FileNumber>'.$filenumber.'</FileNumber>
						<IncomingOffice code="'.$incomingoffice.'"/>
					</PurchaseReference>
				</PurchaseCancelRQ>';
	  
		$contents = get_xml_curl_request( $xml );
		 
		$file_name_rq = 'getPurchaseCancel_rq';
		$file_name_rs = 'getPurchaseCancel_rs'; 
		 
		PurchaseXML::create_xml_logs( $xml, $contents, $file_name_rs, $file_name_rq, 'Ymd_His' );
	    
		return $contents;	 
	}
	
	public function view_purchaise_details( $filenumber, $incomingoffice ){ 
		
		global $wpdb;  
		global $theme_dir;    
		global $bedsonline_username;
		global $bedsonline_password;
		    
		$search_result = pods( 'searchresult' );
		$admin_language = $search_result->field('language'); 
		    
		$xml = '<PurchaseDetailRQ echoToken="DummyEchoToken"
				  xmlns="http://www.hotelbeds.com/schemas/2005/06/messages"
				  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance ../xsd/PurchaseDetailRQ.xsd" version="2013/12">
				  <Language>'.$admin_language.'</Language>
				  <Credentials>
					<User>'.$bedsonline_username.'</User>
					<Password>'.$bedsonline_password.'</Password>
				  </Credentials>
				  <PurchaseReference>
					<FileNumber>'.$filenumber.'</FileNumber>
					<IncomingOffice code="'.$incomingoffice.'"/>
				  </PurchaseReference>
				</PurchaseDetailRQ>';
	  
		$contents = get_xml_curl_request( $xml );
		 
		$file_name_rq = 'getPurchaseDetail_rq';
		$file_name_rs = 'getPurchaseDetail_rs'; 
		
		PurchaseXML::create_xml_logs( $xml, $contents, $file_name_rs, $file_name_rq, 'Ymd_His' );
		   
		return $contents;	 
	}
}
?>