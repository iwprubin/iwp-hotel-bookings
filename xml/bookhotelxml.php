<?php
class BookHotelXML {
	
	public static function init() {
        $hotelxml = __CLASS__;
        new $hotelxml;
    }

    public function __construct() {
	   //construct what you see fit here...
    }
	
	private function get_xml_request( $xml ){
		  
		$search_result = pods( 'searchresult' );
		$server_url = $search_result->field('server_url'); 
			
		// $server_url = 'http://testapi.interface-xml.com/appservices/http/FrontendService'; //Test Server
		// $server_url = 'http://testapi.interface-xml.com/appservices/http/AcceptanceTest';  //Acceptance Server
		// $server_url = 'http://api.interface-xml.com/appservices/http/FrontendService';  //Live Server
				   
		$ch = curl_init(); 
		curl_setopt( $ch, CURLOPT_URL, $server_url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type: text/xml', 'Accept-Encoding: gzip,deflate' ) ); 
		curl_setopt( $ch,CURLOPT_ENCODING , "gzip" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		$response = curl_exec($ch); 
		curl_close($ch);   
		
		return $response; 
 
	}
	
	
	private function create_xml_logs( $request, $response, $file_name_rs, $file_name_rq, $format='Y-m-d' ){
		
		$suffix = date( $format );
		$path = get_template_directory().'-child/inc/xml-logs/'; 
		$full_file_path = $path.$file_name_rs.'_'.$suffix.'.xml';
		 
		iwp_create_file( $request, $path, $file_name_rq, 'xml', $suffix );		
		iwp_create_file( $response, $path, $file_name_rs, 'xml', $suffix );	

		return $full_file_path;
	}
  
	public function get_available_hotels( $checkindate, $checkoutdate, $destination, $zonecode, $rooms, $adultcount, $childcount, $children_ages, $xmldata, $hotelcode, $selected_hotel_catergory='0', $pagination_page=null, $selected_hotel_board='0' ){ 	
		 
		global $wpdb;  
		global $theme_dir;   
		global $session;
		global $bedsonline_username;
		global $bedsonline_password;
		 
		register_session(); 
		  
		$booking_session_id = $_SESSION['booking_session_id'];
		$booking_token = $_SESSION['booking_token'];
	 
		$page_number = $_POST['page'];
		 
		if( !empty( $pagination_page) ){
			$page_number = $pagination_page;
		} else { 
			if( $page_number == '' ){
				$page_number = 1;
			} else {
				$page_number = $page_number;
			}
		}  
		
		$search_result = pods( 'searchresult' );
		$admin_language = $search_result->field('language');
		$admin_items_per_page = $search_result->field('items_per_page');
		 
		if( $checkindate && $checkoutdate ){ 
			$checkindate = date( "Ymd", strtotime( $checkindate ) );
			$checkoutdate = date( "Ymd", strtotime( $checkoutdate ) );
			
			$xml = '<HotelValuedAvailRQ echoToken="'.$booking_token.'" sessionId = "'.$booking_session_id.'" xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance ../xsd/HotelValuedAvailRQ.xsd" showDiscountsList="Y" version="2013/12">  
						<Language>'.$admin_language.'</Language>
						<Credentials>
							<User>'.$bedsonline_username.'</User>
							<Password>'.$bedsonline_password.'</Password>  
						</Credentials> 
						<PaginationData pageNumber="'.$page_number.'" itemsPerPage="'.$admin_items_per_page.'" />
						<CheckInDate date="'.$checkindate.'"/>
						<CheckOutDate date="'.$checkoutdate.'"/>';
						if( $hotelcode ){
							$xml .= '<HotelCodeList withinResults = "Y">
										<ProductCode>'.$hotelcode.'</ProductCode>
									</HotelCodeList>'; 
						}
						if( $zonecode ){
							$xml .= '<Destination code="'.$destination.'" type="SIMPLE">
										<ZoneList>
											<Zone type="SIMPLE" code="'.$zonecode.'"/>
										</ZoneList>
									</Destination>';	 
						}else{ 
							$xml .= '<Destination code="'.$destination.'" type="SIMPLE"/>';   
						}
						
						$xml .= '<ExtraParamList>
							<ExtendedData type="EXT_DISPLAYER">
								<Name>DISPLAYER_DEFAULT</Name>
								<Value>PROMOTION:Y</Value>
							</ExtendedData>
						</ExtraParamList>';
						if( $selected_hotel_catergory ){
							$xml .= '<CategoryList>';  
							foreach( $selected_hotel_catergory as $selected => $sel ) {
								if( $sel == '1EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="1EST" />';
								} elseif( $sel == '2EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="2EST" />';
								} elseif( $sel == '3EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="3EST" />';
								} elseif( $sel == '4EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="4EST" />';
								} elseif( $sel == '5EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="5EST" />';
								} 
							}
							$xml .= '</CategoryList>';  
						} 
						 
						if( $selected_hotel_board ){ 
							$xml .= '<BoardList>';  
							foreach( $selected_hotel_board as $hb => $sel_hb ) { 
								$xml .= '<HotelBoard type="SIMPLE" code="'.$sel_hb.'" />';
								 
							}
							$xml .= '</BoardList>';  
						} 
						$xml .= '<OccupancyList>';
							for( $x = 1; $x <= $rooms; $x++ ){   
								$new_room_count = $rooms/$rooms;  
								$key= $x - 1;	
								 
								$adult = $adultcount[$key]; 
								$children = $childcount[$key]; 
								  
								if( $children_ages ){
									foreach($children_ages as $keys=>$col){ 
										foreach($col as $keyss=>$val){     
											if( $keys == $key ){
												if( $keyss == '0' ){ 
												$children_ages1 = $val; 
											 
												} elseif( $keyss == '1' ){ 
													$children_ages2 = $val;
													 
												} elseif( $keyss == '2' ){ 
													$children_ages3 = $val;
												 
												} elseif( $keyss == '3' ){ 
													$children_ages4 = $val;
													 
												}
											}
										} 
									}
								}  
								$xml .= '<HotelOccupancy>   
										<RoomCount>'.$new_room_count.'</RoomCount>
										<Occupancy>
											<AdultCount>'.$adult.'</AdultCount>
											<ChildCount>'.$children.'</ChildCount>';   
											if( $children_ages ){
												if( $children != "0"){		 
													$xml .= '<GuestList>'; 
														
													if( $children == "1"){	
														$xml .= '<Customer type="CH">
															<Age>'.$children_ages1.'</Age>
														</Customer>';
													} elseif( $children == "2"){	
														$xml .= '<Customer type="CH">
															<Age>'.$children_ages1.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages2.'</Age>
														</Customer>';
													} elseif( $children == "3"){
														$xml .= '<Customer type="CH">
															<Age>'.$children_ages1.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages2.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages3.'</Age>
														</Customer>';
													} elseif( $children == "4"){
														$xml .= '<Customer type="CH">
															<Age>'.$children_ages1.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages2.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages3.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages4.'</Age>
														</Customer>';
													}
													$xml .= '</GuestList>';
												}
											}
									$xml .= '</Occupancy>
								</HotelOccupancy>';
							} 
						$xml .= '</OccupancyList>
					</HotelValuedAvailRQ>'; 
			
			if( $xmldata ){
				$contents = file_get_contents( $xmldata );
				$xml = $contents; 
			} else {
				$xml = $xml;
			}
			
			$contents = BookHotelXML::get_xml_request( $xml );
			  
			   
			$file_name_rs = 'getHotelValuedAvailrs';
			$file_name_rq = 'getHotelValuedAvailrq'; 
			$file_location = HotelXML::create_xml_logs( $xml, $contents, $file_name_rs, $file_name_rq );
			return $contents;
		} 
	} 
	  
	public function get_url_from_user( $checkindate, $checkoutdate, $destination, $zonecode, $rooms, $adultcount, $childcount, $children_ages, $xmldata, $hotelcode, $selected_hotel_catergory = '0', $pagination_page = null, $selected_hotel_board = '0' ){ 	
		 
		global $wpdb;  
		global $theme_dir;   
		global $session;
		global $bedsonline_username;
		global $bedsonline_password;
		 
		register_session(); 
		  
		$booking_session_id = $_SESSION['booking_session_id'];
		$booking_token = $_SESSION['booking_token'];
	 
		$page_number = $_POST['page'];
		 
		if( !empty( $pagination_page) ){
			$page_number = $pagination_page;
		} else { 
			if( $page_number == '' ){
				$page_number = 1;
			} else {
				$page_number = $page_number;
			}
		}  
		
		$search_result = pods( 'searchresult' );
		$admin_language = $search_result->field('language');
		$admin_items_per_page = $search_result->field('items_per_page');
		
		if( $checkindate && $checkoutdate ){ 
		
			$checkindate = date( "Ymd", strtotime( $checkindate ) );
			$checkoutdate = date( "Ymd", strtotime( $checkoutdate ) );
		
		
			$xml = '<HotelValuedAvailRQ echoToken="'.$booking_token.'" sessionId = "'.$booking_session_id.'" xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance ../xsd/HotelValuedAvailRQ.xsd" showDiscountsList="Y" version="2013/12">  
						<Language>'.$admin_language.'</Language>
						<Credentials>
							<User>'.$bedsonline_username.'</User>
							<Password>'.$bedsonline_password.'</Password>  
						</Credentials> 
						<PaginationData pageNumber="'.$page_number.'" itemsPerPage="'.$admin_items_per_page.'" />
						<CheckInDate date="'.$checkindate.'"/>
						<CheckOutDate date="'.$checkoutdate.'"/>';
						if( $hotelcode ){
							$xml .= '<HotelCodeList withinResults = "Y">
										<ProductCode>'.$hotelcode.'</ProductCode>
									</HotelCodeList>'; 
						}
						if( $zonecode ){
							$xml .= '<Destination code="'.$destination.'" type="SIMPLE">
										<ZoneList>
											<Zone type="SIMPLE" code="'.$zonecode.'"/>
										</ZoneList>
									</Destination>';	 
						}else{ 
							$xml .= '<Destination code="'.$destination.'" type="SIMPLE"/>';   
						}
						
						$xml .= '<ExtraParamList>
							<ExtendedData type="EXT_DISPLAYER">
								<Name>DISPLAYER_DEFAULT</Name>
								<Value>PROMOTION:Y</Value>
							</ExtendedData>
						</ExtraParamList>';
						if( $selected_hotel_catergory ){
							$xml .= '<CategoryList>';  
							foreach( $selected_hotel_catergory as $selected => $sel ) {
								if( $sel == '1EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="1EST" />';
								} elseif( $sel == '2EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="2EST" />';
								} elseif( $sel == '3EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="3EST" />';
								} elseif( $sel == '4EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="4EST" />';
								} elseif( $sel == '5EST' ){
									$xml .= '<HotelCategory type="SIMPLE" code="5EST" />';
								} 
							}
							$xml .= '</CategoryList>';  
						} 
						 
						if( $selected_hotel_board ){ 
							$xml .= '<BoardList>';  
							foreach( $selected_hotel_board as $hb => $sel_hb ) { 
								$xml .= '<HotelBoard type="SIMPLE" code="'.$sel_hb.'" />';
								 
							}
							$xml .= '</BoardList>';  
						} 
						$xml .= '<OccupancyList>';
							for( $x = 1; $x <= $rooms; $x++ ){   
								$new_room_count = $rooms/$rooms;  
								$key= $x - 1;	
								 
								$adult = $adultcount[$key]; 
								$children = $childcount[$key]; 
								  
								if( $children_ages ){
									foreach($children_ages as $keys=>$col){ 
										foreach($col as $keyss=>$val){     
											if( $keys == $key ){
												if( $keyss == '0' ){ 
												$children_ages1 = $val; 
											 
												} elseif( $keyss == '1' ){ 
													$children_ages2 = $val;
													 
												} elseif( $keyss == '2' ){ 
													$children_ages3 = $val;
												 
												} elseif( $keyss == '3' ){ 
													$children_ages4 = $val;
													 
												}
											}
										} 
									}
								}  
								$xml .= '<HotelOccupancy>   
										<RoomCount>'.$new_room_count.'</RoomCount>
										<Occupancy>
											<AdultCount>'.$adult.'</AdultCount>
											<ChildCount>'.$children.'</ChildCount>';   
											if( $children_ages ){
												if( $children != "0"){		 
													$xml .= '<GuestList>'; 
														
													if( $children == "1"){	
														$xml .= '<Customer type="CH">
															<Age>'.$children_ages1.'</Age>
														</Customer>';
													} elseif( $children == "2"){	
														$xml .= '<Customer type="CH">
															<Age>'.$children_ages1.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages2.'</Age>
														</Customer>';
													} elseif( $children == "3"){
														$xml .= '<Customer type="CH">
															<Age>'.$children_ages1.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages2.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages3.'</Age>
														</Customer>';
													} elseif( $children == "4"){
														$xml .= '<Customer type="CH">
															<Age>'.$children_ages1.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages2.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages3.'</Age>
														</Customer>
														<Customer type="CH">
															<Age>'.$children_ages4.'</Age>
														</Customer>';
													}
													$xml .= '</GuestList>';
												}
											}
									$xml .= '</Occupancy>
								</HotelOccupancy>';
							} 
						$xml .= '</OccupancyList>
					</HotelValuedAvailRQ>'; 
			
			if( $xmldata ){
				$contents = file_get_contents( $xmldata );
				$xml = $contents; 
			} else {
				$xml = $xml;
			}
			
			$contents = BookHotelXML::get_xml_request( $xml );
			
			  
			
			$suffix =  date('Ymd_His');
			$path = get_template_directory().'-child/inc/xml-logs/';
			$file_name = 'userInputedDetails'; 
			$full_file_name = $file_name.'_'.$suffix.'.xml';
			
			iwp_create_file( $xml, $path, $file_name, 'xml', $suffix );		 		
		  
			return $full_file_name;		 
	 
		} 
	}   
	
	public function get_hotels_details( $hotel_code ){ 
		 	  
		if( $hotel_code ){  
			 
			global $wpdb;  
			global $theme_dir;   
			global $session;
			global $bedsonline_username;
			global $bedsonline_password;
			    
			$booking_token = $_SESSION['booking_token'];
			$search_result = pods( 'searchresult' );
			$admin_language = $search_result->field('language'); 
			
			if( $booking_token ){
				$booking_token = "dummytocken";
			}
		
			$xml = '<HotelDetailRQ echoToken="'.$booking_token.'"  xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages HotelDetailRQ.xsd">
							<Language>'.$admin_language.'</Language>
							<Credentials>
							<User>'.$bedsonline_username.'</User>
							<Password>'.$bedsonline_password.'</Password>
							</Credentials>
							 <HotelCode>'.$hotel_code.'</HotelCode>
						</HotelDetailRQ>'; 
						
			$contents = BookHotelXML::get_xml_request( $xml );
			
			$suffix =  date('Ymd_His');
			$path = get_template_directory().'-child/inc/xml-logs/'; 
			$file_name_rs = 'getHotelValuedAvailrs';
			$file_name_rq = 'getHotelValuedAvailrq'; 
			iwp_create_file( $xml, $path, $file_name_rq, 'xml', $suffix );		 		
			iwp_create_file( $contents, $path, $file_name_rs, 'xml', $suffix );		 		
 
			return $contents;
			 
 		}  
	}  
}
?>