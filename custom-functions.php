<?php 
/* Standard code here */   
function printr( $array ){
	echo '<pre>';
	print_r($array);
	echo '</pre><br />';
}

function printx( $array ){
	echo '<pre>';
	print_r($array);
	echo '</pre><br />';
	exit();
}

function iwp_get_request( $name, $default_value=NULL ){ 
	
	global $wpdb;
	global $session;
	
	$value = isset( $_POST[$name] )?$_POST[$name]:( isset( $_GET[$name] )?$_GET[$name]:NULL );
	
	if( isset( $value ) && $value == '' && isset( $default_value ) ){
		$value = $default_value;
	}
	
	if( get_magic_quotes_gpc() == TRUE ){
		$value = stripslashes( $value );
	}
	
	return $value;
}

function iwp_show_alerts(){

	global $wpdb; 
	
	global $session;
	
	// $result = '';
	$alerts = $_SESSION['alerts'];  
	$alerts_type = $_SESSION['alerts_type'];   
	
	if( $alerts_type ){
		$success = isset($alerts_type['success'])?$alerts_type['success']:'';
		$warning = isset($alerts_type['warning'])?$alerts_type['warning']:'';
		$danger = isset($alerts_type['danger'])?$alerts_type['danger']:'';
		
		if( $success ){
			$result = '<div class="alert alert-success" role="alert">'.$alerts.'</div>';
		}
		
		if( $warning ){
			$result = '<div class="alert alert-warning" role="alert">'.$alerts.'</div>';
		}
		
		if( $danger ){
			$result = '<div class="alert alert-danger" role="alert">'.$alerts.'</div>';
		} 
	} 
	iwp_unset_alerts();	
	
	return $result;
} 

function iwp_set_alerts( $type, $message ){
 
	global $wpdb;
	global $session;
	 
	// $_SESSION['alerts'][$type] = $message; 
	$_SESSION['alerts_type'] = $type;
	$_SESSION['alerts'] = $message;  
}

function iwp_unset_alerts(){
	  
	global $wpdb;
	global $session;
	
	unset( $_SESSION['alerts'] );
	unset( $_SESSION['alerts_type'] );
	 
}

function set_content_type( $content_type ){
	global $wpdb; 
	return 'text/html';
}

function iwp_create_file( $content, $path, $file_name, $ext, $suffix ){
 
	global $wpdb;
	
	$full_file_path = $path.$file_name.'_'.$suffix.'.'.$ext;

	if ( is_writable( dirname(__FILE__) ) ) {
		$handle = @fopen( $full_file_path, 'w' );
		@fwrite( $handle, $content );
		@fclose( $handle );
		return TRUE;
	} else {
		echo 'Path '.dirname(__FILE__).' not writable.';
		return False;
		exit;
	}
}	
  
function iwp_get_day_from_date( $given_date ){ 
	
	global $wpdb;
	
	if( $given_date ){
		$timestamp = strtotime( $given_date ); 
		$day = date( 'l', $timestamp );
		$day_display = '( '.$day.' )';
		
		return $day_display;
	} 
}

function iwp_limit_chars( $string, $lenght, $html=FALSE, $char_set='UTF-8', $dots='...' ){
	
	global $wpdb;
	
	if( $html == TRUE ){
		$char_set = strtoupper( $char_set );
						
		$string = strip_tags( $string );
		$string = html_entity_decode( $string, ENT_QUOTES, $char_set );
	
		$strlength = strlen( $string );
		
		if ( $strlength > $lenght ){       
			$limited = substr( $string, 0, $lenght );
			$limited .= $dots;                  
		} else {
			$limited = $string;
		}
		
		$limited = htmlentities( $limited, ENT_QUOTES, $char_set );
	} else {
	
		$strlength = strlen( $string );
		
		if ( $strlength > $lenght ){       
			$limited = substr( $string, 0, $lenght );
			$limited .= $dots;                  
		} else {
			$limited = $string;
		}
	}
	
	return $limited;
}

function iwp_get_page_contents(){

	global $wpdb;
	
	$date_format = get_option('date_format');
	$time_format = get_option('time_format');

	the_post();
	$page_id = get_the_ID();
	$result['page_id'] = $page_id;
	$result['page_title'] = get_the_title();
	$result['page_link'] = get_permalink();
	$result['page_content'] = get_the_content();
	$result['page_excerpt'] = get_the_excerpt();
	$result['page_author'] = get_the_author();
	$result['page_comments'] = get_comments_number();
	$result['page_datetime'] = get_the_time($date_format.' \a\t '.$time_format);
	$page = get_page($page_id);
	$result['page_post_name'] = $page->post_name;
	$custom = get_post_custom_values('meta_description');
	$result['page_meta_description'] = $custom[0];
	
	if( has_post_thumbnail( $page_id ) ){
		$post_thumbnail_id = get_post_thumbnail_id( $page_id );
		$image = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
		$result['page_featured_image'] = isset( $image[0] )?$image[0]:'';
	}
	
	ob_start();
	the_content();
	$result['page_the_content'] = ob_get_contents();
	ob_end_clean(); 
	wp_reset_query();
	
	return $result;
}

function get_value( $array, $name, $default_value=NULL ){
	
	if( is_array($array) ){
		$value = isset($array[$name])?$array[$name]:NULL;
	} elseif( is_object($array) ){
		$value = isset($array->$name)?$array->$name:NULL;
	} else {
		$value = $array;
	}
	
	if( $value == '' && isset($default_value) ){
		$value = $default_value;
	}
	
	return $value;
}

function iwp_datetime( $time_stamp, $format='Y-m-d H:i:s' ){
	
	$date_time = date($format, strtotime($time_stamp));
	if( $date_time == '1970-01-01'|| $date_time == '' ){
		$date_time = '0000-00-00';
	}
	
	return $date_time;
}

function get_date_of_birth( $sday, $smonth='', $syear='' ){
	
	if( $smonth == '' && $syear == '' ){
		$sday = iwp_datetime($sday,'d');
		$smonth = iwp_datetime($sday,'m');
		$syear = iwp_datetime($sday,'Y');
	}
	
	ob_start();
	?>
	<div class="row">
		<div class="col-sm-3">
			<select name="bdaydate" class="form-control">
			<?php
			if( $sday == '' ){
				?>
				<option value="">Day</option>
				<?php
			}
			for($day=1; $day<=31; $day++){
				$tday = ($day<10)?('0'.$day):$day;
				if($sday == $day){
					?>
					<option value="<?php echo $day; ?>" selected="selected"><?php echo $tday; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $day; ?>" ><?php echo $tday; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-sm-4">
			<select name="bdaymonth" class="form-control">
			<?php
			if( $smonth == '' ){
				?>
				<option value="">Month</option>
				<?php
			}
			for($month=1; $month<=12; $month++){
				$tmonth = ($month<10)?('0'.$month):$month;
				if($smonth == $month){
					?>
					<option value="<?php echo $month; ?>" selected="selected"><?php echo $tmonth; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $month; ?>" ><?php echo $tmonth; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
		<div class="col-sm-5">
			<select name="bdayyear" class="form-control">
			<?php
			if( $syear == '' ){
				?>
				<option value="">Year</option>
				<?php
			}
			$current_year = date('Y');
			$limit_year = $current_year-100;
			for($year=$current_year; $year>=$limit_year; $year--){
				if($syear == $year){
					?>
					<option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
					<?php
				} else {
					?>
					<option value="<?php echo $year; ?>" ><?php echo $year; ?></option>
					<?php
				}
			}
			?>
			</select>
		</div>
	</div>
	<?php
	$contents = ob_get_contents();
	ob_end_clean();
	
	return $contents;
}

function iwp_current_date(){
	$date = date('Y-m-d');
	
	return $date;
}

function iwp_current_datetime(){
	$date = date('Y-m-d H:i:s');
	
	return $date;
}

function iwp_is_serialized($value, &$result = null){
	// Bit of a give away this one
	if (!is_string($value)){
		return false;
	}
	// Serialized false, return true. unserialize() returns false on an
	// invalid string or it could return false if the string is serialized
	// false, eliminate that possibility.
	if ($value === 'b:0;'){
		$result = false;
		return true;
	}
	$length	= strlen($value);
	$end	= '';
	switch ( !empty( $value[0] ) ){
		case 's':
			if ($value[$length - 2] !== '"'){
				return false;
			}
		case 'b':
		case 'i':
		case 'd':
			// This looks odd but it is quicker than isset()ing
			$end .= ';';
		case 'a':
		case 'O':
			$end .= '}';
			if ($value[1] !== ':'){
				return false;
			}
			switch ($value[2]){
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				break;
				default:
					return false;
			}
		case 'N':
			$end .= ';';
			if ($value[$length - 1] !== $end[0]){
				return false;
			}
		break;
		default:
			return false;
	}
	if(($result = @unserialize($value)) === false){
		$result = null;
		return false;
	}
	return true;
}
/*-------------------------------------------------------------------*/
/*-------------------------------------------------------------------*/
/* Custom code here */
 
/*-------------------------------------------------------------------*/
/*-------------------------------------------------------------------*/
?>