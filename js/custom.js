jQuery(document).ready(function(){
	jQuery('input.example-datepicker').datepicker();	 
}); 
  
jQuery( function(){
    var submit_actor = null;
    var form_name = jQuery( 'form#hotels-filter' );
    var submit_button = form_name.find( 'input[type=submit]' );
 
    form_name.submit( function( event ){
		if( null === submit_actor ){ 
			submit_actor = submit_button[0];
		} 
		
		var choose_action = submit_actor.name;
		
		if( choose_action == 'filter_months_year' ){
			
			var month = jQuery('select#filter-by-month').val();	 
			var year = jQuery('select#filter-by-year').val();	  
			  
			if( month != 0 ){
				if( year == 0 ){ 
					alert('Sorry, Please select Year to filter!');
					jQuery('select#filter-by-year').focus();
					return false;
				} 
			}   
			if( month == 0  ){ 
				if( year == 0 ){ 
					alert('Sorry, Please select Month and Year to filter!');
					jQuery('select#filter-by-month').focus();
					return false;
				}  
			} 
			
		} else if( choose_action == 'filter_date' ){
			var start_date = jQuery('input#start_date').val();	  
			var end_date = jQuery('input#end_date').val();	  
			 
			if( start_date == '' && end_date == '' ){  
				alert('Sorry, Please select date to filter!');
				jQuery('input#start_date').focus();
				return false; 
			} else if( start_date != '' && end_date == '' ){  
				jQuery('input#end_date').val( start_date );	
			} else if( start_date == '' && end_date != '' ){  
				alert('Sorry, Please select start date to filter!');
				jQuery('input#end_date').focus();
				return false; 
			}  
		}  
    });

    submit_button.click( function( event ){
		submit_actor = this;
    });  
}); 

jQuery('form#income_month_year').submit(function( event ){
	var month = jQuery('select#filter-by-month').val();	 
	var year = jQuery('select#filter-by-year').val();	  
	var submitActor = null;
	
	if( month != 0 ){
		if( year == 0 ){ 
			alert('Sorry, Please select Year to filter!');
			jQuery('select#filter-by-year').focus();
			return false;
		} 
	}  
	
	if( month == 0  ){ 
		if( year == 0 ){ 
			alert('Sorry, Please select Year to filter!');
			jQuery('select#filter-by-year').focus();
			return false;
		}  
	} 
}); 

jQuery('form#statistics_month_year').submit(function( event ){
	var month = jQuery('select#filter-by-month').val();	 
	var year = jQuery('select#filter-by-year').val();	  
	var submitActor = null;
	
	if( month != 0 ){
		if( year == 0 ){ 
			alert('Sorry, Please select Year to filter!');
			jQuery('select#filter-by-year').focus();
			return false;
		} 
	}  
	
	if( month == 0  ){ 
		if( year == 0 ){ 
			alert('Sorry, Please select Year to filter!');
			jQuery('select#filter-by-year').focus();
			return false;
		}  
	} 
}); 

jQuery('form#income_date_range').submit(function(){
	 
	var start_date = jQuery('input#start_date').val();	  
	var end_date = jQuery('input#end_date').val();	  
	 
	 
	var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
	var firstDate = new Date(start_date);
	var secondDate = new Date(end_date);

	var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
	console.log( diffDays );
	if( start_date == '' && end_date == '' ){  
		alert('Sorry, Please select date to filter!');
		jQuery('input#start_date').focus();
		return false; 
	} else if( start_date != '' && end_date == '' ){  
		jQuery('input#end_date').val( start_date );	
	} else if( start_date == '' && end_date != '' ){  
		alert('Sorry, Please select start date to filter!');
		jQuery('input#end_date').focus();
		return false; 
	} else if( start_date != '' && end_date != '' ){  
		if( diffDays > 31 ){
			alert('Sorry, You have exceeded the number of days it should not exceed to 31 days!');
			jQuery('input#end_date').focus();
			return false;  
		}
	} 
}); 

jQuery('form#statistics_date_range').submit(function(){
	 
	var start_date = jQuery('input#start_date').val();	  
	var end_date = jQuery('input#end_date').val();	  
	 
	var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
	var firstDate = new Date(start_date);
	var secondDate = new Date(end_date);

	var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
	console.log( diffDays );
	
	if( start_date == '' && end_date == '' ){  
		alert('Sorry, Please select date to filter!');
		jQuery('input#start_date').focus();
		return false; 
	} else if( start_date != '' && end_date == '' ){  
		jQuery('input#end_date').val( start_date );	
	} else if( start_date == '' && end_date != '' ){  
		alert('Sorry, Please select start date to filter!');
		jQuery('input#end_date').focus();
		return false; 
	} else if( start_date != '' && end_date != '' ){  
		if( diffDays > 31 ){
			alert('Sorry, You have exceeded the number of days it should not exceed to 31 days!');
			jQuery('input#end_date').focus();
			return false;  
		}
	}
}); 