<?php
class HotelAPI { 

	function get_json_hotel_details( $hotel_code ){
		
		$apiKey = "qg36upzve4ecr82y569y6gez";
		$sharedSecret = "9RMN3QZzzq"; 
		$signature = hash("sha256", $apiKey.$sharedSecret.time());
		  
		$endpoint = 'https://api.test.hotelbeds.com/hotel-content-api/1.0/hotels/'.$hotel_code.'?language=ENG';
	  
		$headerfields = array(
			'Api-Key:'.$apiKey, 
			'X-Signature:'.$signature, 
			'Accept:application/json',
			'Content-Type:application/json',
		); 

		$ch = curl_init($endpoint);
			assert(curl_setopt_array($ch,
				array(
					CURLOPT_AUTOREFERER => true,
					CURLOPT_BINARYTRANSFER => true,
					CURLOPT_COOKIESESSION => true,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_FORBID_REUSE => false,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_SSL_VERIFYPEER => false, 
					CURLOPT_HTTPHEADER=>$headerfields,  
					CURLOPT_CUSTOMREQUEST=>'GET',
					CURLOPT_HEADER=>false, 
					CURLINFO_HEADER_OUT=>false,    
			)));  
			 
		$response = curl_exec($ch);
		
		$decoded_curl_result = html_entity_decode( $response );
		$json_data = json_decode($decoded_curl_result, true);
		 
		curl_close($ch);   
		return $json_data;
		 
	}
}
?>