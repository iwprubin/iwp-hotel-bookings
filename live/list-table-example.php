<?php
/*
Plugin Name: iWP Hotel Bookings List
Plugin URI: http://iwebprovider.com/
Description: A highly documented plugin that demonstrates how to create custom List Tables using official WordPress APIs.
Version: 1.0
Author: Rubin Klain iWP , Charlene iWP
Author URI:  http://iwebprovider.com/
License: GPL2
*/
/*  Copyright 2015  Rubin Klain iWP  (email : rubin@iwebprovider.com )

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
 
/* == NOTICE ===================================================================
 * Please do not alter this file. Instead: make a copy of the entire plugin, 
 * rename it, and work inside the copy. If you modify this plugin directly and 
 * an update is released, your changes will be lost!
 * ========================================================================== */
 
/*************************** LOAD THE BASE CLASS *******************************
 *******************************************************************************
 * The WP_List_Table class isn't automatically available to plugins, so we need
 * to check if it's available and load it if necessary. In this tutorial, we are
 * going to use the WP_List_Table class directly from WordPress core.
 *
 * IMPORTANT:
 * Please note that the WP_List_Table class technically isn't an official API,
 * and it could change at some point in the distant future. Should that happen,
 * I will update this plugin with the most current techniques for your reference
 * immediately.
 *
 * If you are really worried about future compatibility, you can make a copy of
 * the WP_List_Table class (file path is shown just below) to use and distribute
 * with your plugins. If you do that, just remember to change the name of the
 * class to avoid conflicts with core.
 *
 * Since I will be keeping this tutorial up-to-date for the foreseeable future,
 * I am going to work with the copy of the class provided in WordPress core.
 */
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
} 

if(!class_exists('ApproveEmail')){
    require_once( ABSPATH . 'wp-content/plugins/iwp-hotel-bookings/email-templates/approve-email.php' );
}

if(!class_exists('CancelEmail')){
    require_once( ABSPATH . 'wp-content/plugins/iwp-hotel-bookings/email-templates/cancel-email.php' );
}

if(!class_exists('NotificationEmail')){
    require_once( ABSPATH . 'wp-content/plugins/iwp-hotel-bookings/email-templates/notification-email.php' );
} 

if(!class_exists('PDFAttachment')){
    require_once( ABSPATH . 'wp-content/plugins/iwp-hotel-bookings/email-templates/pdf-attachment.php' );
}

if(!class_exists('BookHotelXML')){
    require_once( ABSPATH . 'wp-content/plugins/iwp-hotel-bookings/xml/bookhotelxml.php' ); 
} 

 

// require_once( ABSPATH . 'wp-content/plugins/iwp-hotel-bookings/custom-functions.php' );
/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 * 
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 * 
 * Our theme for this list table is going to be hotels.
 */
class iWP_Hotel_Bookings_List_Table extends WP_List_Table {
    
    /** ************************************************************************
     * Normally we would be querying data from a database and manipulating that
     * for use in your list table. For this example, we're going to simplify it
     * slightly and create a pre-built array. Think of this as the data that might
     * be returned by $wpdb->query()
     * 
     * In a real-world scenario, you would make your own custom query inside
     * this class' prepare_items() method.
     * 
     * @var array 
     **************************************************************************/
	 

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We 
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page; 
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'booking',     //singular name of the listed records
            'plural'    => 'bookings',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }


    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title() 
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as 
     * possible. 
     * 
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     * 
     * For more detailed insight into how columns are handled, take a look at 
     * WP_List_Table::single_row_columns()
     * 
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name){
        switch($column_name){
            case 'book_id':
				return $item[$column_name];
			case 'fullname': 
                return $item[$column_name]; 
            case 'Name':
				return $item[$column_name];
            case 'bkd_destination':   
                return $item[$column_name]; 
            case 'booking_date': 
				return $item[$column_name]; 
			case 'bkd_total_amount':
				return $item[$column_name];
			case 'book_status':      
				return $item[$column_name]; 
			case 'view_action':      
				return $item[$column_name]; 
			case 'book_created_date':      
				return $item[$column_name]; 
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }
   

    /** ************************************************************************
     * Recommended. This is a custom column method and is responsible for what
     * is rendered in any column with a name/slug of 'title'. Every time the class
     * needs to render a column, it first looks for a method named 
     * column_{$column_title} - if it exists, that method is run. If it doesn't
     * exist, column_default() is called instead.
     * 
     * This example also illustrates how to implement rollover actions. Actions
     * should be an associative array formahotel_bookingsed as 'slug'=>'link html' - and you
     * will need to generate the URLs yourself. You could even ensure the links
     * 
     * 
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (hotel title only)
     **************************************************************************/
    function column_cppt_order_reference($item){
        
        //Build row actions
        $actions = array(
            'view'      => sprintf('<a href="?page=%s&action=%s&id=%s">View</a>','iwp_view_hotel_bookings_list_page','view',$item['book_id']),
            'delete'    => sprintf('<a href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['book_id']),
        );
        
        //Return the title contents
        // return sprintf('%2$s <span style="color:silver">(id:%2$s)</span>%3$s',
            // /*$1%s*/ $item['HotelCode'],
            // /*$2%s*/ $item['cppt_order_reference'],
            // /*$3%s*/ $this->row_actions($actions)
        // ); 
		
		 return sprintf('%2$s %3$s',
            /*$1%s*/ $item['HotelCode'],
            /*$2%s*/ $item['book_id'],
            /*$3%s*/ $this->row_actions($actions)
        
        );
    }



    /** ************************************************************************
     * REQUIRED if displaying checkboxes or using bulk actions! The 'cb' column
     * is given special treatment when columns are processed. It ALWAYS needs to
     * have it's own method.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (hotel title only)
     **************************************************************************/
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("hotel")
            /*$2%s*/ $item['book_id']                //The value of the checkbox should be the record's id
        );
    }


    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value 
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     * 
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        $columns = array( 
			// 'cb'                    => '<input type="checkbox" />', //Render a checkbox instead of text 
			'cb'   	   		 		=> '<input type="checkbox" />',
			'book_id'   	   		=> 'ID', 
			'fullname'  		    => 'Full Name', 
            'Name'    				=> 'Hotel Name',
			'bkd_destination'       => 'Destination', 
            'booking_date'          => 'Booking Date', 
			'bkd_total_amount'  	=> 'Amount',
            'book_status'        	=> 'Status',
            'view_action'        	=> 'Action',
            'book_created_date'        	=> 'Created' 
        );
        return $columns; 
    }


    /** ************************************************************************
     * Optional. If you want one or more columns to be sortable (ASC/DESC toggle), 
     * you will need to register it here. This should return an array where the 
     * key is the column that needs to be sortable, and the value is db column to 
     * sort by. Often, the key and value will be the same, but this is not always
     * the case (as the value is a column name from the database, not the list table).
     * 
     * This method merely defines which columns should be sortable and makes them
     * clickable - it does not handle the actual sorting. You still need to detect
     * the ORDERBY and ORDER querystring variables within prepare_items() and sort
     * your data accordingly (usually by modifying your query).
     * 
     * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
     **************************************************************************/
    function get_sortable_columns() {
        $sortable_columns = array(
			'book_id'    => array('book_id',false),    //true means it's already sorted 
			'Name'    => array('Name',false),     
			'fullname'    => array('fullname',false),     
			'bkd_total_amount'    => array('bkd_total_amount',false),     
			'bkd_destination'    => array('bkd_destination',false) 
        );
        return $sortable_columns;
    }


    /** ************************************************************************
     * Optional. If you need to include bulk actions in your list table, this is
     * the place to define them. Bulk actions are an associative array in the format
     * 'slug'=>'Visible Title'
     * 
     * If this method returns an empty value, no bulk action will be rendered. If
     * you specify any bulk actions, the bulk actions box will be rendered with
     * the table automatically on display().
     * 
     * Also note that list tables are not automatically wrapped in <form> elements,
     * so you will need to create those manually in order for bulk actions to function.
     * 
     * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
     **************************************************************************/
	 
	 function display_tablenav( $which ) { 
		?>
		<div class="tablenav <?php echo esc_attr($which); ?>"> 
			<?php 
			$this->bulk_actions( $which ); 
			$this->extra_tablenav( $which );   
			$this->pagination($which); 
			?>
			<br class="clear" />
			 
		</div>
		<?php
	}

	/**
	 * Disables the views for 'side' context as there's not enough free space in the UI
	 * Only displays them on screen/browser refresh. Else we'd have to do this via an AJAX DB update.
	 * 
	 * @see WP_List_Table::extra_tablenav()
	 */
	function extra_tablenav( $which ){
		global $wpdb, $wp_meta_boxes;
	 
		if( $which == "top" ){
			  iwp_months_year_dropdown(); 
			  submit_button( __( 'Filter' ), 'secondary', 'filter_months_year', false, array( 'id' => 'get-filter-months-year-submit' ) );
			  
			  iwp_date_selection();
			  submit_button( __( 'Filter' ), 'secondary', 'filter_date', false, array( 'id' => 'get-filter-date-submit' ) );
			   
		}elseif ( $which == "bottom" ){
			
			$querydata = get_database_records(); 
			$sum_amount = 0;
			if( $querydata ){
				foreach( $querydata as $querydatum ){ 
		  
					$total_amount = $querydatum->bkd_total_amount; 
					$sum_amount += $total_amount;
				} 
				$sum_amount = round( $sum_amount, 2 );   
			} 
			?>
			<div style="text-align: right;">
				<h2>Total Amount PHP: <?php echo number_format( $sum_amount,2,'.',',' ); ?> </h2>
			</div>
			<?php
			
		}
	}
	
    function get_bulk_actions() {
        $actions = array( 
            'confim_m'    => 'Confim', 
            'delete'    => 'Delete' 
        );  
        return $actions;
    }
	

    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     * 
     * @see $this->prepare_items()
     **************************************************************************/
    function process_bulk_action() {
        
        //Detect when a bulk action is being triggered...
		if( 'refund'===$this->current_action() ){ 
			global $wpdb;    
			$page = $_REQUEST['page']; 
			$id = $_GET['id']; 
			if( $id ){
				$booking_trash = $wpdb->update( 'wp_cribsandtrips_booking', 
					array(  
						'book_refund' => 0 
					), 
					array( 'book_id' => $id  ) 
				);  
			}  
			?> 
			<script>
			window.location.href='http://cribsandtrips.com/wp-admin/admin.php?page=<?php echo $page; ?>';
			</script>
			<?php 
		} elseif( 'delete'===$this->current_action() ){ 
			global $wpdb;    
			$page = $_REQUEST['page']; 
			$booking = $_GET['booking'];
			foreach( $booking as $book ) {
				$booking_trash = $wpdb->update( 'wp_cribsandtrips_booking', 
					array(  
						'book_status' => 'trashed' 
					), 
					array( 'book_id' => $book  ) 
				);  
            }
			?> 
			<script>
			window.location.href='http://cribsandtrips.com/wp-admin/admin.php?page=<?php echo $page; ?>';
			</script>
			<?php 
		} elseif( 'confim_m'===$this->current_action() ){ 
			global $wpdb;    
			$page = $_REQUEST['page']; 
			$booking = $_GET['booking'];
			foreach( $booking as $book ) { 
				$result = get_records( $book ); 
				foreach( $result as $details ){ 
					$book_details_tblpref = 'bkd_';
					$book_tblpref = 'book_';
				 
					$book_status = get_value( $details, $book_tblpref.'status' ); 
					if( $book_status == 'paid' ){ 
						$cub_first_name = get_value( $details, $book_tblpref.'first_name' );
						$cub_middle_name = get_value( $details, $book_tblpref.'middle_name' );
						$cub_last_name = get_value( $details, $book_tblpref.'last_name' );
						$cub_contact_no = get_value( $details, $book_tblpref.'contact_no' );
						$cub_email = get_value( $details, $book_tblpref.'email' );
						$cub_birthday = get_value( $details, $book_tblpref.'birthday' );
						$cub_country_code = get_value( $details, $book_tblpref.'country' );
						$cub_country = get_country_name( $cub_country_code );
						
						$purchase_file_number = get_value( $details,$book_tblpref.'file_number' );
						$service_filenumber = get_value( $details,$book_details_tblpref.'reference' );
						$purchase_incoming_office = get_value( $details,$book_details_tblpref.'incoming_office' );
						
						$bkd_id = get_value( $details, 'bkd_id' );
						$hotel_code = get_value( $details, 'HotelCode' );
						$destination = get_value( $details,$book_details_tblpref.'destination' ); 
						$passenger_firstname = get_value( $details,$book_details_tblpref.'passenger_firstname' );
						$passenger_lastname = get_value( $details,$book_details_tblpref.'passenger_lastname' );	
						$passenger_firstname_child = get_value( $details,$book_details_tblpref.'passenger_firstname_child' );	
						$passenger_lastname_child = get_value( $details,$book_details_tblpref.'passenger_lastname_child' );	 
						$children_ages = get_value( $details,$book_details_tblpref.'children_ages' );
						$passenger_firstname = html_entity_decode( $passenger_firstname, ENT_QUOTES );  
						$passenger_lastname = html_entity_decode( $passenger_lastname, ENT_QUOTES ); 
						$passenger_firstname_child = html_entity_decode( $passenger_firstname_child, ENT_QUOTES ); 
						$passenger_lastname_child = html_entity_decode( $passenger_lastname_child, ENT_QUOTES ); 
						$children_ages = html_entity_decode( $children_ages, ENT_QUOTES ); 
						$passenger_first_name_details = check_if_serialized( ( $passenger_firstname ) );   
						$passenger_last_name_details = check_if_serialized( ( $passenger_lastname ) );
						$passenger_first_name_child_details = check_if_serialized( ( $passenger_firstname_child ) );
						$passenger_last_name_child_details = check_if_serialized( ( $passenger_lastname_child ) );
						$children_ages = check_if_serialized( ( $children_ages ) );
						
						if( $passenger_first_name_child_details ){
							$first_name = array_merge($passenger_first_name_details, $passenger_first_name_child_details);
						} else {
							$first_name = $passenger_first_name_details;
						}
						
						if( $passenger_last_name_child_details ){
							$last_name = array_merge($passenger_last_name_details, $passenger_last_name_child_details);
						} else {
							$last_name = $passenger_last_name_details;
						}
						 
						$length_first = count( $first_name );
						$length_last = count( $last_name );
						$fullname = array();
						
						if( $length_first > $length_last ){
							$base_count = $length_first;
						} else {
							$base_count =  $length_last;
						}
						
						for( $x=0; $x< $base_count;$x++ ){
							$first = preg_replace('/[^A-Za-z0-9\- \']/', '', $first_name[$x]);
							$second = preg_replace('/[^A-Za-z0-9\- \']/', '', $last_name[$x]);
							array_push( $fullname, $first.' '.$second);
						}
						$passenger = $fullname ; 
						$checkindate = get_value( $details,$book_details_tblpref.'start_date' ); 
						$checkoutdate = get_value( $details,$book_details_tblpref.'end_date' );
						
						$room = get_value( $details,$book_details_tblpref.'room' );
						$room_count = get_value( $details,$book_details_tblpref.'room' );
						$adult = get_value( $details,$book_details_tblpref.'adult' );
						$adult_count = get_value( $details,$book_details_tblpref.'adult' );
						$child = get_value( $details,$book_details_tblpref.'child' );
						$child_count = get_value( $details,$book_details_tblpref.'child' );
						
						$room_type = get_value( $details,$book_details_tblpref.'room_type' );
						$room_board = get_value( $details,$book_details_tblpref.'room_board' );
						$room_occupancy = get_value( $details,$book_details_tblpref.'room_occupancy' );
						$room_amount = get_value( $details,$book_details_tblpref.'room_amount_no_discount' );
						$discount_name = get_value( $details,$book_details_tblpref.'discount_name' );
						$discount_period = get_value( $details,$book_details_tblpref.'discount_period' );
						$discount_occupancy_amount = get_value( $details,$book_details_tblpref.'discount_occupancy_amount' );
						$total_discount_amount = get_value( $details,$book_details_tblpref.'total_discount_amount' );
						$get_comment = get_value( $details,$book_details_tblpref.'contract_comment' );
						$contract_comment = preg_replace('/[^A-Za-z0-9\-., \']/', ' ', $get_comment);
						$cancellation_policy_time = get_value( $details,$book_details_tblpref.'cancellation_policy_time' );
						$cancellation_policy_date = get_value( $details,$book_details_tblpref.'cancellation_policy_date' );
						$cancellation_policy_amount = get_value( $details,$book_details_tblpref.'cancellation_policy_amount' );
						$cancellation_policy_amount_currency = get_value( $details,$book_details_tblpref.'cancellation_policy_amount_currency' );
						$agency_reference = get_value( $details,$book_details_tblpref.'agency_reference' );
						$total_amount_currency = get_value( $details,$book_details_tblpref.'total_amount_currency' );
						$total_amount = get_value( $details,$book_details_tblpref.'total_amount' );
						$created = get_value( $details, $book_tblpref.'created' );
						$modified = get_value( $details, $book_tblpref.'modified' );
						$published = get_value( $details, $book_tblpref.'published' );
						$hotel_code = get_value( $details, 'HotelCode' );
						$hotel_name = get_value( $details, 'Name' );
						
						
						if( $room_count > 1 ){
							$room_count .= ' rooms';
						} else {
							$room_count .= ' room';
						}
						
						if( $adult_count > 1 ){
							$adult_count .= ' adults';
						} else {
							$adult_count .= ' adult';
						}
						
						if( $child_count > 0){
							if( $child_count > 1 ){
							$child_count .= ' children';
							} else {
								$child_count .= ' child';
							}
						} else {
							$child_count = Null;
						}
						
						$summary_occupancy = $room_count.' '.$adult_count.' '.$child_count;  
						 
						$bookings_data =  array( 	         
							'booking_reference' => $service_filenumber,        
							'service_ref' => $purchase_file_number,      
							'incoming_office' => $purchase_incoming_office,      
							'reference_number' => $purchase_incoming_office.'-'.$purchase_file_number, 			
							'booker_first_name' => $cub_first_name,      
							'booker_middle_name' => $cub_middle_name,     
							'booker_last_name' => $cub_last_name,       
							'booker_email' => $cub_email,     
							'booker_contact_no' => $cub_contact_no,       
							'booker_country' => $cub_country,    			
							'holder_name' => $holder_name,       
							'holder_last_name' => $holder_last_name,       
							'hotel_name' => $hotel_name,       
							'hotel_category' => $category_code,       
							'hotel_address' => $destination_name.' '.$destination_zone,       
							'hotel_availableroom' => $services,       
							'room_type' => $hotel_room_room_type,            
							'room_count' => $room_count,            
							'board' => $hotel_room_board,            
							'board_type' => $hotel_room_board,            
							'summary_occupancy' => $summary_occupancy,     
							'occupancy' => $hotel_room_occupancy,     
							'price' => $currency_response.' '.$hotel_room_amount,    
							'item_amount' => $currency_response.' '.$item_amount,    
							'contract_remarks' => $contract_comment,    
							'total_price' => $currency_response.' '.$total_amount,   
							'currency_response' => $currency_response,   
							'agency_reference' => $agency_reference,    
							'service_description' => $hotel_name,   
							'check_in' => $checkindate,   
							'check_out' => $checkoutdate,       
							'cancellation_date' => $cancellation_policy_date,   
							'cancellation_policy_time' => $cancellation_policy_time,   
							'cancellation_charge' => $currency_response.' '.$cancellation_policy_amount,
							'latitude' => $hotel_latitude,  
							'longitude' => $hotel_longitude,   
							'adults' => $customer_list_adult_count,   
							'children_ages' => $customer_list_child_age_array,   
							'children' => $customer_list_child_count,   
							'hotel_telephone' => $hotel_contact_number,    
							'hotel_fax' => $hotel_fax_number,    
							'final_price' => $final_price,    
							'hotel_email' => $hotel_email,
							'total_net_amount' => $final_total,
							'service_charge' => $service_charge,
							'tax_value' => $tax_value,
							'web_admin_fee' => $web_admin_fee,
							'book_or_number' => $book_or_number,
							'book_invoice_number' => $book_invoice_number,
							'booking_id' => $book  
						);  
						 
						successful_approve_email_to_client( $bookings_data );
						successful_approve_email_to_admin( $bookings_data );
						
						$booking_trash = $wpdb->update( 'wp_cribsandtrips_booking', 
							array(  
								'book_status' => 'confirmed' 
							), 
							array( 'book_id' => $book  ) 
						);  
					} else {
						?> 
						<script>
							alert("Booking ID <?php echo $book; ?> can not be confirmed." )
						</script>
						<?php  
					}
				}  
            }
			?> 
			<script>
			window.location.href='http://cribsandtrips.com/wp-admin/admin.php?page=<?php echo $page; ?>';
			</script>
			<?php 
        } elseif( 'view'===$this->current_action() ){ 
			echo iwp_view_hotel_bookings_list_page();
			exit; 
        } elseif( 'cancel'===$this->current_action() ){ 
			echo iwp_cancel_hotel_bookings_list_page();
			exit;  
        } elseif( 'confirm_cancel'===$this->current_action() ){ 
			echo iwp_confirm_cancel_hotel_bookings_list_page();
			exit;  
        } elseif( 'confirmed'===$this->current_action() ){ 
			$page = $_REQUEST['page']; 
			echo iwp_confirmed_hotel_booking();  
        } elseif( 'resend_voucher_client'===$this->current_action() ){ 
			echo resend_voucher_client();
			echo iwp_view_hotel_bookings_list_page( 'view' );
			exit;  
        } elseif( 'resend_voucher_admin'===$this->current_action() ){ 
			echo resend_voucher_admin();
			echo iwp_view_hotel_bookings_list_page( 'view' );
			exit;  
        } elseif( 'resend_receipt_client'===$this->current_action() ){  
			echo resend_receipt_client();  
			echo iwp_view_hotel_bookings_list_page( 'view' );
			exit;  
        } elseif( 'resend_invoice_admin'===$this->current_action() ){ 
			echo resend_invoice_admin();
			echo iwp_view_hotel_bookings_list_page( 'view' );
			exit;  
        }  
    } 
	
	
    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     * 
     * @global WPDB $wpdb
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items() {
        
 		$querydata = get_database_records();
		$data=array();
		$hotel_api = new HotelAPI();
		
		foreach( $querydata as $querydatum ){ 
			 
			$first_name = $querydatum->book_first_name; 
			$last_name = $querydatum->book_last_name; 
			$hotel_code = $querydatum->HotelCode; 
			$hotel_name = $querydatum->Name;  
			$start_date = $querydatum->bkd_start_date; 
			$end_date = $querydatum->bkd_end_date; 
			$created = $querydatum->book_created;   
			$created = date( "Y-m-d", strtotime( $created ) ); 
			
			if( $hotel_name == '' || !isset( $querydatum->Name ) ){  
				$api_file_hotels_details = iwp_get_hotel_details( $hotel_code ); 
				$hotel_detail_rs = get_value( $api_file_hotels_details, 'hotel' );  
				$hotel_name_array = get_value( $hotel_detail_rs, 'name' );  
				$hotel_name = get_value( $hotel_name_array, 'content' );   
			}
			$querydatum->Name = $hotel_name;	
		  
			$book_id = $querydatum->book_id; 
			$service_status = $querydatum->bkd_service_status; 
			$total_amount = $querydatum->bkd_total_amount; 
			$status = $querydatum->book_status;  
			$refund = $querydatum->book_refund;  
			if( $refund == 1 ){ 
				$new_status = $status.', <span style="color:#870217">refund</span>';
				$querydatum->book_status = $new_status; 
			} 
			$page = $_REQUEST['page'];
			  
			$booking_status = booking_status(  $status, $book_id, $service_status, $refund );
			$view_action = sprintf('<a href="?page=%s&action=%s&id=%s">View</a>',$page,'view',$book_id );
		  
			$querydatum->bkd_total_amount = number_format( $total_amount,2,'.',',' );  
			$querydatum->book_created_date = $created; 
			$querydatum->view_action = $view_action.$booking_status; 
			$querydatum->fullname = $last_name.', '.$first_name; 
			$querydatum->booking_date = $start_date.' '.$end_date; 

			  
			array_push( $data, (array)$querydatum );
		}
		 
		  
        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 15;
        
        
        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        
        
        /**
         * REQUIRED. Finally, we build an array to be used by the class for column 
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        
        /**
         * Optional. You can handle your bulk actions however you see fit. In this
         * case, we'll handle them within our package just to keep things clean.
         */ 
        $this->process_bulk_action();

        /**
         * Instead of querying a database, we're going to fetch the example data
         * property we created for use in this plugin. This makes this example 
         * package slightly different than one you might build on your own. In 
         * this example, we'll be using array manipulation to sort and paginate 
         * our data. In a real-world implementation, you will probably want to 
         * use sort and pagination data to build a custom query instead, as you'll
         * be able to use your precisely-queried data immediately.
         */
        // $data = $this->example_data;
                
       
		
        /**
         * This checks for sorting input and sorts the data in our array accordingly.
         * 
         * In a real-world situation involving a database, you would probably want 
         * to handle sorting by passing the 'orderby' and 'order' values directly 
         * to a custom query. The returned data will be pre-sorted, and this array
         * sorting technique would be unnecessary.
         */
        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'book_created'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='desc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');
        
        
        /***********************************************************************
         * ---------------------------------------------------------------------
         * vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
         * 
         * In a real-world situation, this is where you would place your query.
         *
         * For information on making queries in WordPress, see this Codex entry:
         * hhotel_bookingsp://codex.wordpress.org/Class_Reference/wpdb
         * 
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         * ---------------------------------------------------------------------
         **********************************************************************/
        
                
        /**
         * REQUIRED for pagination. Let's figure out what page the user is currently 
         * looking at. We'll need this later, so you should always include it in 
         * your own package classes.
         */
		          
		 
        $current_page = $this->get_pagenum();
        
        /**
         * REQUIRED for pagination. Let's check how many items are in our data array. 
         * In real-world use, this would be the total number of items in your database, 
         * without filtering. We'll need this later, so you should always include it 
         * in your own package classes.
         */
        $total_items = count($data);
        
        
        /**
         * The WP_List_Table class does not handle pagination for us, so we need
         * to ensure that the data is trimmed to only the current page. We can use
         * array_slice() to 
         */
        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
        
        
        
        /**
         * REQUIRED. Now we can add our *sorted* data to the items property, where 
         * it can be used by the rest of the class.
         */
        $this->items = $data;
        
        
        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( 
			array( 
				'total_items' => $total_items,                  //WE have to calculate the total number of items
				'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
				'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
			) 
		); 
    } 
}





/** ************************ REGISTER THE TEST PAGE ****************************
 *******************************************************************************
 * Now we just need to define an admin page. For this example, we'll add a top-level
 * menu item to the bohotel_bookingsom of the admin menus.
 */
function iwp_hotel_bookings_add_menu_items(){
    add_menu_page('iWP Hotel Bookings List', 'Hotel Bookings', 'activate_plugins', 'iwp_hotel_bookings_list', 'iwp_hotel_bookings_render_list_page', 'dashicons-building' , 24 ); 
							
	add_submenu_page( 'iwp_hotel_bookings_list', 'Hotel Bookings Income Graph', 'Income Graph', 'manage_options', 'iwp_hotel_bookings_income_graph', 'iwp_hotel_bookings_render_income_graph' );
	
	add_submenu_page( 'iwp_hotel_bookings_list', 'Hotel Bookings Statistics Graph', 'Statistics Graph', 'manage_options', 'iwp_hotel_bookings_statistics_graph', 'iwp_hotel_bookings_render_statistics_graph' );
}  

function iwp_hotel_bookings_scripts() {
	$page = $_REQUEST['page'];
	$action = $_REQUEST['action']; 
	$filter = $_REQUEST['filter']; 
	if( $page == 'iwp_hotel_bookings_list' && $action == 'cancel' ){
		$plugin_dir_url = plugin_dir_url( __FILE__ );    
		wp_register_style( 'iwp-hotel-bookings-bootstrap-style',  plugin_dir_url( __FILE__ ) . 'css/iwp-hotel-bookings-bootstrap.css' );
		wp_enqueue_style( 'iwp-hotel-bookings-bootstrap-style' );  
	}   
}

function enqueue_date_picker(){ 
	wp_enqueue_script( 'field-date-js', plugin_dir_url( __FILE__ ) . 'js/custom.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'), time(), true );	 
	 
	wp_enqueue_style('e2b-admin-ui-css',plugin_dir_url( __FILE__ ) . 'css/e2b-admin-ui-css.css' ,false,"1.9.0",false); 
	wp_enqueue_style('custom-css',plugin_dir_url( __FILE__ ) . 'css/custom.css' ,false,false,false); 
}
 
add_action( 'admin_enqueue_scripts', 'enqueue_date_picker' );
add_action('admin_menu', 'iwp_hotel_bookings_add_menu_items');
add_action( 'admin_init', 'iwp_hotel_bookings_scripts' ); 


function get_records( $id ){
	global $wpdb; //This is used only if making any database queries  
	if( $id ){
		$sql = "SELECT *"
		." FROM `wp_cribsandtrips_booking` AS `a`"  
		." LEFT JOIN `wp_cribsandtrips_booking_details` AS `b`"
		." ON `a`.`book_id`=`b`.`book_id`" 
		." LEFT JOIN `wp_cribsandtrips_hotels` AS `c`"
		." ON `b`.`HotelCode`=`c`.`HotelCode`" 
		." WHERE" 
		." `a`.`book_id`='".$id."'"    
		; 
		$result = $wpdb->get_results( $sql );
		
		return $result;
	}
	
}

function get_database_records(){
	
	global $wpdb; //This is used only if making any database queries
		  
	$status_type = $_GET['status_type'];  
	$month = $_GET['month'];  
	$year = $_GET['year'];  
	$filter_months_year = $_GET['filter_months_year'];  
	$filter_date = $_GET['filter_date'];  
	
	$start_date = $_GET['start_date'];  
	$end_date = $_GET['end_date'];  
		  
	$sql = "SELECT *"
		." FROM `wp_cribsandtrips_booking` AS `a`"  
		." LEFT JOIN `wp_cribsandtrips_booking_details` AS `b`"
		." ON `a`.`book_id`=`b`.`book_id`" 
		." LEFT JOIN `wp_cribsandtrips_hotels` AS `c`"
		." ON `b`.`HotelCode`=`c`.`HotelCode`" 
		." WHERE"  
		." `a`.`book_published`='1'"   
		;  			
	if( $status_type ){
		$sql .= " AND `a`.`book_status`='".$status_type."'"; 
	}
	
	if( $filter_months_year && $month && $filter_months_year == 'Filter' ){
		$sql .= " AND MONTH(`a`.`book_created`) = '".$month."' "; 
	}
	
	if( $filter_months_year && $year && $filter_months_year == 'Filter' ){
		$sql .= " AND YEAR(`a`.`book_created`) = '".$year."' "; 
	}
	
	if( $filter_date && $start_date && $end_date && $filter_date == 'Filter' ){ 
		$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
		$end_date = date( "Y-m-d", strtotime( $end_date ) );  
		 
		// $start_date = date( "Y-m-d H:i:s", strtotime( $start_date ) ); 
		// $end_date = date( "Y-m-d H:i:s", strtotime( $end_date ) );  
		
		$sql .= " AND ( DATE(`a`.`book_created`) BETWEEN '".$start_date."' AND '".$end_date."')";   
		 
	}
	 
	$sql .=" ORDER BY `a`.`book_created` DESC";
	 
	$querydata = $wpdb->get_results( $sql ); 
  
	return $querydata;
	
}

/** *************************** RENDER LIST TABLE PAGE *************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */
function iwp_hotel_bookings_render_list_page(){
	//Page name of the plugin
	$page = $_REQUEST['page']; 
    //Create an instance of our package class...
    $hotel_accommodations_list = new iWP_Hotel_Bookings_List_Table();
    //Fetch, prepare, sort, and filter our data...
    $hotel_accommodations_list->prepare_items();
	 
    ?>
	
    <div class="wrap"> 
		<div id="icon-users" class="icon32"><br/></div>
        <h2>Hotel Bookings</h2>
        <ul class="subsubsub">  
			<li class="all">
				<a href="?page=<?php echo $page;?>">All</a>
			</li>
			<li class="cancelled">
				&nbsp;|&nbsp;<a href="?page=<?php echo $page;?>&status_type=cancelled">Cancelled</a>
			</li>
			<li class="approve">
				&nbsp;|&nbsp;<a href="?page=<?php echo $page;?>&status_type=confirmed">Confirmed</a>
			</li>
			<li class="approve">
				&nbsp;|&nbsp;<a href="?page=<?php echo $page;?>&status_type=void">Void</a>
			</li>
			<li class="approve">
				&nbsp;|&nbsp;<a href="?page=<?php echo $page;?>&status_type=pending">Pending</a>
			</li>
			<li class="trashed">
				&nbsp;|&nbsp;<a href="?page=<?php echo $page;?>&status_type=trashed">Trashed</a>
			</li>
		</ul>
       <!--
	   <div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
            <p>This page demonstrates the use of the <hotel_bookings><a href="hhotel_bookingsp://codex.wordpress.org/Class_Reference/WP_List_Table" target="_blank" style="text-decoration:none;">WP_List_Table</a></hotel_bookings> class in plugins.</p> 
            <p>For a detailed explanation of using the <hotel_bookings><a href="hhotel_bookingsp://codex.wordpress.org/Class_Reference/WP_List_Table" target="_blank" style="text-decoration:none;">WP_List_Table</a></hotel_bookings>
            class in your own plugins, you can view this file <a href="<?php echo admin_url( 'plugin-editor.php?plugin='.plugin_basename(__FILE__) ); ?>" style="text-decoration:none;">in the Plugin Editor</a> or simply open <hotel_bookings style="color:gray;"><?php echo __FILE__ ?></hotel_bookings> in the PHP editor of your choice.</p>
            <p>Additional class details are available on the <a href="hhotel_bookingsp://codex.wordpress.org/Class_Reference/WP_List_Table" target="_blank" style="text-decoration:none;">WordPress Codex</a>.</p>
        </div> -->
        
        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
		<div id="iwp_wp_list_table">
			<form id="hotels-filter" method="get">
				<!-- For plugins, we also need to ensure that the form posts back to our current page -->
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
				<!-- Now we can render the completed list table -->
				<?php $hotel_accommodations_list->display() ?>
			</form>
        
		</div>
    </div>
    <?php
}

/** *************************** VIEW DETAILS PAGE ******************************
 *******************************************************************************
 *   
 */
function iwp_view_hotel_bookings_list_page( $force_action=null ){
 
	global $wpdb; //This is used only if making any database queries  
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 

	if( $id && $action == 'view' || $id && $force_action == 'view' ){
		 
		$result = get_records( $id );
	   
		foreach( $result as $details ){
			 
			$book_details_tblpref = 'bkd_';
			$book_tblpref = 'book_';
			
			$bkd_id = get_value( $details, 'bkd_id' );
			$hotel_code = get_value( $details, 'HotelCode' );
			$destination = get_value( $details,$book_details_tblpref.'destination' ); 
			$passenger_firstname = get_value( $details,$book_details_tblpref.'passenger_firstname' );
			$passenger_lastname = get_value( $details,$book_details_tblpref.'passenger_lastname' );	
			$passenger_firstname_child = get_value( $details,$book_details_tblpref.'passenger_firstname_child' );	
			$passenger_lastname_child = get_value( $details,$book_details_tblpref.'passenger_lastname_child' );	 
			$children_ages = get_value( $details,$book_details_tblpref.'children_ages' );
			$passenger_firstname = html_entity_decode( $passenger_firstname, ENT_QUOTES );  
			$passenger_lastname = html_entity_decode( $passenger_lastname, ENT_QUOTES ); 
			$passenger_firstname_child = html_entity_decode( $passenger_firstname_child, ENT_QUOTES ); 
			$passenger_lastname_child = html_entity_decode( $passenger_lastname_child, ENT_QUOTES ); 
			$children_ages = html_entity_decode( $children_ages, ENT_QUOTES ); 
			$passenger_first_name_details = check_if_serialized( ( $passenger_firstname ) );   
			$passenger_last_name_details = check_if_serialized( ( $passenger_lastname ) );
			$passenger_first_name_child_details = check_if_serialized( ( $passenger_firstname_child ) );
			$passenger_last_name_child_details = check_if_serialized( ( $passenger_lastname_child ) );
			$children_ages = check_if_serialized( ( $children_ages ) );
			
			if( $passenger_first_name_child_details ){
				$first_name = array_merge($passenger_first_name_details, $passenger_first_name_child_details);
			} else {
				$first_name = $passenger_first_name_details;
			}
			
			if( $passenger_last_name_child_details ){
				$last_name = array_merge($passenger_last_name_details, $passenger_last_name_child_details);
			} else {
				$last_name = $passenger_last_name_details;
			}
			 
			$length_first = count( $first_name );
			$length_last = count( $last_name );
			$fullname = array();
			
			if( $length_first > $length_last ){
				$base_count = $length_first;
			} else {
				$base_count =  $length_last;
			}
			
			for( $x=0; $x< $base_count;$x++ ){
				
				// $first = preg_replace('/[^A-Za-z0-9\- \']/', '', $first_name[$x]);
				$first = $first_name[$x];
				// $second = preg_replace('/[^A-Za-z0-9\- \']/', '', $last_name[$x]);
				$second = $last_name[$x];
				array_push( $fullname, $first.' '.$second);
			}
			$passenger = $fullname ; 
			$end_date = get_value( $details,$book_details_tblpref.'end_date' );
			$start_date = get_value( $details,$book_details_tblpref.'start_date' );
			$room = get_value( $details,$book_details_tblpref.'room' );
			$adult = get_value( $details,$book_details_tblpref.'adult' );
			$child = get_value( $details,$book_details_tblpref.'child' );
			
			$room_type = get_value( $details,$book_details_tblpref.'room_type' );
			$room_board = get_value( $details,$book_details_tblpref.'room_board' );
			$room_occupancy = get_value( $details,$book_details_tblpref.'room_occupancy' );
			$room_amount = get_value( $details,$book_details_tblpref.'room_amount_no_discount' );
			$discount_name = get_value( $details,$book_details_tblpref.'discount_name' );
			$discount_period = get_value( $details,$book_details_tblpref.'discount_period' );
			$discount_occupancy_amount = get_value( $details,$book_details_tblpref.'discount_occupancy_amount' );
			$total_discount_amount = get_value( $details,$book_details_tblpref.'total_discount_amount' );
			$get_comment = get_value( $details,$book_details_tblpref.'contract_comment' );
			$contract_comment = preg_replace('/[^A-Za-z0-9\-., \']/', ' ', $get_comment);
			$cancellation_policy_time = get_value( $details,$book_details_tblpref.'cancellation_policy_time' );
			$cancellation_policy_date = get_value( $details,$book_details_tblpref.'cancellation_policy_date' );
			$cancellation_policy_amount = get_value( $details,$book_details_tblpref.'cancellation_policy_amount' );
			$cancellation_policy_amount_currency = get_value( $details,$book_details_tblpref.'cancellation_policy_amount_currency' );
			$agency_reference = get_value( $details,$book_details_tblpref.'agency_reference' );
			$total_amount_currency = get_value( $details,$book_details_tblpref.'total_amount_currency' );
			$total_amount = get_value( $details,$book_details_tblpref.'total_amount' );
			$created = get_value( $details, $book_tblpref.'created' );
			$modified = get_value( $details, $book_tblpref.'modified' );
			$published = get_value( $details, $book_tblpref.'published' );
			$hotel_code = get_value( $details, 'HotelCode' );
			$hotel_name = get_value( $details, 'Name' );   
			$service_status = get_value( $details,$book_details_tblpref.'service_status' );
			$status = get_value( $details, $book_tblpref.'status' );
			$refund = get_value( $details, $book_tblpref.'refund' );
			$payment_type = get_value( $details, $book_tblpref.'payment' );
			  
			  
			$cancellation_policy_date_cv = html_entity_decode( $cancellation_policy_date, ENT_QUOTES );
			$cancellation_policy_amount_cv = html_entity_decode( $cancellation_policy_amount, ENT_QUOTES );
			$cancellation_policy_time = html_entity_decode( $cancellation_policy_time, ENT_QUOTES );
			$cancellation_policy_date_is = iwp_is_serialized( $cancellation_policy_date_cv );
			$cancellation_policy_amount_is = iwp_is_serialized( $cancellation_policy_amount_cv );
			$cancellation_policy_time_is = iwp_is_serialized( $cancellation_policy_time );
		  
			if( $published == TRUE ){
				$published = 'Yes';
			} elseif( $published == FALSE ){
				$published = 'No';
			}
		} 
		$view_action = sprintf('<a href="?page=%s">Back</a>', $page );
		?>
		<div class="wrap"> 
			<div id="icon-users" class="icon32"><br/></div>
			<h2>View Hotel Bookings</h2>
			<ul class="subsubsub">
				<li class="back"><a href="<?php echo '?page='.$page; ?>">Back</a></li> 
				<?php
				if( $service_status == 'CONFIRMED'){
					if( $status == 'paid' || $status == 'pending' ){
						$action1 = sprintf('<a href="?page=%s&action=%s&id=%s">Cancel</a>',$page,'cancel',$id ); 
						echo '<li class="presentation" > | '.$action1.'</li>';
					} 
					if( $status == 'paid' ){
						$action2 = sprintf('<a href="?page=%s&action=%s&id=%s">Approve</a>',$page,'approve',$id ); 
						echo '<li class="approve" > | '.$action2.'</li>';
					}
					if( $status == 'paid' || $status == 'confirmed' ){
						$action3 = sprintf('<a href="?page=%s&action=%s&id=%s">Resend Voucher Client</a>',$page,'resend_voucher_client',$id ); 
						echo '<li class="resend" > | '.$action3.'</li>';
					} 
					if( $status == 'paid' || $status == 'confirmed' ){
						$action4 = sprintf('<a href="?page=%s&action=%s&id=%s">Resend Voucher Admin</a>',$page,'resend_voucher_admin',$id ); 
						echo '<li class="resend" > | '.$action4.'</li>';
					} 
					if( $status == 'paid' || $status == 'confirmed' ){
						$action5 = sprintf('<a href="?page=%s&action=%s&id=%s">Resend Receipt Client</a>',$page,'resend_receipt_client',$id ); 
						echo '<li class="resend" > | '.$action5.'</li>';
					}
					if( $status == 'paid' || $status == 'confirmed' ){
						$action6 = sprintf('<a href="?page=%s&action=%s&id=%s">Resend Invoice Admin</a>',$page,'resend_invoice_admin',$id ); 
						echo '<li class="resend" > | '.$action6.'</li>';
					} 
				}  
				if( $refund && $refund == 1){
					$action7 = sprintf('<a href="?page=%s&action=%s&id=%s">Confirm Refund</a>',$page,'refund',$id ); 
					echo '<li class="resend" > | '.$action7.'</li>'; 
				}
				?>
			</ul> 
			<br /><br /> 
			<table class="form-table">
				<tbody>
					<tr>
						<th>
							<label>Agency Reference:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $agency_reference; ?></div> 
						</td>
						<th>
							<label>Status:</label>
						</th>
						<td>
							<div class="regular-text"> 
								<?php 
								if( $refund && $refund == 1){
									echo ucfirst($status.', Refund'); 
								} else {
									echo ucfirst($status); 
								} 
								?> 
							</div> 
						</td>
					</tr> 
					<tr>
						<th>
							<label>Booking Details ID:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $bkd_id; ?></div> 
						</td>
						<th>
							<label>Book ID:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $id; ?></div> 
						</td>
					</tr> 
					<tr>
						<th>
							<label>Hotel Name:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $hotel_name; ?></div> 
						</td>
						<th>
							<label>Destination:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $destination; ?></div> 
						</td>
					</tr> 
					<tr>
						<th>
							<label>Passenger:</label>
						</th>
						<td colspan="3">
							<div>
								<?php 
								if( $passenger ){
									for( $x = 0;$x < count( $passenger ); $x++){
										if( $x != count( $passenger ) - 1 ){
											echo $passenger[$x].','.'<br />';
										} else {
											echo $passenger[$x].'<br />';
										}
									}
								}
								?>
							</div>
						</td> 
					</tr> 
					<tr>
						<th>
							<label>Start Date:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo datetime( $start_date,'M j, Y'); ?></div> 
						</td>
						<th>
							<label>End Date:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo datetime( $end_date,'M j, Y'); ?></div> 
						</td>
					</tr> 		
					<tr>
						<th>
							<label>Adult:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $adult; ?></div> 
						</td>
						<th>
							<label>Child:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $child; ?></div> 
						</td>
					</tr>  
					<tr>
						<th>
							<label>Room:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $room; ?></div> 
						</td>
						<th>
							<label>Room Board:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $room_board; ?></div> 
						</td>
					</tr> 
					<tr>
						<th>
							<label>Children Ages:</label>
						</th>
						<td>
							<div class="regular-text">
								<?php
								if( $children_ages ){
									foreach( $children_ages as $ages => $val ){
										if( $val ){
											for($x=0;$x < count( $val );$x++){ 
												if( $x != count( $val ) - 1 ){ 
													if( $val[$x] ){
														echo $val[$x].',';
													} 
												} else {
													if( $val[$x] ){	
														echo $val[$x];
													}
												}
											} 
										}
									}
								} 	 
								?>
							</div> 
						</td>
						<th>
							<label>Room Type:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $room_type; ?></div> 
						</td>
					</tr> 
					<tr>
						<th>
							<label>Room Occupancy:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $room_occupancy; ?></div> 
						</td>
						<th>
							<label>Room Amount:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $room_amount; ?></div> 
						</td>
					</tr>  
					<?php
					if( $discount_occupancy_amount || $discount_occupancy_amount > 0 ){
						?> 
						<tr>
							<th>
								<label>Discount Name:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $discount_name; ?></div> 
							</td>
							<th>
								<label>Discount Period:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $discount_period; ?></div> 
							</td>
						</tr> 
						<tr>
							<th>
								<label>Discount Occupancy Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $discount_occupancy_amount; ?></div> 
							</td>
							<th>
								<label>Total Discount Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $total_discount_amount; ?></div> 
							</td>
						</tr>   
						<?php
					}
					?>
					<tr>
						<th>
							<label>Cancellation Policy Date:</label>
						</th>
						<td>
							<div class="regular-text">
								<?php
								$cancellation_policy_date_is = iwp_is_serialized( $cancellation_policy_date_cv ); 
								if( $cancellation_policy_date_is ){
									$cancellation_policy_date_is_count = count( $cancellation_policy_date_is );
									for( $cx=0; $cx< $cancellation_policy_date_is_count;$cx++ ){
										if( $cancellation_policy_date_is[$cx] ){
											$cancelation_detail = $cancellation_policy_date_is[$cx];  
											if( $cx == 0 ){
												echo datetime($cancelation_detail,'M j, Y');   
											} else {
												echo '<br />'.datetime($cancelation_detail,'M j, Y');   
											}
											
										}
									
									}
								} else {
									echo datetime($cancellation_policy_date,'M j, Y'); 
								} 
								?>
							</div> 
						</td>
						<th>
							<label>Cancellation Policy Amount:</label>
						</th>
						<td>
							<div class="regular-text">
								<?php 
								$cancellation_policy_amount_is = iwp_is_serialized( $cancellation_policy_amount_cv ); 
								if( $cancellation_policy_amount_is ){
									$cancellation_policy_amount_is_count = count( $cancellation_policy_amount_is );
									for( $cx=0; $cx< $cancellation_policy_amount_is_count;$cx++ ){
										if( $cancellation_policy_amount_is[$cx] ){
											$cancelation_detail = $cancellation_policy_amount_is[$cx];  
											if( $cx == 0 ){
												echo $cancelation_detail;  
											} else { 
												echo '<br />'.$cancelation_detail; 
											}
											
										}
									
									}
								} else {
									echo $cancellation_policy_amount; 
								} 
								?>
							</div> 
						</td>
					</tr> 	
					<tr>
						<th>
							<label>Cancellation Policy Amount Currency:</label>
						</th>
						<td>
							<div class="regular-text"> <?php echo $cancellation_policy_amount_currency;?></div> 
						</td>
						<th>
							<label>Cancellation Policy Time:</label>
						</th>
						<td>
							<div class="regular-text">
								<?php 
								$cancellation_policy_time_is = iwp_is_serialized( $cancellation_policy_time );
								if( $cancellation_policy_time_is ){
									$cancellation_policy_time_is_count = count( $cancellation_policy_time_is );
									for( $cx=0; $cx< $cancellation_policy_time_is_count;$cx++ ){
										if( $cancellation_policy_time_is[$cx] ){
											$cancelation_detail = $cancellation_policy_time_is[$cx];  
											if( $cx == 0 ){
												echo datetime($cancellation_policy_time,'H:ia'); 
											} else { 
												echo '<br />'.datetime($cancellation_policy_time,'H:ia'); 
											}
											
										} 
									}
								} else {
									echo datetime($cancellation_policy_time,'H:ia'); 
								} 
								?>
							</div> 
						</td>
					</tr> 
					<tr>
						<th>
							<label>Total Amount Currency:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $total_amount_currency; ?></div> 
						</td>
						<th>
							<label>Total Amount:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $total_amount; ?></div> 
						</td>
					</tr>   
					<tr>
						<th>
							<label>Total Amount Currency:</label>
						</th>
						<td colspan="3">
							<div class="regular-text"><?php echo $contract_comment; ?></div> 
						</td> 
					</tr>  
					<tr>
						<th colspan="3" >
							<label>Payment Information (<?php echo ucfirst($payment_type); ?>) </label>
						</th>
					</tr>
					<?php
					if( $payment_type == 'pesopay'){
						$payment_details = get_payment_pesopay( $id );
						$cppt_order_reference = get_value( $payment_details, 'cppt_order_reference' ); 
						$payment_pesopay_details = get_payment_pesopay_details( $cppt_order_reference );
						$cppt_amount = get_value( $payment_pesopay_details, 'cppt_amount' );
						$cppt_currency = get_value( $payment_pesopay_details, 'cppt_currency' );
						$cppt_holder_name = get_value( $payment_pesopay_details, 'cppt_holder_name' );
						$cppt_payment_method = get_value( $payment_pesopay_details, 'cppt_payment_method' );
						
						$cppt_currency_text = get_pesopay_currency( $cppt_currency );
						?>
						<tr>
							<th>
								<label>Order Reference:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_order_reference; ?></div> 
							</td> 
							<th>
								<label>Card Holder:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_holder_name; ?></div> 
							</td>   
						</tr>
						<tr> 
							<th>
								<label>Card Type:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_payment_method; ?></div> 
							</td> 
							<th>
								<label>Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_currency_text.' - '.$cppt_amount; ?></div> 
							</td> 
						</tr>
						<?php
						 
					} elseif( $payment_type == 'paypal'){
						$payment_details = get_payment_paypal( $id );
						$paypal_api_key = get_value( $payment_details, 'bpy_paypal_api_key' );
						$paypal_currency = get_value( $payment_details, 'bpy_total_amount_currency' );
						$paypal_amount = get_value( $payment_details, 'bpy_total_amount' );
						?>
						<tr>
							<th>
								<label>Paypal API:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $paypal_api_key; ?></div> 
							</td> 
							<th>
								<label>Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $paypal_currency.' - '.$paypal_amount; ?></div> 
							</td> 
						</tr>
						<?php
					} elseif( $payment_type == 'eghl'){
						$payment_details = get_payment_eghl( $id );
						$issuingbank = get_value( $payment_details, 'bghl_issuingbank' );
						$bankrefno = get_value( $payment_details, 'bghl_bankrefno' );
						$txnstatus = get_value( $payment_details, 'bghl_txnstatus' );
						$amount = get_value( $payment_details, 'bghl_amount' );
						$currencycode = get_value( $payment_details, 'bghl_currencycode' );  
						if( $txnstatus == 0 ){
							$txnstatus_text = 'Successful';
						} else {
							$txnstatus_text = 'Failed';
						}
						?>
						<tr>
							<th>
								<label>Bank:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $issuingbank; ?></div> 
							</td> 
							<th>
								<label>Bank Ref No:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $bankrefno; ?></div> 
							</td> 
						</tr>
						<tr>
							<th>
								<label>Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $currencycode.' - '.$amount; ?></div> 
							</td> 
							<th>
								<label>Status:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $txnstatus_text; ?></div> 
							</td> 
						</tr>
						<?php
					}
					?>
				
					<tr>
						<th>
							<label>Modified:</label>
						</th>
						<td colspan="3">
							<div class="regular-text"><?php echo datetime($modified,'M j, Y \a\t H:ia'); ?></div> 
						</td> 
					</tr>  
					<tr>
						<th>
							<label>Created:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo datetime($created,'M j, Y \a\t H:ia'); ?></div> 
						</td>
						<th>
							<label>Published:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $published; ?></div> 
						</td>
					</tr>  
				</tbody>
			</table>  
		</div> 
		<?php 
	}
}
 
function iwp_cancel_hotel_bookings_list_page(){
 
	global $wpdb; //This is used only if making any database queries   
	global $theme_url;
	global $theme_dir;
	$id = $_GET['id']; 
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 

	if( $id && $action == 'cancel' ){
		
		$result = get_records( $id );
		 
		foreach( $result as $details ){  
			$book_details_tblpref = 'bkd_';
			$book_tblpref = 'book_'; 
			$filenumber = get_value( $details, $book_tblpref.'file_number'); 
			$incomingoffice =  get_value( $details, $book_details_tblpref.'incoming_office'); 
			$total_amount =  get_value( $details, $book_details_tblpref.'total_amount'); 
			$currency =  get_value( $details, $book_details_tblpref.'total_amount_currency'); 
			$reference =  get_value( $details, $book_details_tblpref.'reference'); 
			$cub_first_name = get_value( $details, $book_tblpref.'first_name' ); 
			$cub_middle_name = get_value( $details, $book_tblpref.'middle_name' ); 
			$cub_last_name = get_value( $details, $book_tblpref.'last_name' ); 
			$cub_contact_no = get_value( $details, $book_tblpref.'contact_no' ); 
			$cub_email = get_value( $details, $book_tblpref.'email' ); 
			$cub_birthday = get_value( $details, $book_tblpref.'birthday' ); 
			$cub_country_code = get_value( $details, $book_tblpref.'country' ); 
			
			$cub_country = get_country_name( $cub_country_code ); 
			$passenger_firstname = get_value( $details, $book_details_tblpref.'passenger_firstname' );
			$passenger_lastname = get_value( $details, $book_details_tblpref.'passenger_lastname' );	
			$passenger_firstname_child = get_value( $details, $book_details_tblpref.'passenger_firstname_child' );	
			$passenger_lastname_child = get_value( $details, $book_details_tblpref.'passenger_lastname_child' );	 
			$passenger_firstname = html_entity_decode( $passenger_firstname, ENT_QUOTES );  
			$passenger_lastname = html_entity_decode( $passenger_lastname, ENT_QUOTES ); 
			$passenger_firstname_child = html_entity_decode( $passenger_firstname_child, ENT_QUOTES ); 
			$passenger_lastname_child = html_entity_decode( $passenger_lastname_child, ENT_QUOTES );  
			$passenger_first_name_details = unserialize( $passenger_firstname );   
			$passenger_last_name_details = unserialize( $passenger_lastname );
			$passenger_first_name_child_details = unserialize( $passenger_firstname_child );
			$passenger_last_name_child_details = unserialize( $passenger_lastname_child );
			  
			if( $passenger_first_name_child_details ){
				$first_name = array_merge($passenger_first_name_details, $passenger_first_name_child_details);
			} else {
				$first_name = $passenger_first_name_details;
			}
			
			if( $passenger_last_name_child_details ){
				$last_name = array_merge($passenger_last_name_details, $passenger_last_name_child_details);
			} else {
				$last_name = $passenger_last_name_details;
			}
			
			$length_first = count( $first_name );
			$length_last = count( $last_name );
			
		}
		$view_action = sprintf('<a href="?page=%s">Back</a>', $page ); 
		$confirm_booking = iwp_get_boooking_information( $reference );   
		
		$booking_array = get_value( $confirm_booking, 'booking' );  
		$reference = get_value( $booking_array, 'reference' ); 
		$clientreference = get_value( $booking_array, 'clientReference' ); 
		$creationdate = get_value( $booking_array, 'creationDate' ); 
		$booking_status = get_value( $booking_array, 'status' ); 
		$creationuser = get_value( $booking_array, 'creationUser' ); 
		$holder = get_value( $booking_array, 'holder' ); 
		$holder_name = get_value( $holder, 'name' ); 
		$holder_surname = get_value( $holder, 'surname' ); 
		$hotel_array = get_value( $booking_array, 'hotel' ); 
		$checkin = get_value( $hotel_array, 'checkIn' ); 
		$checkOut = get_value( $hotel_array, 'checkOut' ); 
		$code = get_value( $hotel_array, 'code' ); 
		$name = get_value( $hotel_array, 'name' ); 
		$categorycode = get_value( $hotel_array, 'categoryCode' ); 
		$categoryname = get_value( $hotel_array, 'categoryName' ); 
		$destinationcode = get_value( $hotel_array, 'destinationCode' ); 
		$destinationname = get_value( $hotel_array, 'destinationName' ); 
		$zonecode = get_value( $hotel_array, 'zoneCode' ); 
		$zonename = get_value( $hotel_array, 'zoneName' ); 
		$latitude = get_value( $hotel_array, 'latitude' ); 
		$longitude = get_value( $hotel_array, 'longitude' );  
		$totalsellingrate = get_value( $booking_array, 'totalSellingRate' ); 
		$totalsellingrate_nocommission = get_value( $booking_array, 'totalSellingRate' ); 
		$totalnet = get_value( $booking_array, 'totalNet' ); 
		$pendingamount = get_value( $booking_array, 'pendingAmount' ); 
		// $currency = get_value( $booking_array, 'currency' ); 
		$rooms_array = get_value( $hotel_array, 'rooms' );  
		$totalsellingrate = Hotel::commission( $totalsellingrate );
				
		$tax_value = Hotel::value_added_tax( $totalsellingrate_nocommission );
		$service_charge = Hotel::service_charge( $totalsellingrate );
		$web_admin_fee = Hotel::web_admin_fee( $totalsellingrate );				
	 
		$final_price = $totalsellingrate;    
		$add1 = $final_price + $service_charge;
		$add2 = $tax_value + $web_admin_fee;
		 
		$final_total = $final_price + $service_charge + $tax_value + $web_admin_fee;
		$item_amount = (float)$total_amount; 
	 
		 
		?>
		<div class="wrap"> 
			<div id="icon-users" class="icon32"><br/></div>
			<h2>Cancel Hotel Bookings</h2>
			<ul class="subsubsub">
				<li class="back"><a href="<?php echo '?page='.$page; ?>">Back</a></li>  
			</ul>  
			<br /><br />
			<div class="container-fluid margin-top-bottom-xs">   
					<div class="container-fluid margin-top-bottom-xs">   
						<h4 class="title-header text-left heading-color"><strong>BOOKER DETAILS</strong></h4>  
						<div class="row"> 
							<label class="col-md-3 text-left">First Name:</label>
							<div class="col-md-9"> 
								<label><?php echo $cub_first_name; ?></label>
							</div>
						</div> 
						<div class="row"> 
							<label class="col-md-3 text-left">Middle Name:</label>
							<div class="col-md-9"> 
								<label><?php echo $cub_middle_name; ?></label>
							</div>
						</div>
						<div class="row"> 
							<label class="col-md-3 text-left">Last Name:</label>
							<div class="col-md-9"> 
								<label><?php echo $cub_last_name; ?></label>
							</div>
						</div> 
						<div class="row"> 
							<label class="col-md-3 text-left">Contact no:</label>
							<div class="col-md-9"> 
								<label><?php echo $cub_contact_no; ?></label>
							</div>
						</div> 
						<div class="row"> 
							<label class="col-md-3 text-left">Email:</label>
							<div class="col-md-9"> 
								<label><?php echo $cub_email; ?></label>
							</div>
						</div>
						<div class="row"> 
							<label class="col-md-3 text-left">Birthday:</label>
							<div class="col-md-9"> 
								<label><?php echo $cub_birthday; ?></label>
							</div>
						</div>
						<div class="row"> 
							<label class="col-md-3 text-left">Country:</label>
							<div class="col-md-9"> 
								<label><?php echo $cub_country; ?></label>
							</div>
						</div> 
						<h4 class="title-header text-left heading-color"><strong>PASSENGER DETAILS</strong></h4>  
						<div class="row"> 
							<label class="col-md-1 text-center"> </label>  
							<label class="col-md-5 text-center">First Name</label>  
							<label class="col-md-5 text-center">Family Name</label>  
						</div>   
						<?php
						$passenger_first_name_datails_count = count( $first_name );
						for ( $x = 1; $x <= $passenger_first_name_datails_count; $x++ ){   
								
							$key= $x - 1;	 
							$passenger_first_name = $first_name[$key]; 
							$passenger_last_name = $last_name[$key];  
							
							if( $passenger_first_name && $passenger_last_name ){
								?>	
								<div class="row"> 
									<label class="col-md-1"><?php echo $x?>.</label>  
									<label class="col-md-5"><?php echo $passenger_first_name?></label>  
									<label class="col-md-5"><?php echo $passenger_last_name?></label>  
								</div> 
								<?php
							}
						}	
						?> 
						<h4 class="title-header text-left heading-color"><strong>SERVICES</strong></h4>   
						<h5 class="title-header text-left heading-color"> <strong>ACCOMMODATION - PAY AT AGENCY</strong></h5>		
						<?php echo iwp_get_hotel_rating( $categorycode ); ?>
						<h4 class="heading-color" style="margin-top:0px;margin-bottom:0px;"><strong><?php echo $name; ?></strong></h4> 
						<div class="row">    
							<label class="col-md-12">From: <?php echo $checkin.' '.iwp_get_day_from_date( $checkin ); ?> - To: <?php echo $checkOut.' '.iwp_get_day_from_date( $checkOut ); ?></label>  
						</div>
						<div class="row"> 
							<label class="col-md-4"><strong>Room Type</strong></label> 
							<label class="col-md-3"><strong>Board</strong></label>
							<label class="col-md-3"><strong>Occupancy</strong></label>
							<label class="col-md-2"> </label> 
						</div>
						<hr />
						<div class="row"> 
							<?php
							if( $rooms_array ){ 	 
								foreach( $rooms_array as $rooms_details ){ 
									$hotel_room_room_type = get_value( $rooms_details, 'name');
									$hotel_room_rates = get_value( $rooms_details, 'rates');
									if( $hotel_room_rates ){	
										foreach( $hotel_room_rates as $hr_rates ){
											// printr($hr_rates );
											$hotel_room_board = get_value( $hr_rates, 'boardName');
											$hotel_room_amount =  get_value( $hr_rates, 'net');
											$hotel_room_amount_nocom = get_value( $hr_rates, 'net');
											$hotel_room_rateComments = get_value( $hr_rates, 'rateComments');
											$hotel_room_cancellationPolicies = get_value( $hr_rates, 'cancellationPolicies');
											$hotel_room_rateBreakDown = get_value( $hr_rates, 'rateBreakDown');
											//$paymentType = get_value( $hr_rates, 'paymentType');
											//$get_payment_at = $hotel_class::get_payment_at( $paymentType );
											$hotel_room_amount = Hotel::commission( $hotel_room_amount );
											$hotel_room_amount_view = (float)$hotel_room_amount;  
											$hotel_room_amount_nocom = (float)$hotel_room_amount_nocom;  
											$hotel_room_amount = number_format( $hotel_room_amount_view,2,'.',',' );  
											$hotel_room_amount_nocom = number_format( $hotel_room_amount_nocom,2,'.',',' );  
					
											$room_count = get_value( $hr_rates, 'rooms');
											$adult_count = get_value( $hr_rates, 'adults');
											$child_count = get_value( $hr_rates, 'children');
										
											$room_count_add = $room_count?$room_count:$room_count_summary;
											$adult_count_add = $adult_count?$adult_count:$adult_count_summary;
											$child_count_add = $child_count?$child_count:$child_count_summary;
											 
											if( $adult_count > 1 ){
												$adult_count .= ' adults';
											} else {
												$adult_count .= ' adult';
											}
											
											if( $child_count > 0){
												if( $child_count > 1 ){
												$child_count .= ' children';
												} else {
													$child_count .= ' child';
												}
											} else {
												$child_count = Null;
											} 
										}
										?>
										<tr>
											<td><label class="col-md-4"><?php echo $hotel_room_room_type.' x '.$room_count; ?></label></td>
											<td><label class="col-md-3"><?php echo $hotel_room_board; ?></label></td>
											<td><label class="col-md-3"><?php echo $adult_count.' '.$child_count; ?></label></td> 
											<td><label class="col-md-2"><?php echo $currency.' '.$hotel_room_amount; ?></label></td>  
										</tr>
										<?php 
									
									}
								}
							}
							?> 
						</div>
						<?php
						if( $hotel_room_rateComments ){
							?>
							<hr />
							<div class="row"> 
								<p class="col-md-12"><strong>Contract remarks</strong><br />
									<?php echo $hotel_room_rateComments; ?>
								</p> 
							</div>
							<?php
						}
						?>	 
						<hr />
						<div class="row"> 
							<p class="col-md-12"><strong><?php echo $hotel_room_room_type; ?></strong><br />
								In the event of cancellation after
								<?php 
								
								if( $hotel_room_cancellationPolicies ){
									$cancellationpolicies_count = count( $hotel_room_cancellationPolicies ); 
									for( $cx=0; $cx< $cancellationpolicies_count;$cx++ ){
										$cancelation_detail = $hotel_room_cancellationPolicies[$cx]; 
										$cancellation_policy_amount = get_value( $cancelation_detail, 'amount');
										$cancellation_policy_amount_view = (float)$cancellation_policy_amount;  
										$cancellation_policy_amount = number_format( $cancellation_policy_amount_view,2,'.',',' );   
										$cancellation_policy_date_from = get_value( $cancelation_detail, 'from');
									
										$cancellation_policy_date_from = date( "m-d-Y", strtotime( $cancellation_policy_date_from ) ); 
										$cancellation_policy_time = date( "h:iA", strtotime( $cancellation_policy_date_from ) ); 
										//$cancellation_policy_time = $cancelation_detail->attributes()->time; 
										//printr( $cancellation_policy_time );
										if( $cx == 0 ){
											echo iwp_show_local_time( $cancellation_policy_time ).' on '.$cancellation_policy_date_from.' the following charges will be applied: '.$currency.' '.$cancellation_policy_amount;
										} else {
											echo ' and '.iwp_show_local_time( $cancellation_policy_time ).' on '.$cancellation_policy_date_from.' the following charges will be applied: '.$currency.' '.$cancellation_policy_amount;
										} 
									} 
								}
								?>
								Cancellation charges described above will not apply during 1 hour from the creation time of the booking. During this time you may cancel the booking online with no fees. Automatically, after those 1 hour the described cancellation policy will be applied. Date and time is calculated based on local time of destination.
							</p> 
						</div>
						<hr /> 
						<div class="row"> 
							<p class="col-md-12">Date and time is calculated based on local time of destination.</p> 
							<p class="col-md-12"><strong>All payment will be paid in Philippine Peso Currency.</strong></p>
						</div>  
						<div class="row"> 
							<h5 class="col-md-12 col-md-offset-4 heading-color text-right"><strong>Booking total: <?php echo $currency.' '.number_format( $item_amount,2,'.',',' ); ?></strong></h5> 
						</div> 
						<?php 
						$success_cancel = 'http://cribsandtrips.com/wp-admin/admin.php?page='.$page.'&action=confirm_cancel&id='.$id; 
						$error_cancel = 'http://cribsandtrips.com/wp-admin/admin.php?page=iwp_hotel_bookings_list'; 
						$item_amount = $cancellation_policy_amount;
						$fee_amount = '0.00';
						// $today_date = date( '04-27-2016' );  
						$today_date = date( "m-d-Y" );  
						
						if( $hotel_room_cancellationPolicies ){
							$cancellationpolicies_count = count( $hotel_room_cancellationPolicies ); 
							for( $cx=0; $cx< $cancellationpolicies_count;$cx++ ){
								$cancelation_detail = $hotel_room_cancellationPolicies[$cx]; 
								$cancellation_policy_amount = get_value( $cancelation_detail, 'amount'); 
								$cancellation_policy_date_from = get_value( $cancelation_detail, 'from');
							
								$cancellation_policy_date_from = date( "m-d-Y", strtotime( $cancellation_policy_date_from ) ); 
								$cancellation_policy_time = date( "h:iA", strtotime( $cancellation_policy_date_from ) ); 
								 
								
								if( $today_date < $cancellation_policy_date_from  ){   
									$cancel_y_n = 'past';
								} elseif( $today_date > $cancellation_policy_date_from   ){
									$fee_amount = round( $cancellation_policy_amount, 2 ); 
									$cancel_y_n = 'future';
								} elseif(  $today_date == $cancellation_policy_date_from   ){
									$fee_amount = round( $cancellation_policy_amount, 2 ); 
									$cancel_y_n = 'equal';
								}
								 
							} 
						} 
						require_once( $theme_dir.'SHAPaydollarSecure.php' );

						$pesopay = pods( 'pesopay' );
						$merchantId = $pesopay->field('pesopay_merchant_id'); 
						$order_ref_format = $pesopay->field('pesopay_order_ref');  
						$orderRef = date( $order_ref_format ).'-'.$id;
						
						$currCode = $pesopay->field('pesopay_currency_code');  
						$paymentType = $pesopay->field('pesopay_payment_type');  
						$mpsMode = $pesopay->field('pesopay_mps_mode');  
						$payMethod = $pesopay->field('pesopay_pay_method');  
						$lang = $pesopay->field('pesopay_language');  
						$secureHashSecret = $pesopay->field('pesopay_secure_hash_secret');  
						$redirect = $pesopay->field('pesopay_redirect');  
						
						//Optional Parameter for connect to our payment page
						$pesopay_remark="cancellation fee"; 
						$oriCountry="608";
						$destCountry="608";

						// $secureHashSecret='qoVpGh8PiE7efDaVp8S8wc0JqjEKC0Xu';//offered by paydollar
						//Secure hash is used to authenticate the integrity of the transaction information and the identity of the merchant. It is calculated by hashing the combination of various transaction parameters and the Secure Hash Secret.
						$paydollarSecure=new SHAPaydollarSecure();  
						$secureHash=$paydollarSecure->generatePaymentSecureHash($merchantId, $orderRef, $currCode, $item_amount, $paymentType, $secureHashSecret); 
					
						$paymentgateway = pods( 'paymentgateway' );
						$payment_type = $paymentgateway->field('payment_type'); 
					
						if( $payment_type == 'pesopay' ){ 
							$payment_url = $pesopay->field('pesopay_url');   
						} elseif( $payment_type == 'paypal' ){ 
							$payment_paypal = pods( 'paypal' );
							$paypal_api_key = $payment_paypal->field('paypal_api_key');  
							$payment_url = $payment_paypal->field('paypal_url');  
						} elseif( $payment_type == 'eghl' ){  
							$eghl = pods( 'eghl' ); 
							$payment_url = $eghl->field( 'action_link' );  
						} 
						$item_amount = $total_amount - $fee_amount ;  
						if( $item_amount >= 0 ){
							$item_amount = '0.00';
						}
						
						?> 
						<div class="row">   
							<div class="col-md-4"> 
								<?php
								if( $cancel_y_n == 'equal' || $cancel_y_n == 'future' ){  
									if( $fee_amount != 0 && $total_amount > $fee_amount ){ 
									
										$item_amount = $fee_amount;  
										if( $payment_type == 'paypal' ){  
											?>
											<form action="<?php echo $payment_url;?>" method="POST" > 
											<input type="hidden" name="business" value="<?php echo $paypal_api_key; ?>" />
											<input type="hidden" name="cmd" value="_xclick" />
											<input type="hidden" name="item_name" value="<?php echo $hotel_name; ?>" />
											<input type="hidden" name="item_number" value="<?php echo $hotel_code; ?>" /> 
											<input type="hidden" name="amount" value="<?php echo $item_amount;?>">
											<input type="hidden" name="quantity" value="1">
											<input type="hidden" name="undefined_ quantity" value="room(s)">
											<input type="hidden" name="cpp_header_image" value="http://j-leisure.com/dev/includes/images/logo.png" />   
											<input type="hidden" name="currency_code" value="PHP">   
											<input type="hidden" name="cancel_return" value="<?php echo $error_cancel; ?>">
											<input type="hidden" name="return" value="<?php echo $success_cancel; ?>">
											<input type="image" id="btn_buynowcc_lg" class="img-responsive" src="http://cribsandtrips.com/wp-content/uploads/2016/02/btn-buynowCC-LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style= "float: right;">
											<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1"> 
											</form>
											<?php
										} elseif( $payment_type == 'pesopay' ){
											
											if( $id ){
												?>	 
												<form action="<?php echo $payment_url;?>" method="POST" > 
												<input class="btn btn-success" type="submit" name="client_and_payment_date_confirm" value="Book and Pay now" style= "float: right;"/>  
												<?php
											}
											?>
										
											<input type="hidden" name="merchantId" value="<?php echo $merchantId; ?>">
											<input type="hidden" name="amount" value="<?php echo $item_amount; ?>" >
											<input type="hidden" name="orderRef" value="<?php echo $orderRef; ?>">
											<input type="hidden" name="currCode" value="<?php echo $currCode; ?>" >
											<input type="hidden" name="successUrl" value="<?php echo $successUrl; ?>">
											<input type="hidden" name="failUrl" value="<?php echo $failUrl; ?>"></td>
											<input type="hidden" name="cancelUrl" value="<?php echo $cancelUrl; ?>">
											<input type="hidden" name="payType" value="<?php echo $paymentType; ?>">
											<input type="hidden" name="lang" value="<?php echo $lang; ?>">
											<input type="hidden" name="mpsMode" value="<?php echo $mpsMode; ?>">
											<input type="hidden" name="payMethod" value="<?php echo $payMethod; ?>">
											<input type="hidden" name="secureHash" value="<?php echo $secureHash; ?>"> 
											<input type="hidden" name="remark" value="<?php echo $pesopay_remark; ?>"> 
											<input type="hidden" name="redirect" value="<?php echo $redirect; ?>"> 
											<input type="hidden" name="oriCountry" value="<?php echo $oriCountry; ?>"> 
											<input type="hidden" name="destCountry" value="<?php echo $destCountry; ?>">  
											<input type="hidden" name="destination_name" value="<?php echo $destination_name; ?>">   
											</form>
											<?php
										} elseif( $payment_type == 'eghl' ){ 
										
											/* pods */
											$eghl = pods( 'eghl' );
											$merchant_name = $eghl->field( 'merchant_name' ); 
											$service_id = $eghl->field( 'service_id' ); 
											$password = $eghl->field( 'password' ); 
											$language = $eghl->field( 'language' ); 
											$payment_method = $eghl->field( 'payment_method' ); 
											$transaction_type = $eghl->field( 'transaction_type' );  
											$payment_id = $eghl->field( 'payment_id' ); 
											$page_timeout = $eghl->field( 'page_timeout' ); 
											$action_link = $eghl->field( 'action_link' ); 
											/* In all return Url we can the ID of the booking to track records  */ 
										  
											$merchant_return_url = $error_cancel; 
											$merchant_approval_url = $success_cancel; 
											$mechant_unapproval_url = $error_cancel; 
											$merchant_callback_url = $error_cancel; 
											 
											
											/* End of Pods */
											$payment_id = date('ymdhis').'-'.$id;
										
											if( $transaction_type == TRUE ){
												$transaction_type = 'SALE';
											} elseif( $transaction_type == FALSE ){
												$transaction_type = 'AUTH';
											}
											/* Its okay if the order number is not equal to to payment ID */
											$order_no = $payment_id ;
											
											/* Data from user inputted data */ 
											$currencycode = 'PHP';  /* Booking Currency from Bedsonline */
											// $custip = '112.201.2.7'; */
											$custip = $_SERVER['SERVER_ADDR'];/* Device IP */
											$blog_title = get_bloginfo( 'name' ); 
										 
											
											/* Hash Format */ 
											$eghl_item_amount = number_format( $item_amount, 2, '.', ''); 
											//printr( $eghl_item_amount );
											$key = $password.$service_id.$payment_id.$merchant_return_url.$merchant_approval_url.$mechant_unapproval_url.$merchant_callback_url;
											$key .= $eghl_item_amount.$currencycode.$custip.$page_timeout;
											$hash_key = hash('sha256',$key);
											
											$customer_name = $cub_last_name.', '.$cub_first_name.' '.$cub_middle_name;
											$payment_desc= $hotel_name.', '.$checkindate.'- '.$checkoutdate.' '.$hotel_room_room_type;
											 
											if( $id ){
												?>	
												<form action="<?php echo $action_link;?>" method="POST" > 
												<input class="btn btn-success" type="submit" name="client_and_payment_date_confirm" value="Book and Pay now" style= "float: right;"/>  
												<?php
											}
											?> 
											<input type="hidden" name="MerchantName" value="<?php echo $merchant_name; ?>">
											<input type="hidden" name="PaymentID" value="<?php echo $payment_id; ?>">
											<input type="hidden" name="OrderNumber" value="<?php echo $order_no; ?>">
											<input type="hidden" name="PaymentDesc" value="<?php echo $payment_desc; ?>">
											<input type="hidden" name="Amount" value="<?php echo $eghl_item_amount; ?>">
											<input type="hidden" name="CustName" value="<?php echo $customer_name; ?>">
											<input type="hidden" name="CustEmail" value="<?php echo $cub_email; ?>">
											<input type="hidden" name="CustPhone" value="<?php echo $cub_contact_no; ?>">
											<input type="hidden" name="TransactionType" value="<?php echo $transaction_type; ?>">
											<input type="hidden" name="PymtMethod" value="<?php echo $payment_method; ?>">
											<input type="hidden" name="ServiceID" value="<?php echo $service_id; ?>">
											<input type="hidden" name="CurrencyCode" value="<?php echo $currencycode; ?>">
											<input type="hidden" name="HashValue" value="<?php echo $hash_key; ?>">
											<input type="hidden" name="CustIP" value="<?php echo $custip; ?>">
											<input type="hidden" name="LanguageCode" value="<?php echo $language; ?>">
											<input type="hidden" name="PageTimeout" value="<?php echo $page_timeout; ?>">
											<input type="hidden" name="MerchantReturnURL" value="<?php echo $merchant_return_url; ?>">
											<input type="hidden" name="MerchantApprovalURL" value="<?php echo $merchant_approval_url; ?>">
											<input type="hidden" name="MerchantUnApprovalURL" value="<?php echo $mechant_unapproval_url; ?>">
											<input type="hidden" name="MerchantCallBackURL" value="<?php echo $merchant_callback_url; ?>">
											 </form>
											<?php
											
										}
									} else {
										?>
										<a href="<?php echo '?page='.$page.'&action=confirm_cancel&id='.$id;?>">
										<input class="col-md-12 btn btn-success" type="submit" name="client_and_payment_date_confirm" value="Cancel Booking" />  
										<?php
									}
								} else {
									?>
									<a href="<?php echo '?page='.$page.'&action=confirm_cancel&id='.$id;?>">
									<input class="col-md-12 btn btn-success" type="submit" name="client_and_payment_date_confirm" value="Cancel Booking" />  
									<?php
								}
								?>
								<!--<a href="<?php echo '?page='.$page.'&action=confirm_cancel&id='.$id;?>">
									<input class="col-md-12 btn btn-success" type="submit" name="client_and_payment_date_confirm" value="Cancel Booking" />   
								</a>
								<!--<input class="col-md-12 btn btn-success" type="button" name="client_and_payment_date_confirm" value="Cancel Booking" onclick="cancellaccommodation()"/>   -->
							</div>					
						</div>
					</div> 		 
			</div><!-- /.container-fluid --> 
		</div>
		<?php
		 
	}
}

function iwp_confirm_cancel_hotel_bookings_list_page(){
	global $wpdb; //This is used only if making any database queries  
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 

	if( $id && $action == 'confirm_cancel' ){
		
		$result = get_records( $id );
		 
		foreach( $result as $details ){
			$book_details_tblpref = 'bkd_';
			$book_tblpref = 'book_'; 
			$reference = get_value( $details, $book_details_tblpref.'reference'); 
			$filenumber = get_value( $details, $book_tblpref.'file_number'); 
			$incomingoffice =  get_value( $details, $book_details_tblpref.'incoming_office'); 
			$cub_first_name = get_value( $details, $book_tblpref.'first_name' ); 
			$cub_middle_name = get_value( $details, $book_tblpref.'middle_name' ); 
			$cub_last_name = get_value( $details, $book_tblpref.'last_name' ); 
			$cub_contact_no = get_value( $details, $book_tblpref.'contact_no' ); 
			$cub_email = get_value( $details, $book_tblpref.'email' ); 
			$cub_birthday = get_value( $details, $book_tblpref.'birthday' ); 
			$cub_country_code = get_value( $details, $book_tblpref.'country' ); 
			$cub_country = get_country_name( $cub_country_code );
			
			$purchase_file_number = get_value( $details,$book_tblpref.'file_number' );
			$service_filenumber = get_value( $details,$book_details_tblpref.'reference' );
			$purchase_incoming_office = get_value( $details,$book_details_tblpref.'incoming_office' );
			
			$bkd_id = get_value( $details, 'bkd_id' );
			$hotel_code = get_value( $details, 'HotelCode' );
			$destination = get_value( $details,$book_details_tblpref.'destination' ); 
			$passenger_firstname = get_value( $details,$book_details_tblpref.'passenger_firstname' );
			$passenger_lastname = get_value( $details,$book_details_tblpref.'passenger_lastname' );	
			$passenger_firstname_child = get_value( $details,$book_details_tblpref.'passenger_firstname_child' );	
			$passenger_lastname_child = get_value( $details,$book_details_tblpref.'passenger_lastname_child' );	 
			$children_ages = get_value( $details,$book_details_tblpref.'children_ages' );
			$passenger_firstname = html_entity_decode( $passenger_firstname, ENT_QUOTES );  
			$passenger_lastname = html_entity_decode( $passenger_lastname, ENT_QUOTES ); 
			$passenger_firstname_child = html_entity_decode( $passenger_firstname_child, ENT_QUOTES ); 
			$passenger_lastname_child = html_entity_decode( $passenger_lastname_child, ENT_QUOTES ); 
			$children_ages = html_entity_decode( $children_ages, ENT_QUOTES ); 
			$passenger_first_name_details = check_if_serialized( ( $passenger_firstname ) );   
			$passenger_last_name_details = check_if_serialized( ( $passenger_lastname ) );
			$passenger_first_name_child_details = check_if_serialized( ( $passenger_firstname_child ) );
			$passenger_last_name_child_details = check_if_serialized( ( $passenger_lastname_child ) );
			$children_ages = check_if_serialized( ( $children_ages ) );
			
			if( $passenger_first_name_child_details ){
				$first_name = array_merge($passenger_first_name_details, $passenger_first_name_child_details);
			} else {
				$first_name = $passenger_first_name_details;
			}
			
			if( $passenger_last_name_child_details ){
				$last_name = array_merge($passenger_last_name_details, $passenger_last_name_child_details);
			} else {
				$last_name = $passenger_last_name_details;
			}
			 
			$length_first = count( $first_name );
			$length_last = count( $last_name );
			$fullname = array();
			
			if( $length_first > $length_last ){
				$base_count = $length_first;
			} else {
				$base_count =  $length_last;
			}
			
			for( $x=0; $x< $base_count;$x++ ){
				$first = preg_replace('/[^A-Za-z0-9\- \']/', '', $first_name[$x]);
				$second = preg_replace('/[^A-Za-z0-9\- \']/', '', $last_name[$x]);
				array_push( $fullname, $first.' '.$second);
			}
			$passenger = $fullname ; 
			$checkindate = get_value( $details,$book_details_tblpref.'start_date' ); 
			$checkoutdate = get_value( $details,$book_details_tblpref.'end_date' );
			
			$room = get_value( $details,$book_details_tblpref.'room' );
			$room_count = get_value( $details,$book_details_tblpref.'room' );
			$adult = get_value( $details,$book_details_tblpref.'adult' );
			$adult_count = get_value( $details,$book_details_tblpref.'adult' );
			$child = get_value( $details,$book_details_tblpref.'child' );
			$child_count = get_value( $details,$book_details_tblpref.'child' );
			
			$room_type = get_value( $details,$book_details_tblpref.'room_type' );
			$room_board = get_value( $details,$book_details_tblpref.'room_board' );
			$room_occupancy = get_value( $details,$book_details_tblpref.'room_occupancy' );
			$room_amount = get_value( $details,$book_details_tblpref.'room_amount_no_discount' );
			$discount_name = get_value( $details,$book_details_tblpref.'discount_name' );
			$discount_period = get_value( $details,$book_details_tblpref.'discount_period' );
			$discount_occupancy_amount = get_value( $details,$book_details_tblpref.'discount_occupancy_amount' );
			$total_discount_amount = get_value( $details,$book_details_tblpref.'total_discount_amount' );
			$get_comment = get_value( $details,$book_details_tblpref.'contract_comment' );
			$contract_comment = preg_replace('/[^A-Za-z0-9\-., \']/', ' ', $get_comment);
			$cancellation_policy_time = get_value( $details,$book_details_tblpref.'cancellation_policy_time' );
			$cancellation_policy_date = get_value( $details,$book_details_tblpref.'cancellation_policy_date' );
			$cancellation_policy_amount = get_value( $details,$book_details_tblpref.'cancellation_policy_amount' );
			$cancellation_policy_amount_currency = get_value( $details,$book_details_tblpref.'cancellation_policy_amount_currency' );
			$agency_reference = get_value( $details,$book_details_tblpref.'agency_reference' );
			$total_amount_currency = get_value( $details,$book_details_tblpref.'total_amount_currency' );
			$total_amount = get_value( $details,$book_details_tblpref.'total_amount' );
			$created = get_value( $details, $book_tblpref.'created' );
			$modified = get_value( $details, $book_tblpref.'modified' );
			$published = get_value( $details, $book_tblpref.'published' );
			$hotel_code = get_value( $details, 'HotelCode' );
			$hotel_name = get_value( $details, 'Name' );
			 
			if( $room_count > 1 ){
				$room_count .= ' rooms';
			} else {
				$room_count .= ' room';
			}
			
			if( $adult_count > 1 ){
				$adult_count .= ' adults';
			} else {
				$adult_count .= ' adult';
			}
			
			if( $child_count > 0){
				if( $child_count > 1 ){
				$child_count .= ' children';
				} else {
					$child_count .= ' child';
				}
			} else {
				$child_count = Null;
			} 
			 
		}
		
		$boooking_cancellation = iwp_get_boooking_cancellation( $reference ); 
		
		$booking_array = get_value( $boooking_cancellation, 'booking' );  
		$reference = get_value( $booking_array, 'reference' ); 
		$clientreference = get_value( $booking_array, 'clientReference' ); 
		$status = get_value( $booking_array, 'status' ); 
		$holder_array = get_value( $booking_array, 'holder' ); 
		$holder_name = get_value( $holder_array, 'name' ); 
		$holder_last_name = get_value( $holder_array, 'surname' ); 
		$hotel_array = get_value( $booking_array, 'hotel' ); 
		$hotel_name = get_value( $hotel_array, 'name' ); 
		$hotel_code = get_value( $hotel_array, 'code' );  
      	?>
		
		<div class="wrap"> 
			<div id="icon-users" class="icon32"><br/></div>
			<h2>Cancel Hotel Bookings</h2>
			<ul class="subsubsub">
				<li class="back"><a href="<?php echo '?page='.$page; ?>">Back</a></li>  
			</ul>
			<br /><br />
			<?php
			if( $boooking_cancellation ){
				@$xml = simplexml_load_string( $contents );     
				$errors = $xml->ErrorList->Error;	
				$purchase = $xml->Purchase;	  
				
				if( $status != 'CANCELLED' ){ 
					?>
					<div class="row">
						<div class="col-sm-12">
							<div class="alert alert-danger" role="alert"> 
								<p>Booking can't cancelled. Please contact the hotel for clarifacation.</p>
							</div> 
						</div>
					</div> 
					<?php 
				} else { 
					if( $status == 'CANCELLED'){
						if( $id ){
							
							$booking_updated = $wpdb->update( 
								'wp_cribsandtrips_booking', 
								array(  
									$book_tblpref.'status' => 'cancelled',	// string 
								), 
								array( $book_tblpref.'id' => $id ) 
							); 		
							 
							 
							$summary_occupancy = $room_count.' '.$adult_count.' '.$child_count;  
			 
							$bookings_data =  array( 	         
								'booking_reference' => $service_filenumber,        
								'service_ref' => $service_filenumber,      
								'incoming_office' => $service_filenumber,      
								'reference_number' => $service_filenumber, 			
								'booker_first_name' => $cub_first_name,      
								'booker_middle_name' => $cub_middle_name,     
								'booker_last_name' => $cub_last_name,       
								'booker_email' => $cub_email,     
								'booker_contact_no' => $cub_contact_no,       
								'booker_country' => $cub_country,    			
								'holder_name' => $holder_name,       
								'holder_last_name' => $holder_last_name,       
								'hotel_name' => $hotel_name,       
								'hotel_category' => $category_code,       
								'hotel_address' => $destination_name.' '.$destination_zone,       
								'hotel_availableroom' => $services,       
								'room_type' => $hotel_room_room_type,            
								'room_count' => $room_count,            
								'board' => $hotel_room_board,            
								'board_type' => $hotel_room_board,            
								'summary_occupancy' => $summary_occupancy,     
								'occupancy' => $hotel_room_occupancy,     
								'price' => $currency_response.' '.$hotel_room_amount,    
								'item_amount' => $currency_response.' '.$item_amount,    
								'contract_remarks' => $contract_comment,    
								'total_price' => $currency_response.' '.$total_amount,   
								'currency_response' => $currency_response,   
								'agency_reference' => $agency_reference,    
								'service_description' => $hotel_name,   
								'check_in' => $checkindate,   
								'check_out' => $checkoutdate,       
								'cancellation_date' => $cancellation_policy_date,   
								'cancellation_policy_time' => $cancellation_policy_time,   
								'cancellation_charge' => $currency_response.' '.$cancellation_policy_amount,
								'latitude' => $hotel_latitude,  
								'longitude' => $hotel_longitude,   
								'adults' => $customer_list_adult_count,   
								'children_ages' => $customer_list_child_age_array,   
								'children' => $customer_list_child_count,   
								'hotel_telephone' => $hotel_contact_number,    
								'hotel_fax' => $hotel_fax_number,    
								'final_price' => $final_price,    
								'hotel_email' => $hotel_email,
								'total_net_amount' => $final_total,
								'service_charge' => $service_charge,
								'tax_value' => $tax_value,
								'web_admin_fee' => $web_admin_fee,
								'book_or_number' => $book_or_number,
								'book_invoice_number' => $book_invoice_number,
								'booking_id' => $id  
							);  
							
							successful_cancel_email_to_client( $bookings_data );
							successful_cancel_email_to_admin( $bookings_data );	
							?>
							<div class="row">
								<div class="col-sm-12">
									<div class="alert alert-success" role="alert">
										<p>Booking is successfully cancelled. Cancellation Fee will be deducted to your refund amount if any.</p>
									</div> 
								</div>
							</div> 
							<?php
							
							
						}
					} else {
						?>
						<div class="row">
							<div class="col-sm-12">
								<div class="alert alert-warning" role="alert">
									<p>Booking can't cancelled. Please contact the hotel for clarifacation.</p>
								</div> 
							</div>
						</div>
						<?php 
					} 
				}
			}
					
			?>
		</div>	
		<?php
	}
}


function iwp_confirmed_hotel_booking(){
	  
	global $wpdb; //This is used only if making any database queries  
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 
	$book_tblpref = 'book_';
	if( $id && $action == 'confirmed' ){
		
		$result = get_records( $id );
		
		foreach( $result as $details ){
			 
			$book_details_tblpref = 'bkd_';
			$book_tblpref = 'book_';
		 
			$cub_first_name = get_value( $details, $book_tblpref.'first_name' );
			$cub_middle_name = get_value( $details, $book_tblpref.'middle_name' );
			$cub_last_name = get_value( $details, $book_tblpref.'last_name' );
			$cub_contact_no = get_value( $details, $book_tblpref.'contact_no' );
			$cub_email = get_value( $details, $book_tblpref.'email' );
			$cub_birthday = get_value( $details, $book_tblpref.'birthday' );
			$cub_country_code = get_value( $details, $book_tblpref.'country' );
			$cub_country = get_country_name( $cub_country_code );
			
			$purchase_file_number = get_value( $details,$book_tblpref.'file_number' );
			$service_filenumber = get_value( $details,$book_details_tblpref.'reference' );
			$purchase_incoming_office = get_value( $details,$book_details_tblpref.'incoming_office' );
			
			$bkd_id = get_value( $details, 'bkd_id' );
			$hotel_code = get_value( $details, 'HotelCode' );
			$destination = get_value( $details,$book_details_tblpref.'destination' ); 
			$passenger_firstname = get_value( $details,$book_details_tblpref.'passenger_firstname' );
			$passenger_lastname = get_value( $details,$book_details_tblpref.'passenger_lastname' );	
			$passenger_firstname_child = get_value( $details,$book_details_tblpref.'passenger_firstname_child' );	
			$passenger_lastname_child = get_value( $details,$book_details_tblpref.'passenger_lastname_child' );	 
			$children_ages = get_value( $details,$book_details_tblpref.'children_ages' );
			$passenger_firstname = html_entity_decode( $passenger_firstname, ENT_QUOTES );  
			$passenger_lastname = html_entity_decode( $passenger_lastname, ENT_QUOTES ); 
			$passenger_firstname_child = html_entity_decode( $passenger_firstname_child, ENT_QUOTES ); 
			$passenger_lastname_child = html_entity_decode( $passenger_lastname_child, ENT_QUOTES ); 
			$children_ages = html_entity_decode( $children_ages, ENT_QUOTES ); 
			$passenger_first_name_details = check_if_serialized( ( $passenger_firstname ) );   
			$passenger_last_name_details = check_if_serialized( ( $passenger_lastname ) );
			$passenger_first_name_child_details = check_if_serialized( ( $passenger_firstname_child ) );
			$passenger_last_name_child_details = check_if_serialized( ( $passenger_lastname_child ) );
			$children_ages = check_if_serialized( ( $children_ages ) );
			
			if( $passenger_first_name_child_details ){
				$first_name = array_merge($passenger_first_name_details, $passenger_first_name_child_details);
			} else {
				$first_name = $passenger_first_name_details;
			}
			
			if( $passenger_last_name_child_details ){
				$last_name = array_merge($passenger_last_name_details, $passenger_last_name_child_details);
			} else {
				$last_name = $passenger_last_name_details;
			}
			 
			$length_first = count( $first_name );
			$length_last = count( $last_name );
			$fullname = array();
			
			if( $length_first > $length_last ){
				$base_count = $length_first;
			} else {
				$base_count =  $length_last;
			}
			
			for( $x=0; $x< $base_count;$x++ ){
				$first = preg_replace('/[^A-Za-z0-9\- \']/', '', $first_name[$x]);
				$second = preg_replace('/[^A-Za-z0-9\- \']/', '', $last_name[$x]);
				array_push( $fullname, $first.' '.$second);
			}
			$passenger = $fullname ; 
			$checkindate = get_value( $details,$book_details_tblpref.'start_date' ); 
			$checkoutdate = get_value( $details,$book_details_tblpref.'end_date' );
			
			$room = get_value( $details,$book_details_tblpref.'room' );
			$room_count = get_value( $details,$book_details_tblpref.'room' );
			$adult = get_value( $details,$book_details_tblpref.'adult' );
			$adult_count = get_value( $details,$book_details_tblpref.'adult' );
			$child = get_value( $details,$book_details_tblpref.'child' );
			$child_count = get_value( $details,$book_details_tblpref.'child' );
			
			$room_type = get_value( $details,$book_details_tblpref.'room_type' );
			$room_board = get_value( $details,$book_details_tblpref.'room_board' );
			$room_occupancy = get_value( $details,$book_details_tblpref.'room_occupancy' );
			$room_amount = get_value( $details,$book_details_tblpref.'room_amount_no_discount' );
			$discount_name = get_value( $details,$book_details_tblpref.'discount_name' );
			$discount_period = get_value( $details,$book_details_tblpref.'discount_period' );
			$discount_occupancy_amount = get_value( $details,$book_details_tblpref.'discount_occupancy_amount' );
			$total_discount_amount = get_value( $details,$book_details_tblpref.'total_discount_amount' );
			$get_comment = get_value( $details,$book_details_tblpref.'contract_comment' );
			$contract_comment = preg_replace('/[^A-Za-z0-9\-., \']/', ' ', $get_comment);
			$cancellation_policy_time = get_value( $details,$book_details_tblpref.'cancellation_policy_time' );
			$cancellation_policy_date = get_value( $details,$book_details_tblpref.'cancellation_policy_date' );
			$cancellation_policy_amount = get_value( $details,$book_details_tblpref.'cancellation_policy_amount' );
			$cancellation_policy_amount_currency = get_value( $details,$book_details_tblpref.'cancellation_policy_amount_currency' );
			$agency_reference = get_value( $details,$book_details_tblpref.'agency_reference' );
			$total_amount_currency = get_value( $details,$book_details_tblpref.'total_amount_currency' );
			$total_amount = get_value( $details,$book_details_tblpref.'total_amount' );
			$created = get_value( $details, $book_tblpref.'created' );
			$modified = get_value( $details, $book_tblpref.'modified' );
			$published = get_value( $details, $book_tblpref.'published' );
			$hotel_code = get_value( $details, 'HotelCode' );
			$hotel_name = get_value( $details, 'Name' );
			
			
			if( $room_count > 1 ){
				$room_count .= ' rooms';
			} else {
				$room_count .= ' room';
			}
			
			if( $adult_count > 1 ){
				$adult_count .= ' adults';
			} else {
				$adult_count .= ' adult';
			}
			
			if( $child_count > 0){
				if( $child_count > 1 ){
				$child_count .= ' children';
				} else {
					$child_count .= ' child';
				}
			} else {
				$child_count = Null;
			}
			
			$summary_occupancy = $room_count.' '.$adult_count.' '.$child_count;  
			 
			$bookings_data =  array( 	         
				'booking_reference' => $service_filenumber,        
				'service_ref' => $purchase_file_number,      
				'incoming_office' => $purchase_incoming_office,      
				'reference_number' => $purchase_incoming_office.'-'.$purchase_file_number, 			
				'booker_first_name' => $cub_first_name,      
				'booker_middle_name' => $cub_middle_name,     
				'booker_last_name' => $cub_last_name,       
				'booker_email' => $cub_email,     
				'booker_contact_no' => $cub_contact_no,       
				'booker_country' => $cub_country,    			
				'holder_name' => $holder_name,       
				'holder_last_name' => $holder_last_name,       
				'hotel_name' => $hotel_name,       
				'hotel_category' => $category_code,       
				'hotel_address' => $destination_name.' '.$destination_zone,       
				'hotel_availableroom' => $services,       
				'room_type' => $hotel_room_room_type,            
				'room_count' => $room_count,            
				'board' => $hotel_room_board,            
				'board_type' => $hotel_room_board,            
				'summary_occupancy' => $summary_occupancy,     
				'occupancy' => $hotel_room_occupancy,     
				'price' => $currency_response.' '.$hotel_room_amount,    
				'item_amount' => $currency_response.' '.$item_amount,    
				'contract_remarks' => $contract_comment,    
				'total_price' => $currency_response.' '.$total_amount,   
				'currency_response' => $currency_response,   
				'agency_reference' => $agency_reference,    
				'service_description' => $hotel_name,   
				'check_in' => $checkindate,   
				'check_out' => $checkoutdate,       
				'cancellation_date' => $cancellation_policy_date,   
				'cancellation_policy_time' => $cancellation_policy_time,   
				'cancellation_charge' => $currency_response.' '.$cancellation_policy_amount,
				'latitude' => $hotel_latitude,  
				'longitude' => $hotel_longitude,   
				'adults' => $customer_list_adult_count,   
				'children_ages' => $customer_list_child_age_array,   
				'children' => $customer_list_child_count,   
				'hotel_telephone' => $hotel_contact_number,    
				'hotel_fax' => $hotel_fax_number,    
				'final_price' => $final_price,    
				'hotel_email' => $hotel_email,
				'total_net_amount' => $final_total,
				'service_charge' => $service_charge,
				'tax_value' => $tax_value,
				'web_admin_fee' => $web_admin_fee,
				'book_or_number' => $book_or_number,
				'book_invoice_number' => $book_invoice_number,
				'booking_id' => $id  
			);  
			 
			successful_approve_email_to_client( $bookings_data );
			successful_approve_email_to_admin( $bookings_data );
		}
	 
		$booking_updated = $wpdb->update( 
			'wp_cribsandtrips_booking', 
			array(  
				$book_tblpref.'status' => 'confirmed'	// string 
			), 
			array( $book_tblpref.'id' => $id ) 
		); 	 
		 
		?> 
		<script>
		window.location.href='http://cribsandtrips.com/wp-admin/admin.php?page=<?php echo $page; ?>';
		</script>
		<?php
	}  
	// wp_redirect( site_url( '?page='.$page ) );  
}
function check_if_serialized( $name ){
	if( $name ){ 
		$serialize = is_serialized(( $name ));   
		if( $serialize ){
			return unserialize( $name );
		} else {
			return $name;
		}
	}
}

function datetime( $time_stamp, $format='Y-m-d H:i:s' ){
	if( $time_stamp ){
		$date_time = date($format, strtotime($time_stamp));
		if( $date_time == '1970-01-01'|| $date_time == '' ){
			$date_time = '0000-00-00';
		} 
		return $date_time;
	}  
}

function booking_status(  $status, $id, $service_status, $refund = null ){
	 
	$page = $_REQUEST['page']; 
	
	ob_start(); 	
	if( $service_status == 'CONFIRMED'){
		if( $status == 'paid' || $status == 'pending' ){
			$action1 = sprintf('<a href="?page=%s&action=%s&id=%s">Cancel</a>',$page,'cancel',$id ); 
			echo '<br />'.$action1;
		}
		
		if( $status == 'paid' ){
			$action2 = sprintf('<a href="?page=%s&action=%s&id=%s">Confirm</a>',$page,'confirmed',$id ); 
			echo '<br />'.$action2;
		}
	} 
	if( $refund && $refund == 1){
		$action3 = sprintf('<a href="?page=%s&action=%s&id=%s">Confirm Refund</a>',$page,'refund',$id ); 
		echo '<br />'.$action3;
	}
	$contents = ob_get_contents();
	ob_end_clean();
 
	return $contents;
}

function get_payment_pesopay( $id ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $id ){ 
		$sql  = "SELECT *" 
			." FROM `wp_cribsandtrips_booking_pesopay` " 
			." WHERE"
			." `book_id`='".$id."'"   
			;  	
		$result = $wpdb->get_row( $sql );
		 
		if( $result ){ 
			return $result;
		}
	}
} 

function get_payment_pesopay_details( $id ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $id ){ 
		$sql  = "SELECT *" 
			." FROM `wp_cribsandtrips_peso_pay_transactions` " 
			." WHERE"
			." `cppt_order_reference`='".$id."'"   
			;  	
		$result = $wpdb->get_row( $sql );
		 
		if( $result ){ 
			return $result;
		}
	}
} 

function get_pesopay_currency( $currency ){
   
	if( $currency == 344 ){
		$currency = 'HKD';
	} elseif( $currency == 840 ){
		$currency = 'USD';
	} elseif( $currency == 156 ){
		$currency = 'CNY';
	} elseif( $currency == 392 ){
		$currency = 'JPY';
	} elseif( $currency == 036 ){
		$currency = 'AUD';
	} elseif( $currency == 978 ){
		$currency = 'EUR';
	} elseif( $currency == 124 ){
		$currency = 'CAD';
	} elseif( $currency == 446 ){
		$currency = 'MOP';
	} elseif( $currency == 764 ){
		$currency = 'THB';
	} elseif( $currency == 458 ){
		$currency = 'MYR';
	} elseif( $currency == 410 ){
		$currency = 'KRW';
	} elseif( $currency == 682 ){
		$currency = 'SAR';
	} elseif( $currency == 784 ){
		$currency = 'AED';
	} elseif( $currency == 096 ){
		$currency = 'BND';
	} elseif( $currency == 356 ){
		$currency = 'INR';
	} elseif( $currency == 702 ){
		$currency = 'SGD';
	} elseif( $currency == 826 ){
		$currency = 'GBP';
	} elseif( $currency == 901 ){
		$currency = 'TWD';
	} elseif( $currency == 608 ){
		$currency = 'PHP';
	} elseif( $currency == 360 ){
		$currency = 'IDR';
	} elseif( $currency == 554 ){
		$currency = 'NZD';
	} elseif( $currency == 704 ){
		$currency = 'VND';
	}
	
	return $currency;
}

function get_payment_paypal( $id ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $id ){ 
		$sql  = "SELECT *" 
			." FROM `wp_cribsandtrips_booking_paypal` " 
			." WHERE"
			." `book_id`='".$id."'"   
			;  	
		$result = $wpdb->get_row( $sql );
		 
		if( $result ){ 
			return $result;
		}
	}
}  

function get_payment_eghl( $id ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $id ){ 
		$sql  = "SELECT *" 
			." FROM `wp_cribsandtrips_booking_eghl` " 
			." WHERE"
			." `book_id`='".$id."'"   
			;  	
		$result = $wpdb->get_row( $sql );
		 
		if( $result ){ 
			return $result;
		}
	}
} 

function get_country_name( $cub_country_code ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $cub_country_code ){ 
		$sql_country_code  = "SELECT *" 
			." FROM `wp_cribsandtrips_country_ids` " 
			." WHERE"
			." `CountryCode`='".$cub_country_code."'"  
			." AND `LanguageCode`='ENG'"  
			;  	
		$result_country_code = $wpdb->get_row( $sql_country_code );
		$cub_country = get_value( $result_country_code, 'Name' );
		if( $cub_country ){ 
			return $cub_country;
		}
	}
} 

function successful_cancel_email_to_client( $booking_params ){ 
	global $wpdb;
	global $session;
	global $theme_url;
	global $theme_dir;
	
	/* booking attributes */ 
	extract($booking_params); 
	$check_in = date( 'F d, Y', strtotime( $check_in ) );
	$check_out = date( 'F d, Y', strtotime( $check_out ) );
	$cancellation_date = date( 'F d, Y', strtotime( $cancellation_date ) );
	$booker_name =  $booker_first_name.' '.$booker_last_name;
	  
	//cribs and trips admin booking email
	$settings_booking_email = pods('bookingemailsetting');
	//one
	$settings_booking_email_booking_one = $settings_booking_email->field( 'email_no_1' );		
	$settings_booking_cc_email_one = $settings_booking_email->field( 'cc_no_1' );		
	$settings_booking_bcc_email_one= $settings_booking_email->field( 'bcc_no_1' );
	// two
	$settings_booking_email_booking_two = $settings_booking_email->field( 'email_no_2' );		
	$settings_booking_cc_email_two = $settings_booking_email->field( 'cc_no_2' );		
	$settings_booking_bcc_email_two = $settings_booking_email->field( 'bcc_no_2' );
	 
	 
	// cribs and trips admin setting 
	$settings_general = pods('admin');   
	$admin_email= $settings_general->field( 'email' );
	$admin_company_name= $settings_general->field( 'name' );
	$admin_system_description= $settings_general->field( 'description' );
	$admin_country= $settings_general->field( 'country' );
	$admin_address= $settings_general->field( 'address' );
	$admin_postcode= $settings_general->field( 'postcode' );
	$admin_fax_number= $settings_general->field( 'fax_number' );
	$admin_phone_number= $settings_general->field( 'phone_number' );
	$admin_website= $settings_general->field( 'website' );
	$admin_emergency_number= $settings_general->field( 'emergency_number' );
	$admin_tin_no= $settings_general->field( 'tin_no' );
	$admin_tax_reg_no= $settings_general->field( 'service_tax_reg_no' );
			 
	// $timezone = date_default_timezone_set( "Asia/Singapore" );
	$timezone = date_default_timezone_set( "Asia/Manila" );
	$date_today = date('M d, Y H:i:s'); 
	$format_date_today = date( 'F d, Y', strtotime( $date_today ) );
	$receipt_format_date_today = date( 'm/d/Y', strtotime( $date_today ) );
	$time_format = date( "g:i A", time( $date_today ) );    
	$timestamp = strtotime( $date_today ); 
	$day = date( 'l', $timestamp );
	$final_date = $day.', '.$format_date_today.' at '.$time_format;
	$final_price = (float)$final_price;
	$final_price = number_format( $final_price,2,'.',',' );  
	$total_net_amount = (float)$total_net_amount;
	$total_net_amount = number_format( $total_net_amount,2,'.',',' );     
	$cancellation_policy_time = iwp_show_local_time( $cancellation_policy_time );
	  
	/* Email Message */
	$message_params =  array(   
		'admin_email' => $admin_email,      
		'admin_company_name' => $admin_company_name,      
		'admin_address' => $admin_address,      
		'admin_postcode' => $admin_postcode,      
		'admin_country' => $admin_country,      
		'admin_phone_number' => $admin_phone_number,      
		'admin_fax_number' => $admin_fax_number,      
		'admin_website' => $admin_website,      
		'final_date' => $final_date,      
		'reference_number' => $reference_number,      
		'agency_reference' => $agency_reference,   
		'booker_email' => $booker_email,      
		'booker_name' => $booker_name,      
		'booker_contact_no' => $booker_contact_no,      
		'booker_country' => $booker_country,         
		'service_description' => $service_description,         
		'cancellation_date' => $cancellation_date,         
		'summary_occupancy' => $summary_occupancy,         
		'check_in' => $check_in,         
		'check_out' => $check_out,         
		'check_out' => $check_out,      
	);    
	$message = CancelEmail::message_to_client( $message_params );
	   
	/* Email Subject */ 
	if( !empty( $admin_company_name ) && !empty( $booker_first_name )&& !empty( $booking_id ) ){
		$email_subject = $admin_company_name.' Hotel Booking Cancelled Notification for '.$booker_first_name.' (Trans #:'.$booking_id.')';
	} elseif( empty( $admin_company_name ) && !empty( $booker_first_name )&& !empty( $booking_id ) ){
		$email_subject = 'Hotel Booking Cancelled Notification for '.$booker_first_name.' (Trans #:'.$booking_id.')';
	} elseif( !empty( $admin_company_name ) && empty( $booker_first_name )&& !empty( $booking_id ) ){
		$email_subject = $admin_company_name.' Hotel Booking Cancelled Notification for (Trans #:'.$booking_id.')';
	} elseif( !empty( $admin_company_name ) && !empty( $booker_first_name )&& empty( $booking_id ) ){
		$email_subject = $admin_company_name.' Hotel Booking Cancelled Notification for '.$booker_first_name;
	} elseif( empty( $admin_company_name ) && empty( $booker_first_name )&& empty( $booking_id ) ){
		$email_subject = 'Hotel Booking Cancelled Notification';
	} else {
		$email_subject = 'Hotel Booking Cancelled Notification';
	}
		
	/* Email Sender */
	$attachment_list = array( $file_location, $file_location_receipt );
	$headers[] = 'Content-Type: text/html';
	$headers[] = 'charset=UTF-8';
	$headers[] = 'From: '.$admin_company_name.' <'.$admin_email.'>';
  
	/* Email Send */
	$send = wp_mail( $booker_email, $email_subject, $message, $headers, $attachment_list );
	   
}
	
function successful_cancel_email_to_admin( $booking_params ){ 
	
	global $wpdb;
	global $session;
	global $theme_url;
	global $theme_dir;
  
	/* booking attributes */
	extract($booking_params);  
	
	$booker_name =  $booker_first_name.' '.$booker_last_name;
	$check_in = date( 'F d, Y', strtotime( $check_in ) );
	$check_out = date( 'F d, Y', strtotime( $check_out ) );
	$cancellation_date = date( 'F d, Y', strtotime( $cancellation_date ) );
	
		//cribs and trips admin booking email
	$settings_booking_email = pods('bookingemailsetting');
	//one
	$settings_booking_email_booking_one = $settings_booking_email->field( 'email_no_1' );		
	$settings_booking_cc_email_one = $settings_booking_email->field( 'cc_no_1' );		
	$settings_booking_bcc_email_one= $settings_booking_email->field( 'bcc_no_1' );
	// two
	$settings_booking_email_booking_two = $settings_booking_email->field( 'email_no_2' );		
	$settings_booking_cc_email_two = $settings_booking_email->field( 'cc_no_2' );		
	$settings_booking_bcc_email_two = $settings_booking_email->field( 'bcc_no_2' );
	 
	// cribs and trips admin setting  
	$settings_general = pods('admin');   
	$admin_email= $settings_general->field( 'email' );
	$admin_company_name= $settings_general->field( 'name' );
	$admin_system_description= $settings_general->field( 'description' );
	$admin_country= $settings_general->field( 'country' );
	$admin_address= $settings_general->field( 'address' );
	$admin_postcode= $settings_general->field( 'postcode' );
	$admin_fax_number= $settings_general->field( 'fax_number' );
	$admin_phone_number= $settings_general->field( 'phone_number' );
	$admin_website= $settings_general->field( 'website' );
	$admin_emergency_number= $settings_general->field( 'emergency_number' );
	$admin_tin_no= $settings_general->field( 'tin_no' );
	$admin_tax_reg_no= $settings_general->field( 'service_tax_reg_no' );
	
	// $timezone = date_default_timezone_set( "Asia/Singapore" );
	$timezone = date_default_timezone_set( "Asia/Manila" );
	$date_today = date('M d, Y H:i:s'); 
	$format_date_today = date( 'F d, Y', strtotime( $date_today ) );
	$receipt_format_date_today = date( 'm/d/Y', strtotime( $date_today ) );
	$time_format = date( "g:i A", time( $date_today ) );    
	$timestamp = strtotime( $date_today ); 
	$day = date( 'l', $timestamp );
	$final_date = $day.', '.$format_date_today.' at '.$time_format;
	$final_price = (float)$final_price;
	$final_price = number_format( $final_price,2,'.',',' );  
	$total_net_amount = (float)$total_net_amount;
	$total_net_amount = number_format( $total_net_amount,2,'.',',' );     
	$cancellation_policy_time = iwp_show_local_time( $cancellation_policy_time );
	 
	/* Email Message */
	$message_params =  array(   
		'admin_email' => $admin_email,      
		'admin_company_name' => $admin_company_name,      
		'admin_address' => $admin_address,      
		'admin_postcode' => $admin_postcode,      
		'admin_country' => $admin_country,      
		'admin_phone_number' => $admin_phone_number,      
		'admin_fax_number' => $admin_fax_number,      
		'admin_website' => $admin_website,      
		'final_date' => $final_date,      
		'reference_number' => $reference_number,      
		'agency_reference' => $agency_reference,   
		'booker_email' => $booker_email,      
		'booker_name' => $booker_name,      
		'booker_contact_no' => $booker_contact_no,      
		'booker_country' => $booker_country,         
		'service_description' => $service_description,         
		'cancellation_date' => $cancellation_date,         
		'summary_occupancy' => $summary_occupancy,         
		'check_in' => $check_in,         
		'check_out' => $check_out,         
		'check_out' => $check_out,      
	);    
	$message = CancelEmail::message_to_admin( $message_params );
	 
	/* Email Receiver */  
	$email_list = array( $settings_booking_email_booking_one, $settings_booking_email_booking_two );
	$bcc_email_list = array( $settings_booking_bcc_email_one, $settings_booking_bcc_email_two ); 
	$cc_email_list = array( $settings_booking_cc_email_one, $settings_booking_cc_email_two ); 
 
	/* Email Subject */  
	if( !empty( $admin_company_name ) && !empty( $booker_first_name ) && !empty( $booking_id ) ){ 
		$email_subject = $admin_company_name.' Admin Hotel Booking Cancelled Notification for '.$booker_first_name.' (Trans #:'.$booking_id.')';
	} elseif( empty( $admin_company_name ) && !empty( $booker_first_name ) && !empty( $booking_id ) ){ 
		$email_subject = 'Admin Hotel Booking Cancelled Notification for '.$booker_first_name.' (Trans #:'.$booking_id.')';
	} elseif( empty( $admin_company_name ) && empty( $booker_first_name ) && !empty( $booking_id ) ){ 
		$email_subject = 'Admin Hotel Booking Cancelled Notification for (Trans #:'.$booking_id.')';
	} elseif( !empty( $admin_company_name ) && !empty( $booker_first_name ) && empty( $booking_id ) ){ 
		$email_subject = $admin_company_name.' Admin Hotel Booking Cancelled Notification for '.$booker_first_name;
	} elseif( empty( $admin_company_name ) && empty( $booker_first_name ) && empty( $booking_id ) ){ 
		$email_subject = 'Admin Hotel Booking Cancelled Notification';
	}
	 
	/* Email Sender */ 
	$attachment_list = array( $file_location_voucher, $file_location_invoice );
	$headers[] = 'Content-Type: text/html';
	$headers[] = 'charset=UTF-8';
	$headers[] = 'From: '.$admin_company_name.' <'.$admin_email.'>'; 
	if( $settings_booking_cc_email_one ){
		$headers[] = 'Cc: '.$settings_booking_cc_email_one; // 
	}
	if( $settings_booking_cc_email_two ){
		$headers[] = 'Cc: '.$settings_booking_cc_email_two; // 
	}
	if( $settings_booking_bcc_email_one ){
		$headers[] = 'Bcc: '.$settings_booking_bcc_email_one; // 
	}
	if( $settings_booking_bcc_email_two ){
		$headers[] = 'Bcc: '.$settings_booking_bcc_email_two; // 
	}
	
	/* Email Send */ 
	$send = wp_mail( $email_list, $email_subject, $message, $headers, $attachment_list );
	 
} 

function successful_approve_email_to_client( $booking_params ){ 
	global $wpdb;
	global $session;
	global $theme_url;
	global $theme_dir;
	
	/* booking attributes */ 
	extract($booking_params); 
	$check_in = date( 'F d, Y', strtotime( $check_in ) );
	$check_out = date( 'F d, Y', strtotime( $check_out ) );
	$cancellation_date = date( 'F d, Y', strtotime( $cancellation_date ) );
	$booker_name =  $booker_first_name.' '.$booker_last_name;
	  
	//cribs and trips admin booking email
	$settings_booking_email = pods('bookingemailsetting');
	//one
	$settings_booking_email_booking_one = $settings_booking_email->field( 'email_no_1' );		
	$settings_booking_cc_email_one = $settings_booking_email->field( 'cc_no_1' );		
	$settings_booking_bcc_email_one= $settings_booking_email->field( 'bcc_no_1' );
	// two
	$settings_booking_email_booking_two = $settings_booking_email->field( 'email_no_2' );		
	$settings_booking_cc_email_two = $settings_booking_email->field( 'cc_no_2' );		
	$settings_booking_bcc_email_two = $settings_booking_email->field( 'bcc_no_2' );
	 
	 
	// cribs and trips admin setting 
	$settings_general = pods('admin');   
	$admin_email= $settings_general->field( 'email' );
	$admin_company_name= $settings_general->field( 'name' );
	$admin_system_description= $settings_general->field( 'description' );
	$admin_country= $settings_general->field( 'country' );
	$admin_address= $settings_general->field( 'address' );
	$admin_postcode= $settings_general->field( 'postcode' );
	$admin_fax_number= $settings_general->field( 'fax_number' );
	$admin_phone_number= $settings_general->field( 'phone_number' );
	$admin_website= $settings_general->field( 'website' );
	$admin_emergency_number= $settings_general->field( 'emergency_number' );
	$admin_tin_no= $settings_general->field( 'tin_no' );
	$admin_tax_reg_no= $settings_general->field( 'service_tax_reg_no' );
			 
	// $timezone = date_default_timezone_set( "Asia/Singapore" );
	$timezone = date_default_timezone_set( "Asia/Manila" );
	$date_today = date('M d, Y H:i:s'); 
	$format_date_today = date( 'F d, Y', strtotime( $date_today ) );
	$receipt_format_date_today = date( 'm/d/Y', strtotime( $date_today ) );
	$time_format = date( "g:i A", time( $date_today ) );    
	$timestamp = strtotime( $date_today ); 
	$day = date( 'l', $timestamp );
	$final_date = $day.', '.$format_date_today.' at '.$time_format;
	$final_price = (float)$final_price;
	$final_price = number_format( $final_price,2,'.',',' );  
	$total_net_amount = (float)$total_net_amount;
	$total_net_amount = number_format( $total_net_amount,2,'.',',' );     
	$cancellation_policy_time = iwp_show_local_time( $cancellation_policy_time );
	 
	/* Email Message */
	$message_params =  array(   
		'admin_email' => $admin_email,      
		'admin_company_name' => $admin_company_name,      
		'admin_address' => $admin_address,      
		'admin_postcode' => $admin_postcode,      
		'admin_country' => $admin_country,      
		'admin_phone_number' => $admin_phone_number,      
		'admin_fax_number' => $admin_fax_number,      
		'admin_website' => $admin_website,      
		'final_date' => $final_date,      
		'reference_number' => $reference_number,      
		'agency_reference' => $agency_reference,   
		'booker_email' => $booker_email,      
		'booker_name' => $booker_name,      
		'booker_contact_no' => $booker_contact_no,      
		'booker_country' => $booker_country,         
		'service_description' => $service_description,         
		'cancellation_date' => $cancellation_date,         
		'summary_occupancy' => $summary_occupancy,         
		'check_in' => $check_in,         
		'check_out' => $check_out,         
		'check_out' => $check_out,         
	 
	);   
   	
	$message = ApproveEmail::message_to_client( $message_params );
  
	/* Email Subject */ 
	if( !empty( $admin_company_name ) && !empty( $booker_first_name )&& !empty( $booking_id ) ){
		$email_subject = $admin_company_name.' Hotel Booking Approved Notification for '.$booker_first_name.' (Trans #:'.$booking_id.')';
	} elseif( empty( $admin_company_name ) && !empty( $booker_first_name )&& !empty( $booking_id ) ){
		$email_subject = 'Hotel Booking Approved Notification for '.$booker_first_name.' (Trans #:'.$booking_id.')';
	} elseif( !empty( $admin_company_name ) && empty( $booker_first_name )&& !empty( $booking_id ) ){
		$email_subject = $admin_company_name.' Hotel Booking Approved Notification for (Trans #:'.$booking_id.')';
	} elseif( !empty( $admin_company_name ) && !empty( $booker_first_name )&& empty( $booking_id ) ){
		$email_subject = $admin_company_name.' Hotel Booking Approved Notification for '.$booker_first_name;
	} elseif( empty( $admin_company_name ) && empty( $booker_first_name )&& empty( $booking_id ) ){
		$email_subject = 'Hotel Booking Approved Notification';
	} else {
		$email_subject = 'Hotel Booking Approved Notification';
	} 
	
	/* Email Sender */
	$headers[] = 'Content-Type: text/html';
	$headers[] = 'charset=UTF-8';
	$headers[] = 'From: '.$admin_company_name.' <'.$admin_email.'>';
	$headers[] = 'Cc: mark <mark@iwebprovider.com>';
	
	/* Email Send */
	$send = wp_mail( $booker_email, $email_subject, $message, $headers ); 
}
	
function successful_approve_email_to_admin( $booking_params ){ 
	
	global $wpdb;
	global $session;
	global $theme_url;
	global $theme_dir;
  
	/* booking attributes */
	extract($booking_params);  
	
	$booker_name =  $booker_first_name.' '.$booker_last_name;
	$check_in = date( 'F d, Y', strtotime( $check_in ) );
	$check_out = date( 'F d, Y', strtotime( $check_out ) );
	$cancellation_date = date( 'F d, Y', strtotime( $cancellation_date ) );
	
		//cribs and trips admin booking email
	$settings_booking_email = pods('bookingemailsetting');
	//one
	$settings_booking_email_booking_one = $settings_booking_email->field( 'email_no_1' );		
	$settings_booking_cc_email_one = $settings_booking_email->field( 'cc_no_1' );		
	$settings_booking_bcc_email_one= $settings_booking_email->field( 'bcc_no_1' );
	// two
	$settings_booking_email_booking_two = $settings_booking_email->field( 'email_no_2' );		
	$settings_booking_cc_email_two = $settings_booking_email->field( 'cc_no_2' );		
	$settings_booking_bcc_email_two = $settings_booking_email->field( 'bcc_no_2' );
	 
	// cribs and trips admin setting  
	$settings_general = pods('admin');   
	$admin_email= $settings_general->field( 'email' );
	$admin_company_name= $settings_general->field( 'name' );
	$admin_system_description= $settings_general->field( 'description' );
	$admin_country= $settings_general->field( 'country' );
	$admin_address= $settings_general->field( 'address' );
	$admin_postcode= $settings_general->field( 'postcode' );
	$admin_fax_number= $settings_general->field( 'fax_number' );
	$admin_phone_number= $settings_general->field( 'phone_number' );
	$admin_website= $settings_general->field( 'website' );
	$admin_emergency_number= $settings_general->field( 'emergency_number' );
	$admin_tin_no= $settings_general->field( 'tin_no' );
	$admin_tax_reg_no= $settings_general->field( 'service_tax_reg_no' );
	
	// $timezone = date_default_timezone_set( "Asia/Singapore" );
	$timezone = date_default_timezone_set( "Asia/Manila" );
	$date_today = date('M d, Y H:i:s'); 
	$format_date_today = date( 'F d, Y', strtotime( $date_today ) );
	$receipt_format_date_today = date( 'm/d/Y', strtotime( $date_today ) );
	$time_format = date( "g:i A", time( $date_today ) );    
	$timestamp = strtotime( $date_today ); 
	$day = date( 'l', $timestamp );
	$final_date = $day.', '.$format_date_today.' at '.$time_format;
	$final_price = (float)$final_price;
	$final_price = number_format( $final_price,2,'.',',' );  
	$total_net_amount = (float)$total_net_amount;
	$total_net_amount = number_format( $total_net_amount,2,'.',',' );     
	$cancellation_policy_time = iwp_show_local_time( $cancellation_policy_time );
	 
	 
	/* Email Message */ /* Email Message */
	$message_params =  array(   
		'admin_email' => $admin_email,      
		'admin_company_name' => $admin_company_name,      
		'admin_address' => $admin_address,      
		'admin_postcode' => $admin_postcode,      
		'admin_country' => $admin_country,      
		'admin_phone_number' => $admin_phone_number,      
		'admin_fax_number' => $admin_fax_number,      
		'admin_website' => $admin_website,      
		'final_date' => $final_date,      
		'reference_number' => $reference_number,      
		'agency_reference' => $agency_reference,   
		'booker_email' => $booker_email,      
		'booker_name' => $booker_name,      
		'booker_contact_no' => $booker_contact_no,      
		'booker_country' => $booker_country,         
		'service_description' => $service_description,         
		'cancellation_date' => $cancellation_date,         
		'summary_occupancy' => $summary_occupancy,         
		'check_in' => $check_in,         
		'check_out' => $check_out,         
		'check_out' => $check_out,         
	 
	);   
   	
	$message = ApproveEmail::message_to_admin( $message_params );
	
	/* Email Receiver */  
	$email_list = array( $settings_booking_email_booking_one, $settings_booking_email_booking_two );
	$bcc_email_list = array( $settings_booking_bcc_email_one, $settings_booking_bcc_email_two ); 
	$cc_email_list = array( $settings_booking_cc_email_one, $settings_booking_cc_email_two ); 
 
	/* Email Subject */  
	if( !empty( $admin_company_name ) && !empty( $booker_first_name ) && !empty( $booking_id ) ){ 
		$email_subject = $admin_company_name.' Admin Hotel Booking Approved Notification for '.$booker_first_name.' (Trans #:'.$booking_id.')';
	} elseif( empty( $admin_company_name ) && !empty( $booker_first_name ) && !empty( $booking_id ) ){ 
		$email_subject = 'Admin Hotel Booking Approved Notification for '.$booker_first_name.' (Trans #:'.$booking_id.')';
	} elseif( empty( $admin_company_name ) && empty( $booker_first_name ) && !empty( $booking_id ) ){ 
		$email_subject = 'Admin Hotel Booking Approved Notification for (Trans #:'.$booking_id.')';
	} elseif( !empty( $admin_company_name ) && !empty( $booker_first_name ) && empty( $booking_id ) ){ 
		$email_subject = $admin_company_name.' Admin Hotel Booking Approved Notification for '.$booker_first_name;
	} elseif( empty( $admin_company_name ) && empty( $booker_first_name ) && empty( $booking_id ) ){ 
		$email_subject = 'Admin Hotel Booking Approved Notification';
	} else { 
		$email_subject = 'Admin Hotel Booking Approved Notification';
	}
	  
	/* Email Sender */
	$headers[] = 'Content-Type: text/html';
	$headers[] = 'charset=UTF-8';
	$headers[] = 'From: '.$admin_company_name.' <'.$admin_email.'>'; 
	if( $settings_booking_cc_email_one ){
		$headers[] = 'Cc: '.$settings_booking_cc_email_one; // 
	}
	if( $settings_booking_cc_email_two ){
		$headers[] = 'Cc: '.$settings_booking_cc_email_two; // 
	}
	if( $settings_booking_bcc_email_one ){
		$headers[] = 'Bcc: '.$settings_booking_bcc_email_one; // 
	}
	if( $settings_booking_bcc_email_two ){
		$headers[] = 'Bcc: '.$settings_booking_bcc_email_two; // 
	}
	 
	/* Email Send */
	$send = wp_mail( $email_list, $email_subject, $message, $headers ); 
}  

function resend_voucher_client(){  

	global $wpdb; //This is used only if making any database queries  
	global $theme_dir; 
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 

	if( $id && $action == 'resend_voucher_client' ){
		
		$result = get_records( $id );
		
		foreach( $result as $details ){
			
			$book_details_tblpref = 'bkd_';
			$book_tblpref = 'book_';
			
			$cub_first_name = get_value( $details, $book_tblpref.'first_name' );
			$cub_middle_name = get_value( $details, $book_tblpref.'middle_name' );
			$cub_last_name = get_value( $details, $book_tblpref.'last_name' );
			$cub_contact_no = get_value( $details, $book_tblpref.'contact_no' );
			$cub_email = get_value( $details, $book_tblpref.'email' );
			$cub_birthday = get_value( $details, $book_tblpref.'birthday' );
			$cub_country_code = get_value( $details, $book_tblpref.'country' );
			$cub_country = get_country_name( $cub_country_code );
			
			$room = get_value( $details,$book_details_tblpref.'room' );
			$room_count = get_value( $details,$book_details_tblpref.'room' );
			$adult = get_value( $details,$book_details_tblpref.'adult' );
			$adult_count = get_value( $details,$book_details_tblpref.'adult' );
			$child = get_value( $details,$book_details_tblpref.'child' );
			$child_count = get_value( $details,$book_details_tblpref.'child' );
			
			$file_number = get_value( $details, $book_tblpref.'file_number' );
			$incoming_office = get_value( $details, $book_details_tblpref.'incoming_office' );
			$agency_reference = get_value( $details, $book_details_tblpref.'agency_reference' );
			$book_date = get_value( $details, $book_tblpref.'created' ); 
			$book_date = datetime($book_date,'m/d/Y'); 
			 
			$cancellation_policy_time = get_value( $details,$book_details_tblpref.'cancellation_policy_time' );
			$cancellation_policy_date = get_value( $details,$book_details_tblpref.'cancellation_policy_date' );
			$cancellation_policy_amount = get_value( $details,$book_details_tblpref.'cancellation_policy_amount' );
			$cancellation_policy_amount_currency = get_value( $details,$book_details_tblpref.'cancellation_policy_amount_currency' );
		}
		 

		$purchaise_details = PurchaseXML::view_purchaise_details( $file_number, $incoming_office );
		if( !empty( $purchaise_details ) ){
			$xml = simplexml_load_string( $purchaise_details ); 
			if( !empty( $xml ) ){ 
				$purchase = $xml->Purchase;	 
				$services = $xml->Purchase->ServiceList->Service;  
				if( !empty( $purchase ) ){ 
					foreach( $purchase as $purchases => $tags ){   

						$holder_name_xml = $tags->Holder->Name;   
						$holder_name = ucfirst( strtolower( $holder_name_xml ) );
						$holder_last_name_xml = $tags->Holder->LastName;  
						$holder_last_name = ucfirst( strtolower( $holder_last_name_xml ) );  
						
						$purchase_file_number = $tags->Reference->FileNumber;  
						$purchase_incoming_office = $tags->Reference->IncomingOffice->attributes()->code;  
						$purchase_token = $tags->attributes()->purchaseToken;  
						$purchase_token_time_to_expiration = $tags->attributes()->timeToExpiration;  
						$service_type = $tags->ServiceList->Service->attributes()->type;  
						$service_filenumber = $tags->ServiceList->Service->Reference->FileNumber; 
						$service_spui = $tags->ServiceList->Service->attributes()->SPUI;  
						$currency_response = $tags->ServiceList->Service->Currency->attributes()->code;  
						$contract_comment = $tags->ServiceList->Service->ContractList->Contract->CommentList->Comment;
						$amount = $tags->ServiceList->Service->TotalAmount; 
						$total_amount = $tags->ServiceList->Service->TotalAmount; 
						$total_amount = Hotel::commission( $total_amount );
						$discountlist = $tags->ServiceList->Service->DiscountList;
						if( $discountlist ){
							$discount_description = $tags->ServiceList->Service->DiscountList->Price->Description; 
						}
						$hotel_code = $tags->ServiceList->Service->HotelInfo->Code;
						$hotel_name = $tags->ServiceList->Service->HotelInfo->Name;
						$hotel_category = $tags->ServiceList->Service->HotelInfo->Category;
						$destination_name = $tags->ServiceList->Service->HotelInfo->Destination->Name;
						$destination_zone = $tags->ServiceList->Service->HotelInfo->Destination->ZoneList->Zone;
						$category_code = $tags->ServiceList->Service->HotelInfo->Category;  
						$checkindate = $tags->ServiceList->Service->DateFrom->attributes()->date; 
						$checkindate = date( "Y-m-d", strtotime( $checkindate ) );
						$checkoutdate = $tags->ServiceList->Service->DateTo->attributes()->date;  
						$checkoutdate = date( "Y-m-d", strtotime( $checkoutdate ) ); 
						$customer_list = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->GuestList->Customer; 
						$customer_list_adult_count = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->AdultCount; 
						$customer_list_child_count = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->ChildCount; 
				 
						$nights = (strtotime($checkoutdate) - strtotime($checkindate)) / 86400; 
						if( $customer_list ){ 
							foreach( $customer_list as $customer => $row ){  
								$customer_list_type_array[] = $row->attributes()->type;
								$customer_list_istravel_agent_array[] = $row->attributes()->isTravelAgent;
								$customer_list_customer_id_array[] = $row->CustomerId;
								
								if( $row->Age <= 18 ){
									$customer_list_child_age_array[] = $row->Age; 
								} else {
									$customer_list_adult_age_array[] = $row->Age; 
								} 
							}   
						}   
						
						$total_amount_nocommission = $tags->ServiceList->Service->TotalAmount; 
						$total_amount_nocommission = (float)$total_amount_nocommission;   
						$total_amount_nocommission = number_format( $total_amount_nocommission,2,'.',',' );  
						   
					} 
					
					$tax_value = Hotel::value_added_tax( $total_amount_nocommission );
					$service_charge = Hotel::service_charge( $total_amount );
					$web_admin_fee = Hotel::web_admin_fee( $total_amount );		

					$final_price = $total_amount;
					$final_total = $total_amount + $service_charge + $tax_value;
					
					if( $hotel_code ){ 
						$hotel_details_from_xml = BookHotelXML::get_hotels_details( $hotel_code );
						if( !empty(  $hotel_details_from_xml ) ){ 
							$hotel_details_xml = simplexml_load_string( $hotel_details_from_xml );  
							if( !empty(  $hotel_details_xml ) ){ 
								$hotel_contact = $hotel_details_xml->Hotel->Contact; 
								$hotel_position = $hotel_details_xml->Hotel->Position;
								if( !empty( $hotel_contact ) ){ 
									foreach( $hotel_contact as $contact => $c_list ){
										$hotel_email = $c_list->EmailList->Email;	
										$hotel_contact_number = $c_list->PhoneList->ContactNumber;	
										$hotel_fax_number = $c_list->FaxList->ContactNumber;	
										$hotel_website = $c_list->WebList->Web;	 
									}
								}
								if( !empty( $hotel_position ) ){ 
									foreach( $hotel_position as $position => $p_list ){	
										$hotel_latitude = $p_list->attributes()->latitude;   
										$hotel_longitude = $p_list->attributes()->longitude;  
									}		 
								}		 
							}		 
						}
						
						if( !empty( $destination_name ) && !empty( $destination_zone ) ){
							$destination = $destination_name.' '.$destination_zone;
						} elseif( !empty( $destination_name ) && empty( $destination_zone ) ){
							$destination = $destination_name;
						} else {
							$destination = $destination_zone;
						}  
						
						if( $room_count > 1 ){
							$room_count .= ' rooms';
						} else {
							$room_count .= ' room';
						}
						
						if( $adult_count > 1 ){
							$adult_count .= ' adults';
						} else {
							$adult_count .= ' adult';
						}
						
						if( $child_count > 0){
							if( $child_count > 1 ){
							$child_count .= ' children';
							} else {
								$child_count .= ' child';
							}
						} else {
							$child_count = Null;
						}
						
						$summary_occupancy = $room_count.' '.$adult_count.' '.$child_count;  
						$reference_number = $purchase_incoming_office.'-'.$purchase_file_number; 
			 
						
						// $plugin_dir_url = plugin_dir_url( __FILE__ );  
						$check_in = date( 'F d, Y', strtotime( $checkindate ) );
						$check_out = date( 'F d, Y', strtotime( $checkoutdate ) );
						$cancellation_date = date( 'F d, Y', strtotime( $cancellation_policy_date ) );
						$booker_name =  $cub_first_name.' '.$cub_last_name;
						  
						//cribs and trips admin booking email
						$settings_booking_email = pods('bookingemailsetting');
						//one
						$settings_booking_email_booking_one = $settings_booking_email->field( 'email_no_1' );		
						$settings_booking_cc_email_one = $settings_booking_email->field( 'cc_no_1' );		
						$settings_booking_bcc_email_one= $settings_booking_email->field( 'bcc_no_1' );
						// two
						$settings_booking_email_booking_two = $settings_booking_email->field( 'email_no_2' );		
						$settings_booking_cc_email_two = $settings_booking_email->field( 'cc_no_2' );		
						$settings_booking_bcc_email_two = $settings_booking_email->field( 'bcc_no_2' );
		  
						// cribs and trips admin setting 
						$settings_general = pods('admin');   
						$admin_email= $settings_general->field( 'email' );
						$admin_company_name= $settings_general->field( 'name' );
						$admin_system_description= $settings_general->field( 'description' );
						$admin_country= $settings_general->field( 'country' );
						$admin_address= $settings_general->field( 'address' );
						$admin_postcode= $settings_general->field( 'postcode' );
						$admin_fax_number= $settings_general->field( 'fax_number' );
						$admin_phone_number= $settings_general->field( 'phone_number' );
						$admin_website= $settings_general->field( 'website' );
						$admin_emergency_number= $settings_general->field( 'emergency_number' );
						$admin_tin_no= $settings_general->field( 'tin_no' );
						$admin_tax_reg_no= $settings_general->field( 'service_tax_reg_no' );
								 
						// $timezone = date_default_timezone_set( "Asia/Singapore" );
						$timezone = date_default_timezone_set( "Asia/Manila" );
						$date_today = date('M d, Y H:i:s'); 
						$format_date_today = date( 'F d, Y', strtotime( $date_today ) );
						$receipt_format_date_today = date( 'm/d/Y', strtotime( $date_today ) );
						$time_format = date( "g:i A", time( $date_today ) );    
						$timestamp = strtotime( $date_today ); 
						$day = date( 'l', $timestamp );
						$final_date = $day.', '.$format_date_today.' at '.$time_format;
						$final_price = (float)$final_price;
						$final_price = number_format( $final_price,2,'.',',' );  
						$total_net_amount = (float)$total_net_amount;
						$total_net_amount = number_format( $total_net_amount,2,'.',',' );     
						$cancellation_policy_time = iwp_show_local_time( $cancellation_policy_time );
						
						/* Email Message */
						$message_params =  array(   
							'admin_email' => $admin_email,      
							'admin_company_name' => $admin_company_name,      
							'admin_address' => $admin_address,      
							'admin_postcode' => $admin_postcode,      
							'admin_country' => $admin_country,      
							'admin_phone_number' => $admin_phone_number,      
							'admin_fax_number' => $admin_fax_number,      
							'admin_website' => $admin_website,      
							'final_date' => $final_date,      
							'reference_number' => $reference_number,      
							'agency_reference' => $agency_reference,   
							'booker_email' => $cub_email,      
							'booker_name' => $booker_name,      
							'booker_contact_no' => $cub_contact_no,      
							'booker_country' => $cub_country,         
							'service_description' => $hotel_name,         
							'cancellation_date' => $cancellation_date,         
							'summary_occupancy' => $summary_occupancy,         
							'check_in' => $check_in,         
							'check_out' => $check_out,         
							'check_out' => $check_out,         
						 
						);    
						$message = NotificationEmail::general_message( $message_params );
						   
						/* Email Receiver */  
						$email_list = array( $settings_booking_email_booking_one, $settings_booking_email_booking_two, $cub_email );
						$bcc_email_list = array( $settings_booking_bcc_email_one, $settings_booking_bcc_email_two ); 
						$cc_email_list = array( $settings_booking_cc_email_one, $settings_booking_cc_email_two ); 
				    
						/* Email Subject */  
						if( !empty( $admin_company_name ) && !empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = $admin_company_name.'Hotel Booking Notification for '.$cub_first_name.' (Trans #:'.$id.')';
						} elseif( empty( $admin_company_name ) && !empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = 'Hotel Booking Notification for '.$cub_first_name.' (Trans #:'.$id.')';
						} elseif( !empty( $admin_company_name ) && empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = $admin_company_name.'Hotel Booking Notification for (Trans #:'.$id.')';
						} elseif( !empty( $admin_company_name ) && !empty( $cub_first_name )&& empty( $id ) ){
							$email_subject = $admin_company_name.'Hotel Booking Notification for '.$cub_first_name;
						} elseif( empty( $admin_company_name ) && empty( $cub_first_name )&& empty( $id ) ){
							$email_subject = 'Hotel Booking Notification';
						} else {
							$email_subject = 'Hotel Booking Notification';
						}
						 
						/* Email Sender */  
						$headers[] = 'Content-Type: text/html';
						$headers[] = 'charset=UTF-8';
						$headers[] = 'From: '.$admin_company_name.' <'.$admin_email.'>';
						 
						/* Email Attachment */  
						$bookings_data =  array( 	 
							'hotel_availableroom' => $services,       
							'booking_date' => $receipt_format_date_today,       
							'booking_reference' => $service_filenumber,      
							'pax_name' => $holder_name.' '.$holder_last_name,          
							'agency_ref' => $agency_reference,      
							'hotel_name' => $hotel_name,      
							'hotel_address' => $hotel_address,      
							'check_in' => $checkindate,      
							'check_out' => $checkoutdate,      
							'room_type' => $hotel_room_room_type,  
							'units' => $room_count.' x ',        
							'board' => $hotel_room_board,      
							'board_type' => $hotel_room_board,      
							'hotel_category' => $category_code,      
							'reference_number' => $reference_number,      
							'latitude' =>  $hotel_latitude,      
							'longitude' =>  $hotel_longitude,      
							'adults' => $customer_list_adult_count,   
							'children_ages' => $customer_list_child_age_array,   
							'children' => $customer_list_child_count,   
							'hotel_telephone' => $hotel_contact_number,   
							'hotel_fax' => $hotel_fax_number,   
							'hotel_email' => $hotel_email,   
							'service_ref' => $service_filenumber,
							'book_or_number' => $book_or_number,
							'book_invoice_number' => $book_invoice_number 
						);  
						 
						$file_location = $theme_dir.'/inc/class/pdf/booking-voucher-'.$incoming_office.'-'.$service_filenumber.'.pdf'; 
						$pdf_email = pdf_email_voucher( $bookings_data, $file_location );  
						 
						/* Email Send */  
						$wp_mail =  wp_mail( $cub_email, $email_subject, $message, $headers , $file_location );	  
					}
				}
			}
		}   
	}   
}

function resend_voucher_admin(){  

	global $wpdb; //This is used only if making any database queries  
	global $theme_dir; 
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 

	if( $id && $action == 'resend_voucher_admin' ){
		
		$result = get_records( $id );
		
		foreach( $result as $details ){
			
			$book_details_tblpref = 'bkd_';
			$book_tblpref = 'book_';
			
			$cub_first_name = get_value( $details, $book_tblpref.'first_name' );
			$cub_middle_name = get_value( $details, $book_tblpref.'middle_name' );
			$cub_last_name = get_value( $details, $book_tblpref.'last_name' );
			$cub_contact_no = get_value( $details, $book_tblpref.'contact_no' );
			$cub_email = get_value( $details, $book_tblpref.'email' );
			$cub_birthday = get_value( $details, $book_tblpref.'birthday' );
			$cub_country_code = get_value( $details, $book_tblpref.'country' );
			$cub_country = get_country_name( $cub_country_code );
			
			$room = get_value( $details,$book_details_tblpref.'room' );
			$room_count = get_value( $details,$book_details_tblpref.'room' );
			$adult = get_value( $details,$book_details_tblpref.'adult' );
			$adult_count = get_value( $details,$book_details_tblpref.'adult' );
			$child = get_value( $details,$book_details_tblpref.'child' );
			$child_count = get_value( $details,$book_details_tblpref.'child' );
			
			$file_number = get_value( $details, $book_tblpref.'file_number' );
			$incoming_office = get_value( $details, $book_details_tblpref.'incoming_office' );
			$agency_reference = get_value( $details, $book_details_tblpref.'agency_reference' );
			$book_date = get_value( $details, $book_tblpref.'created' ); 
			$book_date = datetime($book_date,'m/d/Y'); 
			 
			$cancellation_policy_time = get_value( $details,$book_details_tblpref.'cancellation_policy_time' );
			$cancellation_policy_date = get_value( $details,$book_details_tblpref.'cancellation_policy_date' );
			$cancellation_policy_amount = get_value( $details,$book_details_tblpref.'cancellation_policy_amount' );
			$cancellation_policy_amount_currency = get_value( $details,$book_details_tblpref.'cancellation_policy_amount_currency' );
		}
		 

		$purchaise_details = PurchaseXML::view_purchaise_details( $file_number, $incoming_office );
		if( !empty( $purchaise_details ) ){
			$xml = simplexml_load_string( $purchaise_details ); 
			if( !empty( $xml ) ){ 
				$purchase = $xml->Purchase;	 
				$services = $xml->Purchase->ServiceList->Service;  
				if( !empty( $purchase ) ){ 
					foreach( $purchase as $purchases => $tags ){   

						$holder_name_xml = $tags->Holder->Name;   
						$holder_name = ucfirst( strtolower( $holder_name_xml ) );
						$holder_last_name_xml = $tags->Holder->LastName;  
						$holder_last_name = ucfirst( strtolower( $holder_last_name_xml ) );  
						
						$purchase_file_number = $tags->Reference->FileNumber;  
						$purchase_incoming_office = $tags->Reference->IncomingOffice->attributes()->code;  
						$purchase_token = $tags->attributes()->purchaseToken;  
						$purchase_token_time_to_expiration = $tags->attributes()->timeToExpiration;  
						$service_type = $tags->ServiceList->Service->attributes()->type;  
						$service_filenumber = $tags->ServiceList->Service->Reference->FileNumber; 
						$service_spui = $tags->ServiceList->Service->attributes()->SPUI;  
						$currency_response = $tags->ServiceList->Service->Currency->attributes()->code;  
						$contract_comment = $tags->ServiceList->Service->ContractList->Contract->CommentList->Comment;
						$amount = $tags->ServiceList->Service->TotalAmount; 
						$total_amount = $tags->ServiceList->Service->TotalAmount; 
						$total_amount = Hotel::commission( $total_amount );
						$discountlist = $tags->ServiceList->Service->DiscountList;
						if( $discountlist ){
							$discount_description = $tags->ServiceList->Service->DiscountList->Price->Description; 
						}
						$hotel_code = $tags->ServiceList->Service->HotelInfo->Code;
						$hotel_name = $tags->ServiceList->Service->HotelInfo->Name;
						$hotel_category = $tags->ServiceList->Service->HotelInfo->Category;
						$destination_name = $tags->ServiceList->Service->HotelInfo->Destination->Name;
						$destination_zone = $tags->ServiceList->Service->HotelInfo->Destination->ZoneList->Zone;
						$category_code = $tags->ServiceList->Service->HotelInfo->Category;  
						$checkindate = $tags->ServiceList->Service->DateFrom->attributes()->date; 
						$checkindate = date( "Y-m-d", strtotime( $checkindate ) );
						$checkoutdate = $tags->ServiceList->Service->DateTo->attributes()->date;  
						$checkoutdate = date( "Y-m-d", strtotime( $checkoutdate ) ); 
						$customer_list = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->GuestList->Customer; 
						$customer_list_adult_count = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->AdultCount; 
						$customer_list_child_count = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->ChildCount; 
				 
						$nights = (strtotime($checkoutdate) - strtotime($checkindate)) / 86400; 
						if( $customer_list ){ 
							foreach( $customer_list as $customer => $row ){  
								$customer_list_type_array[] = $row->attributes()->type;
								$customer_list_istravel_agent_array[] = $row->attributes()->isTravelAgent;
								$customer_list_customer_id_array[] = $row->CustomerId;
								
								if( $row->Age <= 18 ){
									$customer_list_child_age_array[] = $row->Age; 
								} else {
									$customer_list_adult_age_array[] = $row->Age; 
								} 
							}   
						}   
						
						$total_amount_nocommission = $tags->ServiceList->Service->TotalAmount; 
						$total_amount_nocommission = (float)$total_amount_nocommission;   
						$total_amount_nocommission = number_format( $total_amount_nocommission,2,'.',',' );  
						   
					} 
					
					$tax_value = Hotel::value_added_tax( $total_amount_nocommission );
					$service_charge = Hotel::service_charge( $total_amount );
					$web_admin_fee = Hotel::web_admin_fee( $total_amount );		

					$final_price = $total_amount;
					$final_total = $total_amount + $service_charge + $tax_value;
					
					if( $hotel_code ){ 
						$hotel_details_from_xml = BookHotelXML::get_hotels_details( $hotel_code );
						if( !empty(  $hotel_details_from_xml ) ){ 
							$hotel_details_xml = simplexml_load_string( $hotel_details_from_xml );  
							if( !empty(  $hotel_details_xml ) ){ 
								$hotel_contact = $hotel_details_xml->Hotel->Contact; 
								$hotel_position = $hotel_details_xml->Hotel->Position;
								if( !empty( $hotel_contact ) ){ 
									foreach( $hotel_contact as $contact => $c_list ){
										$hotel_email = $c_list->EmailList->Email;	
										$hotel_contact_number = $c_list->PhoneList->ContactNumber;	
										$hotel_fax_number = $c_list->FaxList->ContactNumber;	
										$hotel_website = $c_list->WebList->Web;	 
									}
								}
								if( !empty( $hotel_position ) ){ 
									foreach( $hotel_position as $position => $p_list ){	
										$hotel_latitude = $p_list->attributes()->latitude;   
										$hotel_longitude = $p_list->attributes()->longitude;  
									}		 
								}		 
							}		 
						}
						
						if( !empty( $destination_name ) && !empty( $destination_zone ) ){
							$destination = $destination_name.' '.$destination_zone;
						} elseif( !empty( $destination_name ) && empty( $destination_zone ) ){
							$destination = $destination_name;
						} else {
							$destination = $destination_zone;
						}  
						
						if( $room_count > 1 ){
							$room_count .= ' rooms';
						} else {
							$room_count .= ' room';
						}
						
						if( $adult_count > 1 ){
							$adult_count .= ' adults';
						} else {
							$adult_count .= ' adult';
						}
						
						if( $child_count > 0){
							if( $child_count > 1 ){
							$child_count .= ' children';
							} else {
								$child_count .= ' child';
							}
						} else {
							$child_count = Null;
						}
						
						$summary_occupancy = $room_count.' '.$adult_count.' '.$child_count;  
						$reference_number = $purchase_incoming_office.'-'.$purchase_file_number; 
			 
						
						// $plugin_dir_url = plugin_dir_url( __FILE__ );  
						$check_in = date( 'F d, Y', strtotime( $checkindate ) );
						$check_out = date( 'F d, Y', strtotime( $checkoutdate ) );
						$cancellation_date = date( 'F d, Y', strtotime( $cancellation_policy_date ) );
						$booker_name =  $cub_first_name.' '.$cub_last_name;
						  
						//cribs and trips admin booking email
						$settings_booking_email = pods('bookingemailsetting');
						//one
						$settings_booking_email_booking_one = $settings_booking_email->field( 'email_no_1' );		
						$settings_booking_cc_email_one = $settings_booking_email->field( 'cc_no_1' );		
						$settings_booking_bcc_email_one= $settings_booking_email->field( 'bcc_no_1' );
						// two
						$settings_booking_email_booking_two = $settings_booking_email->field( 'email_no_2' );		
						$settings_booking_cc_email_two = $settings_booking_email->field( 'cc_no_2' );		
						$settings_booking_bcc_email_two = $settings_booking_email->field( 'bcc_no_2' );
		  
						// cribs and trips admin setting 
						$settings_general = pods('admin');   
						$admin_email= $settings_general->field( 'email' );
						$admin_company_name= $settings_general->field( 'name' );
						$admin_system_description= $settings_general->field( 'description' );
						$admin_country= $settings_general->field( 'country' );
						$admin_address= $settings_general->field( 'address' );
						$admin_postcode= $settings_general->field( 'postcode' );
						$admin_fax_number= $settings_general->field( 'fax_number' );
						$admin_phone_number= $settings_general->field( 'phone_number' );
						$admin_website= $settings_general->field( 'website' );
						$admin_emergency_number= $settings_general->field( 'emergency_number' );
						$admin_tin_no= $settings_general->field( 'tin_no' );
						$admin_tax_reg_no= $settings_general->field( 'service_tax_reg_no' );
								 
						// $timezone = date_default_timezone_set( "Asia/Singapore" );
						$timezone = date_default_timezone_set( "Asia/Manila" );
						$date_today = date('M d, Y H:i:s'); 
						$format_date_today = date( 'F d, Y', strtotime( $date_today ) );
						$receipt_format_date_today = date( 'm/d/Y', strtotime( $date_today ) );
						$time_format = date( "g:i A", time( $date_today ) );    
						$timestamp = strtotime( $date_today ); 
						$day = date( 'l', $timestamp );
						$final_date = $day.', '.$format_date_today.' at '.$time_format;
						$final_price = (float)$final_price;
						$final_price = number_format( $final_price,2,'.',',' );  
						$total_net_amount = (float)$total_net_amount;
						$total_net_amount = number_format( $total_net_amount,2,'.',',' );      
						$cancellation_policy_time = iwp_show_local_time( $cancellation_policy_time );
						
						/* Email Message */
						$message_params =  array(   
							'admin_email' => $admin_email,      
							'admin_company_name' => $admin_company_name,      
							'admin_address' => $admin_address,      
							'admin_postcode' => $admin_postcode,      
							'admin_country' => $admin_country,      
							'admin_phone_number' => $admin_phone_number,      
							'admin_fax_number' => $admin_fax_number,      
							'admin_website' => $admin_website,      
							'final_date' => $final_date,      
							'reference_number' => $reference_number,      
							'agency_reference' => $agency_reference,   
							'booker_email' => $cub_email,      
							'booker_name' => $booker_name,      
							'booker_contact_no' => $cub_contact_no,      
							'booker_country' => $cub_country,         
							'service_description' => $hotel_name,         
							'cancellation_date' => $cancellation_date,         
							'summary_occupancy' => $summary_occupancy,         
							'check_in' => $check_in,         
							'check_out' => $check_out,         
							'check_out' => $check_out,         
						 
						);    
						$message = NotificationEmail::general_message( $message_params );
						  
						/* Email Receiver */  
						$email_list = array( $settings_booking_email_booking_one, $settings_booking_email_booking_two );
						$bcc_email_list = array( $settings_booking_bcc_email_one, $settings_booking_bcc_email_two ); 
						$cc_email_list = array( $settings_booking_cc_email_one, $settings_booking_cc_email_two ); 
						
						/* Email Subject */  
						if( !empty( $admin_company_name ) && !empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = $admin_company_name.' Admin Hotel Booking Notification for '.$cub_first_name.' (Trans #:'.$id.')';
						} elseif( empty( $admin_company_name ) && !empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = 'Admin Hotel Booking Notification for '.$cub_first_name.' (Trans #:'.$id.')';
						} elseif( !empty( $admin_company_name ) && empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = $admin_company_name.' Admin Hotel Booking Notification for (Trans #:'.$id.')';
						} elseif( !empty( $admin_company_name ) && !empty( $cub_first_name )&& empty( $id ) ){
							$email_subject = $admin_company_name.' Admin Hotel Booking Notification for '.$cub_first_name;
						} elseif( empty( $admin_company_name ) && empty( $cub_first_name )&& empty( $id ) ){
							$email_subject = 'Admin Hotel Booking Notification';
						} else {
							$email_subject = 'Admin Hotel Booking Notification';
						}
						 
						/* Email Sender */  
						$headers[] = 'Content-Type: text/html';
						$headers[] = 'charset=UTF-8';
						$headers[] = 'From: '.$admin_company_name.' <'.$admin_email.'>';
						
						if( $settings_booking_cc_email_one ){
							$headers[] = 'Cc: '.$settings_booking_cc_email_one; // 
						}
						if( $settings_booking_cc_email_two ){
							$headers[] = 'Cc: '.$settings_booking_cc_email_two; // 
						}
						if( $settings_booking_bcc_email_one ){
							$headers[] = 'Bcc: '.$settings_booking_bcc_email_one; // 
						}
						if( $settings_booking_bcc_email_two ){
							$headers[] = 'Bcc: '.$settings_booking_bcc_email_two; // 
						}
						 
						/* Email Attachment */  
						$bookings_data =  array( 	 
							'hotel_availableroom' => $services,       
							'booking_date' => $receipt_format_date_today,       
							'booking_reference' => $service_filenumber,      
							'pax_name' => $holder_name.' '.$holder_last_name,          
							'agency_ref' => $agency_reference,      
							'hotel_name' => $hotel_name,      
							'hotel_address' => $hotel_address,      
							'check_in' => $checkindate,      
							'check_out' => $checkoutdate,      
							'room_type' => $hotel_room_room_type,  
							'units' => $room_count.' x ',        
							'board' => $hotel_room_board,      
							'board_type' => $hotel_room_board,      
							'hotel_category' => $category_code,      
							'reference_number' => $reference_number,      
							'latitude' =>  $hotel_latitude,      
							'longitude' =>  $hotel_longitude,      
							'adults' => $customer_list_adult_count,   
							'children_ages' => $customer_list_child_age_array,   
							'children' => $customer_list_child_count,   
							'hotel_telephone' => $hotel_contact_number,   
							'hotel_fax' => $hotel_fax_number,   
							'hotel_email' => $hotel_email,   
							'service_ref' => $service_filenumber,
							'book_or_number' => $book_or_number,
							'book_invoice_number' => $book_invoice_number 
						);   
						$file_location = $theme_dir.'/inc/class/pdf/booking-voucher-'.$incoming_office.'-'.$service_filenumber.'.pdf'; 
						$pdf_email = pdf_email_voucher( $bookings_data, $file_location );  
						 
						/* Email Send */   
						$wp_mail =  wp_mail( $email_list, $email_subject, $message, $headers , $file_location ); 
					}
				}
			}
		}   
	}   
} 

function resend_receipt_client(){  

	global $wpdb; //This is used only if making any database queries  
	global $theme_dir; 
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 

	if( $id && $action == 'resend_receipt_client' ){
		
		$result = get_records( $id );
		
		foreach( $result as $details ){
			
			$book_details_tblpref = 'bkd_';
			$book_tblpref = 'book_';
			 
			$book_or_number = get_value( $details, $book_tblpref.'or_number' );
			$book_invoice_number = get_value( $details, $book_tblpref.'invoice_number' );
			
			$cub_first_name = get_value( $details, $book_tblpref.'first_name' );
			$cub_middle_name = get_value( $details, $book_tblpref.'middle_name' );
			$cub_last_name = get_value( $details, $book_tblpref.'last_name' );
			$cub_contact_no = get_value( $details, $book_tblpref.'contact_no' );
			$cub_email = get_value( $details, $book_tblpref.'email' );
			$cub_birthday = get_value( $details, $book_tblpref.'birthday' );
			$cub_country_code = get_value( $details, $book_tblpref.'country' );
			$cub_country = get_country_name( $cub_country_code );
			
			$room = get_value( $details,$book_details_tblpref.'room' );
			$room_count = get_value( $details,$book_details_tblpref.'room' );
			$adult = get_value( $details,$book_details_tblpref.'adult' );
			$adult_count = get_value( $details,$book_details_tblpref.'adult' );
			$child = get_value( $details,$book_details_tblpref.'child' );
			$child_count = get_value( $details,$book_details_tblpref.'child' );
			
			$file_number = get_value( $details, $book_tblpref.'file_number' );
			$incoming_office = get_value( $details, $book_details_tblpref.'incoming_office' );
			$agency_reference = get_value( $details, $book_details_tblpref.'agency_reference' );
			$book_date = get_value( $details, $book_tblpref.'created' ); 
			$book_date = datetime($book_date,'m/d/Y'); 
			 
			$cancellation_policy_time = get_value( $details,$book_details_tblpref.'cancellation_policy_time' );
			$cancellation_policy_date = get_value( $details,$book_details_tblpref.'cancellation_policy_date' );
			$cancellation_policy_amount = get_value( $details,$book_details_tblpref.'cancellation_policy_amount' );
			$cancellation_policy_amount_currency = get_value( $details,$book_details_tblpref.'cancellation_policy_amount_currency' );
 
		}
		 

		$purchaise_details = PurchaseXML::view_purchaise_details( $file_number, $incoming_office );
		if( !empty( $purchaise_details ) ){
			$xml = simplexml_load_string( $purchaise_details ); 
			if( !empty( $xml ) ){ 
				$purchase = $xml->Purchase;	 
				$services = $xml->Purchase->ServiceList->Service;  
				if( !empty( $purchase ) ){ 
					foreach( $purchase as $purchases => $tags ){   

						$holder_name_xml = $tags->Holder->Name;   
						$holder_name = ucfirst( strtolower( $holder_name_xml ) );
						$holder_last_name_xml = $tags->Holder->LastName;  
						$holder_last_name = ucfirst( strtolower( $holder_last_name_xml ) );  
						
						$purchase_file_number = $tags->Reference->FileNumber;  
						$purchase_incoming_office = $tags->Reference->IncomingOffice->attributes()->code;  
						$purchase_token = $tags->attributes()->purchaseToken;  
						$purchase_token_time_to_expiration = $tags->attributes()->timeToExpiration;  
						$service_type = $tags->ServiceList->Service->attributes()->type;  
						$service_filenumber = $tags->ServiceList->Service->Reference->FileNumber; 
						$service_spui = $tags->ServiceList->Service->attributes()->SPUI;  
						$currency_response = $tags->ServiceList->Service->Currency->attributes()->code;  
						$contract_comment = $tags->ServiceList->Service->ContractList->Contract->CommentList->Comment;
						$amount = $tags->ServiceList->Service->TotalAmount; 
						$total_amount = $tags->ServiceList->Service->TotalAmount; 
						$total_amount = Hotel::commission( $total_amount );
						$discountlist = $tags->ServiceList->Service->DiscountList;
						if( $discountlist ){
							$discount_description = $tags->ServiceList->Service->DiscountList->Price->Description; 
						}
						$hotel_code = $tags->ServiceList->Service->HotelInfo->Code;
						$hotel_name = $tags->ServiceList->Service->HotelInfo->Name;
						$hotel_category = $tags->ServiceList->Service->HotelInfo->Category;
						$destination_name = $tags->ServiceList->Service->HotelInfo->Destination->Name;
						$destination_zone = $tags->ServiceList->Service->HotelInfo->Destination->ZoneList->Zone;
						$category_code = $tags->ServiceList->Service->HotelInfo->Category;  
						$checkindate = $tags->ServiceList->Service->DateFrom->attributes()->date; 
						$checkindate = date( "Y-m-d", strtotime( $checkindate ) );
						$checkoutdate = $tags->ServiceList->Service->DateTo->attributes()->date;  
						$checkoutdate = date( "Y-m-d", strtotime( $checkoutdate ) ); 
						$customer_list = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->GuestList->Customer; 
						$customer_list_adult_count = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->AdultCount; 
						$customer_list_child_count = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->ChildCount; 
				 
						$nights = (strtotime($checkoutdate) - strtotime($checkindate)) / 86400; 
						if( $customer_list ){ 
							foreach( $customer_list as $customer => $row ){  
								$customer_list_type_array[] = $row->attributes()->type;
								$customer_list_istravel_agent_array[] = $row->attributes()->isTravelAgent;
								$customer_list_customer_id_array[] = $row->CustomerId;
								
								if( $row->Age <= 18 ){
									$customer_list_child_age_array[] = $row->Age; 
								} else {
									$customer_list_adult_age_array[] = $row->Age; 
								} 
							}   
						}   
						
						$total_amount_nocommission = $tags->ServiceList->Service->TotalAmount; 
						$total_amount_nocommission = (float)$total_amount_nocommission;   
						$total_amount_nocommission = number_format( $total_amount_nocommission,2,'.',',' );  
						   
					} 
					
					$tax_value = Hotel::value_added_tax( $total_amount_nocommission );
					$service_charge = Hotel::service_charge( $total_amount );
					$web_admin_fee = Hotel::web_admin_fee( $total_amount );		

					$final_price = $total_amount;
					$final_total = $total_amount + $service_charge + $tax_value;
					
					if( $hotel_code ){ 
						$hotel_details_from_xml = BookHotelXML::get_hotels_details( $hotel_code );
						if( !empty(  $hotel_details_from_xml ) ){ 
							$hotel_details_xml = simplexml_load_string( $hotel_details_from_xml );  
							if( !empty(  $hotel_details_xml ) ){ 
								$hotel_contact = $hotel_details_xml->Hotel->Contact; 
								$hotel_position = $hotel_details_xml->Hotel->Position;
								if( !empty( $hotel_contact ) ){ 
									foreach( $hotel_contact as $contact => $c_list ){
										$hotel_email = $c_list->EmailList->Email;	
										$hotel_contact_number = $c_list->PhoneList->ContactNumber;	
										$hotel_fax_number = $c_list->FaxList->ContactNumber;	
										$hotel_website = $c_list->WebList->Web;	 
									}
								}
								if( !empty( $hotel_position ) ){ 
									foreach( $hotel_position as $position => $p_list ){	
										$hotel_latitude = $p_list->attributes()->latitude;   
										$hotel_longitude = $p_list->attributes()->longitude;  
									}		 
								}		 
							}		 
						}
						
						if( !empty( $destination_name ) && !empty( $destination_zone ) ){
							$destination = $destination_name.' '.$destination_zone;
						} elseif( !empty( $destination_name ) && empty( $destination_zone ) ){
							$destination = $destination_name;
						} else {
							$destination = $destination_zone;
						}  
						
						if( $room_count > 1 ){
							$room_count .= ' rooms';
						} else {
							$room_count .= ' room';
						}
						
						if( $adult_count > 1 ){
							$adult_count .= ' adults';
						} else {
							$adult_count .= ' adult';
						}
						
						if( $child_count > 0){
							if( $child_count > 1 ){
							$child_count .= ' children';
							} else {
								$child_count .= ' child';
							}
						} else {
							$child_count = Null;
						}
						
						$summary_occupancy = $room_count.' '.$adult_count.' '.$child_count;  
						$reference_number = $purchase_incoming_office.'-'.$purchase_file_number; 
			  
						// $plugin_dir_url = plugin_dir_url( __FILE__ );  
						$check_in = date( 'F d, Y', strtotime( $checkindate ) );
						$check_out = date( 'F d, Y', strtotime( $checkoutdate ) );
						$cancellation_date = date( 'F d, Y', strtotime( $cancellation_policy_date ) );
						$booker_name =  $cub_first_name.' '.$cub_last_name;
						  
						//cribs and trips admin booking email
						$settings_booking_email = pods('bookingemailsetting');
						//one
						$settings_booking_email_booking_one = $settings_booking_email->field( 'email_no_1' );		
						$settings_booking_cc_email_one = $settings_booking_email->field( 'cc_no_1' );		
						$settings_booking_bcc_email_one= $settings_booking_email->field( 'bcc_no_1' );
						// two
						$settings_booking_email_booking_two = $settings_booking_email->field( 'email_no_2' );		
						$settings_booking_cc_email_two = $settings_booking_email->field( 'cc_no_2' );		
						$settings_booking_bcc_email_two = $settings_booking_email->field( 'bcc_no_2' );
		  
						// cribs and trips admin setting 
						$settings_general = pods('admin');   
						$admin_email= $settings_general->field( 'email' );
						$admin_company_name= $settings_general->field( 'name' );
						$admin_system_description= $settings_general->field( 'description' );
						$admin_country= $settings_general->field( 'country' );
						$admin_address= $settings_general->field( 'address' );
						$admin_postcode= $settings_general->field( 'postcode' );
						$admin_fax_number= $settings_general->field( 'fax_number' );
						$admin_phone_number= $settings_general->field( 'phone_number' );
						$admin_website= $settings_general->field( 'website' );
						$admin_emergency_number= $settings_general->field( 'emergency_number' );
						$admin_tin_no= $settings_general->field( 'tin_no' );
						$admin_tax_reg_no= $settings_general->field( 'service_tax_reg_no' );
								 
						// $timezone = date_default_timezone_set( "Asia/Singapore" );
						$timezone = date_default_timezone_set( "Asia/Manila" );
						$date_today = date('M d, Y H:i:s'); 
						$format_date_today = date( 'F d, Y', strtotime( $date_today ) );
						$receipt_format_date_today = date( 'm/d/Y', strtotime( $date_today ) );
						$time_format = date( "g:i A", time( $date_today ) );    
						$timestamp = strtotime( $date_today ); 
						$day = date( 'l', $timestamp );
						$final_date = $day.', '.$format_date_today.' at '.$time_format;
						$final_price = (float)$final_price;
						$final_price = number_format( $final_price,2,'.',',' );  
						$total_net_amount = (float)$total_net_amount;
						$total_net_amount = number_format( $total_net_amount,2,'.',',' );     
						$cancellation_policy_time = iwp_show_local_time( $cancellation_policy_time );
						
						/* Email Message */
						$message_params =  array(   
							'admin_email' => $admin_email,      
							'admin_company_name' => $admin_company_name,      
							'admin_address' => $admin_address,      
							'admin_postcode' => $admin_postcode,      
							'admin_country' => $admin_country,      
							'admin_phone_number' => $admin_phone_number,      
							'admin_fax_number' => $admin_fax_number,      
							'admin_website' => $admin_website,      
							'final_date' => $final_date,      
							'reference_number' => $reference_number,      
							'agency_reference' => $agency_reference,   
							'booker_email' => $cub_email,      
							'booker_name' => $booker_name,      
							'booker_contact_no' => $cub_contact_no,      
							'booker_country' => $cub_country,         
							'service_description' => $hotel_name,         
							'cancellation_date' => $cancellation_date,         
							'summary_occupancy' => $summary_occupancy,         
							'check_in' => $check_in,         
							'check_out' => $check_out,         
							'check_out' => $check_out,         
						 
						);    
						$message = NotificationEmail::general_message( $message_params );
						 
						/* Email Receiver */  
						$email_list = array( $settings_booking_email_booking_one, $settings_booking_email_booking_two );
						$bcc_email_list = array( $settings_booking_bcc_email_one, $settings_booking_bcc_email_two ); 
						$cc_email_list = array( $settings_booking_cc_email_one, $settings_booking_cc_email_two ); 
				    
						/* Email Subject */  
						if( !empty( $admin_company_name ) && !empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = $admin_company_name.'Hotel Booking Notification for '.$cub_first_name.' (Trans #:'.$id.')';
						} elseif( empty( $admin_company_name ) && !empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = 'Hotel Booking Notification for '.$cub_first_name.' (Trans #:'.$id.')';
						} elseif( !empty( $admin_company_name ) && empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = $admin_company_name.'Hotel Booking Notification for (Trans #:'.$id.')';
						} elseif( !empty( $admin_company_name ) && !empty( $cub_first_name )&& empty( $id ) ){
							$email_subject = $admin_company_name.'Hotel Booking Notification for '.$cub_first_name;
						} elseif( empty( $admin_company_name ) && empty( $cub_first_name )&& empty( $id ) ){
							$email_subject = 'Hotel Booking Notification';
						} else {
							$email_subject = 'Hotel Booking Notification';
						}
						 
						/* Email Sender */  
						$headers[] = 'Content-Type: text/html';
						$headers[] = 'charset=UTF-8';
						$headers[] = 'From: '.$admin_company_name.' <'.$admin_email.'>';
						
						if( $settings_booking_cc_email_one ){
							$headers[] = 'Cc: '.$settings_booking_cc_email_one; // 
						}
						if( $settings_booking_cc_email_two ){
							$headers[] = 'Cc: '.$settings_booking_cc_email_two; // 
						}
						if( $settings_booking_bcc_email_one ){
							$headers[] = 'Bcc: '.$settings_booking_bcc_email_one; // 
						}
						if( $settings_booking_bcc_email_two ){
							$headers[] = 'Bcc: '.$settings_booking_bcc_email_two; // 
						}
						  
						/* Email Attachment */  
						$receipts_details_data =  array(   
							'hotel_availableroom' => $services,      
							'date' => $receipt_format_date_today,      
							'left_company_image' => $company_image,  
							'left_company_name' => $company_name,  
							'left_company_address' => $company_address,  
							'right_company_image' => $admin_image,   
							'right_company_name' => $admin_company_name,   
							'right_company_address' => $admin_address ,      
							'booking_reference' => $service_filenumber,      
							'pax_name' => $holder_name.' '.$holder_last_name,      
							'reference' => $agency_reference,      
							'booking_confirmation_date' => $receipt_format_date_today,      
							'payment_type' => 'Pay at agency',      
							'hotel_name' => $hotel_name,      
							'hotel_address' => $hotel_address,      
							'check_in' => $checkindate,      
							'check_out' => $checkoutdate,      
							'room_type' => $hotel_room_room_type.' x '.$room_count,        
							'board' => $hotel_room_board,      
							'occupancy' => $summary_occupancy,   
							'price' => $final_price,    
							'contract_remarks' => $contract_remarks,      
							'total_price' => $currency_response.' '.$total_amount,    
							'concept' => $hotel_name.' ('.$hotel_room_room_type.') x '.$room_count,      
							'concept_from' => $cancellation_date,      
							'cancellation_policy_time' => $cancellation_policy_time,      
							'units' => $room_count,      
							'value' => $currency_response.' '.$cancellation_policy_amount,
							'total_net_amount' => $final_total, 
							'service_charge' => $service_charge, 
							'tax_value' => $tax_value,
							'web_admin_fee' => $web_admin_fee,
							'book_or_number' => $book_or_number,
							'book_invoice_number' => $book_invoice_number 			
						);     
	     
						$file_location_receipt = $theme_dir.'/inc/class/pdf/booking-receipt-'.$incoming_office.'-'.$booking_reference.'.pdf'; 
						$pdf_email = pdf_email_receipt( $receipts_details_data, $file_location_receipt );  
						 
						/* Email Send */  
						$wp_mail =  wp_mail( $cub_email, $email_subject, $message, $headers, $file_location_receipt );	
					 
					}
				}
			}
		}   
	}   
}

function resend_invoice_admin(){  

	global $wpdb; //This is used only if making any database queries  
	global $theme_dir; 
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 

	if( $id && $action == 'resend_invoice_admin' ){
		
		$result = get_records( $id );
		    
		foreach( $result as $details ){
			
			$book_details_tblpref = 'bkd_';
			$book_tblpref = 'book_';
			
			$book_or_number = get_value( $details, $book_tblpref.'or_number' );
			$book_invoice_number = get_value( $details, $book_tblpref.'invoice_number' );
			
			$cub_first_name = get_value( $details, $book_tblpref.'first_name' );
			$cub_middle_name = get_value( $details, $book_tblpref.'middle_name' );
			$cub_last_name = get_value( $details, $book_tblpref.'last_name' );
			$cub_contact_no = get_value( $details, $book_tblpref.'contact_no' );
			$cub_email = get_value( $details, $book_tblpref.'email' );
			$cub_birthday = get_value( $details, $book_tblpref.'birthday' );
			$cub_country_code = get_value( $details, $book_tblpref.'country' );
			$cub_country = get_country_name( $cub_country_code );
			
			$room = get_value( $details,$book_details_tblpref.'room' );
			$room_count = get_value( $details,$book_details_tblpref.'room' );
			$adult = get_value( $details,$book_details_tblpref.'adult' );
			$adult_count = get_value( $details,$book_details_tblpref.'adult' );
			$child = get_value( $details,$book_details_tblpref.'child' );
			$child_count = get_value( $details,$book_details_tblpref.'child' );
			
			$file_number = get_value( $details, $book_tblpref.'file_number' );
			$incoming_office = get_value( $details, $book_details_tblpref.'incoming_office' );
			$agency_reference = get_value( $details, $book_details_tblpref.'agency_reference' );
			$book_date = get_value( $details, $book_tblpref.'created' ); 
			$book_date = datetime($book_date,'m/d/Y'); 
			 
			$cancellation_policy_time = get_value( $details,$book_details_tblpref.'cancellation_policy_time' );
			$cancellation_policy_date = get_value( $details,$book_details_tblpref.'cancellation_policy_date' );
			$cancellation_policy_amount = get_value( $details,$book_details_tblpref.'cancellation_policy_amount' );
			$cancellation_policy_amount_currency = get_value( $details,$book_details_tblpref.'cancellation_policy_amount_currency' );
		}
		 

		$purchaise_details = PurchaseXML::view_purchaise_details( $file_number, $incoming_office );
		if( !empty( $purchaise_details ) ){
			$xml = simplexml_load_string( $purchaise_details ); 
			if( !empty( $xml ) ){ 
				$purchase = $xml->Purchase;	 
				$services = $xml->Purchase->ServiceList->Service;  
				if( !empty( $purchase ) ){ 
					foreach( $purchase as $purchases => $tags ){   

						$holder_name_xml = $tags->Holder->Name;   
						$holder_name = ucfirst( strtolower( $holder_name_xml ) );
						$holder_last_name_xml = $tags->Holder->LastName;  
						$holder_last_name = ucfirst( strtolower( $holder_last_name_xml ) );  
						
						$purchase_file_number = $tags->Reference->FileNumber;  
						$purchase_incoming_office = $tags->Reference->IncomingOffice->attributes()->code;  
						$purchase_token = $tags->attributes()->purchaseToken;  
						$purchase_token_time_to_expiration = $tags->attributes()->timeToExpiration;  
						$service_type = $tags->ServiceList->Service->attributes()->type;  
						$service_filenumber = $tags->ServiceList->Service->Reference->FileNumber; 
						$service_spui = $tags->ServiceList->Service->attributes()->SPUI;  
						$currency_response = $tags->ServiceList->Service->Currency->attributes()->code;  
						$contract_comment = $tags->ServiceList->Service->ContractList->Contract->CommentList->Comment;
						$amount = $tags->ServiceList->Service->TotalAmount; 
						$total_amount = $tags->ServiceList->Service->TotalAmount; 
						$total_amount = Hotel::commission( $total_amount );
						$discountlist = $tags->ServiceList->Service->DiscountList;
						if( $discountlist ){
							$discount_description = $tags->ServiceList->Service->DiscountList->Price->Description; 
						}
						$hotel_code = $tags->ServiceList->Service->HotelInfo->Code;
						$hotel_name = $tags->ServiceList->Service->HotelInfo->Name;
						$hotel_category = $tags->ServiceList->Service->HotelInfo->Category;
						$destination_name = $tags->ServiceList->Service->HotelInfo->Destination->Name;
						$destination_zone = $tags->ServiceList->Service->HotelInfo->Destination->ZoneList->Zone;
						$category_code = $tags->ServiceList->Service->HotelInfo->Category;  
						$checkindate = $tags->ServiceList->Service->DateFrom->attributes()->date; 
						$checkindate = date( "Y-m-d", strtotime( $checkindate ) );
						$checkoutdate = $tags->ServiceList->Service->DateTo->attributes()->date;  
						$checkoutdate = date( "Y-m-d", strtotime( $checkoutdate ) ); 
						$customer_list = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->GuestList->Customer; 
						$customer_list_adult_count = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->AdultCount; 
						$customer_list_child_count = $tags->ServiceList->Service->AvailableRoom->HotelOccupancy->Occupancy->ChildCount; 
				 
						$nights = (strtotime($checkoutdate) - strtotime($checkindate)) / 86400; 
						if( $customer_list ){ 
							foreach( $customer_list as $customer => $row ){  
								$customer_list_type_array[] = $row->attributes()->type;
								$customer_list_istravel_agent_array[] = $row->attributes()->isTravelAgent;
								$customer_list_customer_id_array[] = $row->CustomerId;
								
								if( $row->Age <= 18 ){
									$customer_list_child_age_array[] = $row->Age; 
								} else {
									$customer_list_adult_age_array[] = $row->Age; 
								} 
							}   
						}   
						
						$total_amount_nocommission = $tags->ServiceList->Service->TotalAmount; 
						$total_amount_nocommission = (float)$total_amount_nocommission;   
						$total_amount_nocommission = round( $total_amount_nocommission, 2 ); 
						 
						 
						   
					} 
					
					$tax_value = Hotel::value_added_tax( $total_amount_nocommission );
					$service_charge = Hotel::service_charge( $total_amount );
					$web_admin_fee = Hotel::web_admin_fee( $total_amount );		

					$final_price = $total_amount;
					$final_total = $total_amount + $service_charge + $tax_value;
					$final_total = round( $final_total, 2 ); 
					if( $hotel_code ){ 
						$hotel_details_from_xml = BookHotelXML::get_hotels_details( $hotel_code );
						if( !empty(  $hotel_details_from_xml ) ){ 
							$hotel_details_xml = simplexml_load_string( $hotel_details_from_xml );  
							if( !empty(  $hotel_details_xml ) ){ 
								$hotel_contact = $hotel_details_xml->Hotel->Contact; 
								$hotel_position = $hotel_details_xml->Hotel->Position;
								if( !empty( $hotel_contact ) ){ 
									foreach( $hotel_contact as $contact => $c_list ){
										$hotel_email = $c_list->EmailList->Email;	
										$hotel_contact_number = $c_list->PhoneList->ContactNumber;	
										$hotel_fax_number = $c_list->FaxList->ContactNumber;	
										$hotel_website = $c_list->WebList->Web;	 
									}
								}
								if( !empty( $hotel_position ) ){ 
									foreach( $hotel_position as $position => $p_list ){	
										$hotel_latitude = $p_list->attributes()->latitude;   
										$hotel_longitude = $p_list->attributes()->longitude;  
									}		 
								}		 
							}		 
						}
						
						if( !empty( $destination_name ) && !empty( $destination_zone ) ){
							$destination = $destination_name.' '.$destination_zone;
						} elseif( !empty( $destination_name ) && empty( $destination_zone ) ){
							$destination = $destination_name;
						} else {
							$destination = $destination_zone;
						}  
						
						if( $room_count > 1 ){
							$room_count .= ' rooms';
						} else {
							$room_count .= ' room';
						}
						
						if( $adult_count > 1 ){
							$adult_count .= ' adults';
						} else {
							$adult_count .= ' adult';
						}
						
						if( $child_count > 0){
							if( $child_count > 1 ){
							$child_count .= ' children';
							} else {
								$child_count .= ' child';
							}
						} else {
							$child_count = Null;
						}
						
						$summary_occupancy = $room_count.' '.$adult_count.' '.$child_count;  
						$reference_number = $purchase_incoming_office.'-'.$purchase_file_number; 
			 
						
						// $plugin_dir_url = plugin_dir_url( __FILE__ );  
						$check_in = date( 'F d, Y', strtotime( $checkindate ) );
						$check_out = date( 'F d, Y', strtotime( $checkoutdate ) );
						$cancellation_date = date( 'F d, Y', strtotime( $cancellation_policy_date ) );
						$booker_name =  $cub_first_name.' '.$cub_last_name;
						  
						//cribs and trips admin booking email
						$settings_booking_email = pods('bookingemailsetting');
						//one
						$settings_booking_email_booking_one = $settings_booking_email->field( 'email_no_1' );		
						$settings_booking_cc_email_one = $settings_booking_email->field( 'cc_no_1' );		
						$settings_booking_bcc_email_one= $settings_booking_email->field( 'bcc_no_1' );
						// two
						$settings_booking_email_booking_two = $settings_booking_email->field( 'email_no_2' );		
						$settings_booking_cc_email_two = $settings_booking_email->field( 'cc_no_2' );		
						$settings_booking_bcc_email_two = $settings_booking_email->field( 'bcc_no_2' );
		  
						// cribs and trips admin setting 
						$settings_general = pods('admin');   
						$admin_email= $settings_general->field( 'email' );
						$admin_company_name= $settings_general->field( 'name' );
						$admin_system_description= $settings_general->field( 'description' );
						$admin_country= $settings_general->field( 'country' );
						$admin_address= $settings_general->field( 'address' );
						$admin_postcode= $settings_general->field( 'postcode' );
						$admin_fax_number= $settings_general->field( 'fax_number' );
						$admin_phone_number= $settings_general->field( 'phone_number' );
						$admin_website= $settings_general->field( 'website' );
						$admin_emergency_number= $settings_general->field( 'emergency_number' );
						$admin_tin_no= $settings_general->field( 'tin_no' );
						$admin_tax_reg_no= $settings_general->field( 'service_tax_reg_no' );
								 
						// $timezone = date_default_timezone_set( "Asia/Singapore" );
						$timezone = date_default_timezone_set( "Asia/Manila" );
						$date_today = date('M d, Y H:i:s'); 
						$format_date_today = date( 'F d, Y', strtotime( $date_today ) );
						$receipt_format_date_today = date( 'm/d/Y', strtotime( $date_today ) );
						$time_format = date( "g:i A", time( $date_today ) );    
						$timestamp = strtotime( $date_today ); 
						$day = date( 'l', $timestamp );
						$final_date = $day.', '.$format_date_today.' at '.$time_format;
						$final_price = (float)$final_price;
						$final_price = number_format( $final_price,2,'.',',' );  
						$total_net_amount = (float)$total_net_amount;
						$total_net_amount = number_format( $total_net_amount,2,'.',',' );     
						$cancellation_policy_time = iwp_show_local_time( $cancellation_policy_time );
						
						/* Email Message */
						$message_params =  array(   
							'admin_email' => $admin_email,      
							'admin_company_name' => $admin_company_name,      
							'admin_address' => $admin_address,      
							'admin_postcode' => $admin_postcode,      
							'admin_country' => $admin_country,      
							'admin_phone_number' => $admin_phone_number,      
							'admin_fax_number' => $admin_fax_number,      
							'admin_website' => $admin_website,      
							'final_date' => $final_date,      
							'reference_number' => $reference_number,      
							'agency_reference' => $agency_reference,   
							'booker_email' => $cub_email,      
							'booker_name' => $booker_name,      
							'booker_contact_no' => $cub_contact_no,      
							'booker_country' => $cub_country,         
							'service_description' => $hotel_name,         
							'cancellation_date' => $cancellation_date,         
							'summary_occupancy' => $summary_occupancy,         
							'check_in' => $check_in,         
							'check_out' => $check_out,         
							'check_out' => $check_out,         
						 
						);    
						$message = NotificationEmail::general_message( $message_params );
						 
						/* Email Receiver */
						$email_list = array( $settings_booking_email_booking_one, $settings_booking_email_booking_two );
						$bcc_email_list = array( $settings_booking_bcc_email_one, $settings_booking_bcc_email_two ); 
						$cc_email_list = array( $settings_booking_cc_email_one, $settings_booking_cc_email_two ); 
						
						/* Email Subject */
						if( !empty( $admin_company_name ) && !empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = $admin_company_name.' Admin Hotel Booking Notification for '.$cub_first_name.' (Trans #:'.$id.')';
						} elseif( empty( $admin_company_name ) && !empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = 'Admin Hotel Booking Notification for '.$cub_first_name.' (Trans #:'.$id.')';
						} elseif( !empty( $admin_company_name ) && empty( $cub_first_name )&& !empty( $id ) ){
							$email_subject = $admin_company_name.' Admin Hotel Booking Notification for (Trans #:'.$id.')';
						} elseif( !empty( $admin_company_name ) && !empty( $cub_first_name )&& empty( $id ) ){
							$email_subject = $admin_company_name.' Admin Hotel Booking Notification for '.$cub_first_name;
						} elseif( empty( $admin_company_name ) && empty( $cub_first_name )&& empty( $id ) ){
							$email_subject = 'Admin Hotel Booking Notification';
						} else {
							$email_subject = 'Admin Hotel Booking Notification';
						}
						 
						/* Email Sender */
						$headers[] = 'Content-Type: text/html';
						$headers[] = 'charset=UTF-8';
						$headers[] = 'From: '.$admin_company_name.' <'.$admin_email.'>';
						
						if( $settings_booking_cc_email_one ){
							$headers[] = 'Cc: '.$settings_booking_cc_email_one; // 
						}
						if( $settings_booking_cc_email_two ){
							$headers[] = 'Cc: '.$settings_booking_cc_email_two; // 
						}
						if( $settings_booking_bcc_email_one ){
							$headers[] = 'Bcc: '.$settings_booking_bcc_email_one; // 
						}
						if( $settings_booking_bcc_email_two ){
							$headers[] = 'Bcc: '.$settings_booking_bcc_email_two; // 
						}
						 
						/* Email Attachment */  
						$invoice_details_data =  array(   
							'hotel_availableroom' => $services,      
							'date' => $receipt_format_date_today,      
							'left_company_image' => $company_image,  
							'left_company_name' => $company_name,  
							'left_company_address' => $company_address,  
							'right_company_image' => $admin_image,   
							'right_company_name' => $admin_company_name,   
							'right_company_address' => $admin_address ,      
							'booking_reference' => $service_filenumber,      
							'pax_name' => $holder_name.' '.$holder_last_name,      
							'reference' => $agency_reference,      
							'booking_confirmation_date' => $receipt_format_date_today,      
							'payment_type' => 'Pay at agency',      
							'hotel_name' => $hotel_name,      
							'hotel_address' => $hotel_address,      
							'check_in' => $checkindate,      
							'check_out' => $checkoutdate,      
							'room_type' => $hotel_room_room_type.' x '.$room_count,        
							'board' => $hotel_room_board,      
							'occupancy' => $summary_occupancy,   
							'price' => $final_price,    
							'contract_remarks' => $contract_remarks,      
							'total_price' => $currency_response.' '.$total_amount,    
							'concept' => $hotel_name.' ('.$hotel_room_room_type.') x '.$room_count,      
							'concept_from' => $cancellation_date,      
							'cancellation_policy_time' => $cancellation_policy_time,      
							'units' => $room_count,      
							'value' => $currency_response.' '.$cancellation_policy_amount,
							'total_net_amount' => $final_total, 
							'service_charge' => $service_charge, 
							'tax_value' => $tax_value,
							'web_admin_fee' => $web_admin_fee,
							'book_or_number' => $book_or_number,
							'book_invoice_number' => $book_invoice_number 			
						);  
						
						$file_location_invoice = $theme_dir.'/inc/class/pdf/booking-invoice-'.$incoming_office.'-'.$booking_reference.'.pdf';
						$pdf_email_invoice = pdf_email_invoice( $invoice_details_data, $file_location_invoice );
						  
						/* Email Send */
						$wp_mail =  wp_mail( $email_list, $email_subject, $message, $headers, $file_location_invoice );	 
					}
				}
			}
		}   
	}   
}


function pdf_email_voucher( $booking_params, $file_location ){ 
		 
	extract( $booking_params );
	$customer_age = array();
	
	if( $hotel_availableroom ){ 	 
		foreach( $hotel_availableroom as $service => $tags ){ 
			$supplier = $tags->Supplier; 
			foreach( $supplier as $supply => $giver ){  
				$supplier_name = $giver->attributes()->name; 
				$supplier_vatnumber = $giver->attributes()->vatNumber; 
			}
		}
	
	}  
	if(  !empty( $supplier_name ) && !empty( $supplier_vatnumber ) ){
		$bookable_details = 'Payable through '.$supplier_name.', acting as agent for the service operating company, details of which can be provided upon request". VAT:'.$supplier_vatnumber.' Reference: '.$reference_number;
	} else {
		$bookable_details = 'Bookable and Payable by J-Leisure';
	} 
	ob_start();	 
	if( $hotel_availableroom ){ 	 
		foreach( $hotel_availableroom as $service => $tags ){ 
											
			$availableroom = $tags->AvailableRoom; 
	 
			if( $availableroom ){ 
				foreach( $availableroom as $available => $avail ){  
					$customer_age_display = ''; 
					$hotel_room_board = $avail->HotelRoom->Board; 
					$hotel_room_room_type = $avail->HotelRoom->RoomType; 
				 
					$room_count = $avail->HotelOccupancy->RoomCount; 
					$adult_count = $avail->HotelOccupancy->Occupancy->AdultCount; 
					$child_count = $avail->HotelOccupancy->Occupancy->ChildCount; 
			 
					$guestlist = $avail->HotelOccupancy->Occupancy->GuestList;   
					$customer_availableroom = $guestlist->Customer;			
					if( $customer_availableroom ){ 
						foreach( $customer_availableroom as $customer_avail => $customer ){  
							 
							$customer_type = $customer->attributes()->type; 
							 
							if( $customer_type == 'CH' ){
								if( isset( $customer->Age ) ){ 
									$customer_age[] = $customer->Age; 
								}
							}
						}
					}
					 
					
					if( $child_count ){
						if( $customer_age ){ 
							$customer_list_adult_age_array_count = count( $customer_age );  
							$customer_list_adult_age_array_count = (float)$customer_list_adult_age_array_count;							
						 
							$new_child_count = $child_count - 1;
							$customer_list_adult_age_array_count = $customer_list_adult_age_array_count - 1;
							
							for($cs=0; $cs<=$new_child_count; $cs++){ 
								if( $cs == $customer_list_adult_age_array_count ){ 
									if( isset( $customer_age[$cs] ) ){
										$customer_age_display .= $customer_age[$cs];
									} 
								} else {
									if( isset( $customer_age[$cs] ) ){
										$customer_age_display .= $customer_age[$cs].', ';
									}
									
								} 
							} 
						} 
					}   
					
					?>
					<tr>
						<td style="border-bottom: 1px dashed #ccc;"><?php echo $room_count.' x '; ?></td>
						<td style="border-bottom: 1px dashed #ccc;"><?php echo $hotel_room_room_type; ?></td>
						<td style="border-bottom: 1px dashed #ccc;text-align:center;"><?php echo $adult_count; ?></td>
						<td style="border-bottom: 1px dashed #ccc;text-align:center;"><?php echo $child_count; ?></td> 
						<td style="border-bottom: 1px dashed #ccc;text-align:center;"><?php echo $customer_age_display; ?></td> 
						<td style="border-bottom: 1px dashed #ccc;"><?php echo $hotel_room_board; ?></td>   
					</tr>
					<?php 
					 $customer_age = ''; 
				}
			}
		}
	}
	$availableroom = ob_get_contents();
	ob_end_clean();
	 
			
	$logo_right = "http://cribsandtrips.com/wp-content/uploads/2015/08/logo.png";		 
	$hotel_category = iwp_get_hotel_rating( $hotel_category );  
	$google_map = '<img src="https://maps.google.com/maps/api/staticmap?center='.$latitude.','.$longitude.'&amp;zoom=11&amp;size=350x165&markers=size:mid" />'; 
	$settings_general = pods('admin');   
	$admin_emergency_number = $settings_general->field( 'emergency_number' );
	 
	$voucher_parms =  array(   
		'logo_right' => $logo_right,      
		'reference_number' => $reference_number,      
		'hotel_name' => $hotel_name,      
		'hotel_category' => $hotel_category,      
		'hotel_address' => $hotel_address,      
		'hotel_telephone' => $hotel_telephone,      
		'hotel_fax' => $hotel_fax,      
		'hotel_email' => $hotel_email,      
		'pax_name' => $pax_name,      
		'booking_date' => $booking_date,      
		'agency_ref' => $agency_ref,      
		'check_in' => $check_in,      
		'check_out' => $check_out,      
		'hotel_availableroom' => $availableroom,      
		'google_map' => $google_map,       
		'service_ref' => $service_ref,       
		'admin_emergency_number' => $admin_emergency_number,       
		'bookable_details' => $bookable_details 
	);      
	$html = PDFAttachment::voucher_template( $voucher_parms );
	 
	// Load library  
	$dompdf = new DOMPDF();
	$dompdf->set_paper('legal', 'portrait');
	
	// Convert to PDF
	$htmlpdf = $dompdf->load_html($html);  
	$dompdf->render(); 

	$pdf = $dompdf->output();    
	file_put_contents($file_location,$pdf);  
	
	return $file_location;  
} 
	
function pdf_email_invoice( $booking_params,$file_location ){ 
		 
	extract( $booking_params ); 
	$customer_age = array();
	$customer_age_display = ''; 
	if( empty($children)){
		$children = 0;
	}
	if( empty($children_ages)){
		$children_ages = 0; 
	} 
	
	ob_start();	 
	$customer_list_adult_age_array_count = count( $children_ages );  
	$customer_list_adult_age_array_count = $customer_list_adult_age_array_count - 1;
	for($cs=0; $cs<=$customer_list_adult_age_array_count; $cs++){ 
		if( $cs == $customer_list_adult_age_array_count ){
			echo $children_ages[$cs];
		} else {
			echo $children_ages[$cs].', ';
		} 
	} 
	$children_ages_content = ob_get_contents();
	ob_end_clean();
	
	
	
	 
	$invoice_no = 'IN'.$book_invoice_number; 
	$logo_left = "http://cribsandtrips.com/wp-content/uploads/2015/08/logo.png";
	  
	$receipt_params =  array(    
		'invoice_no' => $invoice_no,  
		'hotel_availableroom' => $availableroom,  
		'date' => $date,  
		'logo_left_name' => $left_company_name,  
		'logo_left_address' => $left_company_address,  
		'logo_left' => $logo_left,  
		'logo_right_name' => $right_company_name,  
		'logo_right_address' => $right_company_address,  
		'booking_reference' => $booking_reference,  
		'booking_confirmation_date' => $booking_confirmation_date,  
		'pax_name' => $pax_name,  
		'payment_type' => $payment_type,  
		'agency_ref' => $reference,  
		'hotel_name' => $hotel_name,  
		'hotel_address' => $hotel_address,  
		'check_in' => $check_in,  
		'check_out' => $check_out,  
		'room_type' => $room_type,  
		'board' => $board,  
		'occupancy' => $occupancy,  
		'price' => $price,  
		'contract_remarks' => $contract_remarks,  
		'total_price' => $total_price,  
		'room_price_total' => $hotel_room_room_price_total,  
		'tour_package' => '',  
		'tour_from_date' => '',  
		'tour_to_date' => '',  
		'ticket_details' => '',  
		'ticket_price' => '',  
		'total_ticket_price' => '',  
		'arrival_hotel_type' => '',   
		'pickup_date' => '',  
		'arrival_time' => '',  
		'arrival_transfer' => '',  
		'arrival_price' => '',  
		'island_price' => '',   
		'departure_hotel_type' => '',  
		'departure_pickup_date' => '',  
		'departure_arrival_time' => '',   
		'departure_arrival_transfer' => '',  
		'departure_arrival_price' => '',  
		'departure_island_price' => '',  
		'total_departure_price' => '',   
		'total_net_amount' => $total_net_amount,      
		'service_charge' => $service_charge,      
		'tax_value' => $tax_value,      
		'web_admin_fee' => $web_admin_fee,    
		'cancellation_concept' => $concept,       
		'cancellation_concept_from' => $concept_from,       
		'cancellation_policy_time' => $cancellation_policy_time,       
		'cancellation_units' => $units,       
		'cancellation_value' => $value 
	);      
	$html = PDFAttachment::invoice_template( $receipt_params );
	
	// Load all views as normal 
	// Get output html
	// $html = $this->load->view('template/email-template-invoice',$data, true);  
	 
	// Load library  
	$dompdf = new DOMPDF();
	$dompdf->set_paper('legal', 'portrait');
	
	// Convert to PDF
	$htmlpdf = $dompdf->load_html($html);  
	$dompdf->render(); 

	$pdf = $dompdf->output();    
	file_put_contents($file_location,$pdf);  
	
	return $file_location;
}

function pdf_email_receipt( $booking_params,$file_location ){ 
		 
	extract( $booking_params ); 
	$customer_age = array();
	$customer_age_display = '';
	$hotel_room_room_price_total = '';
	ob_start();	   
	
	$customer_list_adult_age_array_count = count( $children_ages );  
	$customer_list_adult_age_array_count = $customer_list_adult_age_array_count - 1;
	for($cs=0; $cs<=$customer_list_adult_age_array_count; $cs++){ 
		if( $cs == $customer_list_adult_age_array_count ){
			echo $children_ages[$cs];
		} else {
			echo $children_ages[$cs].', ';
		} 
	} 
	$children_ages_content = ob_get_contents();
	ob_end_clean();
	
	ob_start();	 
	if( $hotel_availableroom ){ 	 
		foreach( $hotel_availableroom as $service => $tags ){ 
											
			$availableroom = $tags->AvailableRoom; 
	 
			if( $availableroom ){
					
				foreach( $availableroom as $available => $avail ){ 
					 
					
					$hotel_room_board = $avail->HotelRoom->Board; 
					$hotel_room_room_type = $avail->HotelRoom->RoomType; 
					$hotel_room_room_price = $avail->HotelRoom->Price->Amount; 
				 
					$room_count = $avail->HotelOccupancy->RoomCount; 
					$adult_count = $avail->HotelOccupancy->Occupancy->AdultCount; 
					$child_count = $avail->HotelOccupancy->Occupancy->ChildCount; 
			 
					$guestlist = $avail->HotelOccupancy->Occupancy->GuestList;   
					$customer_availableroom = $guestlist->Customer;			
					foreach( $customer_availableroom as $customer_avail => $customer ){  
						 
						$customer_type = $customer->attributes()->type; 
						 
						if( $customer_type == 'CH' ){
							if( isset( $customer->Age ) ){ 
								$customer_age[] = $customer->Age; 
							}
						}
					}
					  
					
					if( $child_count ){
						if( $customer_age ){ 
							$customer_list_adult_age_array_count = count( $customer_age );  
							$customer_list_adult_age_array_count = (float)$customer_list_adult_age_array_count;							
							$customer_list_adult_age_array_count = $customer_list_adult_age_array_count - 1;
							$new_child_count = $child_count - 1;
							$customer_age_display = '';
							for($cs=0; $cs<=$new_child_count; $cs++){ 
								if( $cs == $customer_list_adult_age_array_count ){ 
									if( isset( $customer_age[$cs] ) ){
										$customer_age_display .= $customer_age[$cs];
									} 
								} else {
									if( isset( $customer_age[$cs] ) ){
										$customer_age_display .= $customer_age[$cs].', ';
									}
									
								} 
							} 
						} 
					} 
					if( $adult_count > 1 ){
						$adult_count .= ' adults';
						if( $child_count >= 1){
							$adult_count .= ', ';
						}
					} else {
						$adult_count .= ' adult';
						if( $child_count >= 1){
							$adult_count .= ', ';
						}
					}
					
					
					if( $child_count > 0){
						if( $child_count > 1 ){
						$child_count .= ' children';
						} else {
							$child_count .= ' child';
						}
					} else {
						$child_count = Null;
					}
					
					if( $customer_age_display ){ 
						if( $child_count >  1 ){ 
							$customer_age_display = ' ( '.$customer_age_display.' children ages)';
						} elseif( $child_count <= 1 ){ 
							$customer_age_display = ' ( '.$customer_age_display.' child age)';
						}
					}   
					
					$occupancy = $adult_count.$child_count.$customer_age_display;
					$hotel_room_room_price = Hotel::commission( $hotel_room_room_price ); 
					$hotel_room_room_price = (float)$hotel_room_room_price;
				 
					
					?>
					<tr>
						<td style="border-bottom: 1px dashed #ccc; width: 180px;"><?php echo $hotel_room_room_type; ?></td>
						<td style="border-bottom: 1px dashed #ccc; width: 150px;"><?php echo $hotel_room_board; ?></td>
						<td style="border-bottom: 1px dashed #ccc; width: 150px;"><?php echo $occupancy; ?></td>
						<td style="border-bottom: 1px dashed #ccc; width: 150px; text-align: right; padding-right: 10px;"><?php echo number_format( $hotel_room_room_price,2,'.',',' ); ?></td>
					</tr> 
					<?php  
					$hotel_room_room_price_total += $hotel_room_room_price; 
					$customer_age = '';
					$adult_count = '';
					$child_count = '';
					$customer_age_display = '';
				}
			}
		}
	}
	$availableroom = ob_get_contents();
	ob_end_clean();
	  
	$receipt_no = 'OR'.$book_or_number;   
    $logo_left = "http://cribsandtrips.com/wp-content/uploads/2015/08/logo.png";
	
	$receipt_params =  array(    
		'receipt_no' => $receipt_no,  
		'hotel_availableroom' => $availableroom,  
		'date' => $date,  
		'logo_left_name' => $left_company_name,  
		'logo_left_address' => $left_company_address,  
		'logo_left' => $logo_left,  
		'logo_right_name' => $right_company_name,  
		'logo_right_address' => $right_company_address,  
		'booking_reference' => $booking_reference,  
		'booking_confirmation_date' => $booking_confirmation_date,  
		'pax_name' => $pax_name,  
		'payment_type' => $payment_type,  
		'agency_ref' => $reference,  
		'hotel_name' => $hotel_name,  
		'hotel_address' => $hotel_address,  
		'check_in' => $check_in,  
		'check_out' => $check_out,  
		'room_type' => $room_type,  
		'board' => $board,  
		'occupancy' => $occupancy,  
		'price' => $price,  
		'contract_remarks' => $contract_remarks,  
		'total_price' => $total_price,  
		'room_price_total' => $hotel_room_room_price_total,  
		'tour_package' => '',  
		'tour_from_date' => '',  
		'tour_to_date' => '',  
		'ticket_details' => '',  
		'ticket_price' => '',  
		'total_ticket_price' => '',  
		'arrival_hotel_type' => '',   
		'pickup_date' => '',  
		'arrival_time' => '',  
		'arrival_transfer' => '',  
		'arrival_price' => '',  
		'island_price' => '',   
		'departure_hotel_type' => '',  
		'departure_pickup_date' => '',  
		'departure_arrival_time' => '',   
		'departure_arrival_transfer' => '',  
		'departure_arrival_price' => '',  
		'departure_island_price' => '',  
		'total_departure_price' => '',   
		'total_net_amount' => $total_net_amount,      
		'service_charge' => $service_charge,      
		'tax_value' => $tax_value,      
		'web_admin_fee' => $web_admin_fee,    
		'cancellation_concept' => $concept,       
		'cancellation_concept_from' => $concept_from,       
		'cancellation_policy_time' => $cancellation_policy_time,       
		'cancellation_units' => $units,       
		'cancellation_value' => $value 
	);      
	$html = PDFAttachment::receipt_template( $receipt_params );
	
	// Load library  
	$dompdf = new DOMPDF();
	$dompdf->set_paper('legal', 'portrait');
	
	// Convert to PDF
	$htmlpdf = $dompdf->load_html($html);  
	$dompdf->render(); 

	$pdf = $dompdf->output();    
	file_put_contents($file_location,$pdf);  
	
	return $file_location;
}
 
function iwp_months_year_dropdown(){ 
	global $wpdb, $wp_locale;  
	$selected_year = isset($_GET['year'])?$_GET['year']:0; 
	$selected_month = isset($_GET['month'])?$_GET['month']:0; 
		
	/* $months = $wpdb->get_results( "
		SELECT DISTINCT YEAR( book_created ) AS year, MONTH( book_created ) AS month
		FROM wp_cribsandtrips_booking 
		ORDER BY book_created DESC GROUP BY book_created" ); */
		
	$months = $wpdb->get_results( "SELECT DISTINCT MONTH( book_created ) AS month FROM wp_cribsandtrips_booking GROUP BY book_created ORDER BY book_created ASC;" );
	
	$year = $wpdb->get_results( "SELECT DISTINCT YEAR( book_created ) AS year FROM wp_cribsandtrips_booking GROUP BY book_created ORDER BY book_created DESC;" ); 
	
	$month_count = count( $months );
 
	if ( !$month_count ){ 
		return;
	}
	$m = isset( $_GET['m'] ) ? (int) $_GET['m'] : 0;
	?>
	<label for="filter-by-date" class="screen-reader-text"><?php _e( 'Filter by date' ); ?></label>
	<select name="month" id="filter-by-month">
		<option<?php selected( $m, 0 ); ?> value="0"><?php _e( 'All Months' ); ?></option>
		<?php
        foreach ( $months as $arc_row ) { 
            $month = zeroise( $arc_row->month, 2 ); 
			$month_name = $wp_locale->get_month( $month ); 
			if( $selected_month == $month ){
				echo '<option value='.$month.' selected>'.$month_name.'</option>'; 
			} else {
				echo '<option value='.$month.'>'.$month_name.'</option>'; 
			}
			
        }
		?>
	</select>
	<select name="year" id="filter-by-year">
		<option<?php selected( $m, 0 ); ?> value="0"><?php _e( 'All Year' ); ?></option>
		<?php
		$year_counter = '';
        foreach( $year as $ary_row ){ 
			$year = esc_attr( $ary_row->year );
			if( $year_counter != $year ){
				if( $selected_year == $year ){	
					echo '<option value='.$year.' selected>'.$year.'</option>';
				} else {
					echo '<option value='.$year.'>'.$year.'</option>';
				}
				$year_counter = $year;
			} 
        }
		?>
	</select>    
	<?php 
}

function iwp_date_selection(){
	global $wpdb, $wp_locale; 
	$selected_start_date = $_GET['start_date']; 
	$selected_end_date = $_GET['end_date'];  
	?> 
	<input type="date" id="start_date" name="start_date" class="example-datepicker" value="<?php echo $selected_start_date;?>" Placeholder="Select Start Date" style="width: 130px;"/>  
	<input type="date" id="end_date" name="end_date" class="example-datepicker" value="<?php echo $selected_end_date;?>" Placeholder="Select End Date" style="width: 130px;" />   
	<?php 
}
 
function iwp_hotel_bookings_render_income_graph(){
  
	global $wpdb; //This is used only if making any database queries  
	
	$page = $_REQUEST['page']; 
	  
	$year_today = date('Y'); 
	$status_type = $_GET['status_type'];  
	$month = $_GET['month'];  
	$filter_months_year = $_GET['filter-months-year-graph-submit'];  
	$filter_date = $_GET['filter-date-graph-submit'];  
	$get_year = $_GET['year'];
	$year = isset( $_GET['year'] )?$_GET['year']:$year_today;
		
	$start_date = $_GET['start_date'];  
	$end_date = $_GET['end_date'];  
	$s_month = $_GET['s_month'];  
	 
	$month_data = array();
	$data = array();
	$title_text = '';
	
	
	if( $month && $year && $filter_months_year == 'Filter'){
		 
		$sql_month = "SELECT DAY(LAST_DAY(`book_created`)) as day_count"
			." FROM `wp_cribsandtrips_booking`"  
			." WHERE"  
			." `book_published`='1' "    
			." AND MONTH( `book_created`) = '".$month."' " 
			." AND YEAR( `book_created` ) = '".$year."' "; 
			;
			
		$month_count_result = $wpdb->get_row( $sql_month ); 
		$day_count = get_value( $month_count_result, 'day_count' );
		 
		for($i=1;$i<=$day_count;$i++){ 
			$x_display = $month.'/'.$i.'/'.$year; 
			$month_data[] = "'".$i."'";
			$x_display = date( "Y-m-d", strtotime( $x_display ) ); 
		  
			
			$sql_day = "SELECT sum(`b`.`bkd_total_amount`) as total_amount"
			." FROM `wp_cribsandtrips_booking` AS `a`"  
			." LEFT JOIN `wp_cribsandtrips_booking_details` AS `b`"
			." ON `a`.`book_id`=`b`.`book_id`" 
			." LEFT JOIN `wp_cribsandtrips_hotels` AS `c`"
			." ON `b`.`HotelCode`=`c`.`HotelCode`" 
			." WHERE"  
			." `a`.`book_published`='1'"     
			." AND DATE( `a`.`book_created` )='".$x_display."'"    
			; 	
			$day_count_result = $wpdb->get_results( $sql_day ); 
			
			if( $day_count_result ){
				foreach( $day_count_result as $value ){ 
					$month_number = $value->month;
					$total_amount = $value->total_amount; 
					$data[] = round($total_amount, 2);  
				} 
			} 
		}
		if( $month == '1' ){
			$month_name = 'Jan';
		} elseif( $month == '2' ){
			$month_name = 'Feb';
		} elseif( $month == '3' ){ 
			$month_name = 'Mar';
		} elseif( $month == '4' ){
			$month_name = 'Apr';
		} elseif( $month == '5' ){
			$month_name = 'May';
		} elseif( $month == '6' ){
			$month_name = 'Jun';
		} elseif( $month == '7' ){
			$month_name = 'Jul';
		} elseif( $month == '8' ){
			$month_name = 'Aug';
		} elseif( $month == '9' ){
			$month_name = 'Sep';
		} elseif( $month == '10' ){
			$month_name = 'Oct';
		} elseif( $month == '11' ){
			$month_name = 'Nov';
		} elseif( $month == '12' ){
			$month_name = 'Dec';
		}
		
		$title_text = 'Monthly Booking Total Income for '.$month_name.' '.$year;
		
	} elseif( $start_date && $end_date && $filter_date == 'Filter'){ 
	
		$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
		$end_date = date( "Y-m-d", strtotime( $end_date ) );
		
		$interval = new DateInterval('P1D'); 
		$realEnd = new DateTime($end_date);
		$realEnd->add($interval);

		$period = new DatePeriod(
			 new DateTime($start_date),
			 $interval,
			 $realEnd
		);
		if( $period ){ 
			foreach($period as $date) { 
				$x_display = $date->format('Y-m-d'); 
				$x_display_day = $date->format('d'); 
				$month_data[] = "'".$x_display_day."'";
				$x_display = date( "Y-m-d", strtotime( $x_display ) ); 
			  
				
				$sql_day = "SELECT sum(`b`.`bkd_total_amount`) as total_amount"
				." FROM `wp_cribsandtrips_booking` AS `a`"  
				." LEFT JOIN `wp_cribsandtrips_booking_details` AS `b`"
				." ON `a`.`book_id`=`b`.`book_id`" 
				." LEFT JOIN `wp_cribsandtrips_hotels` AS `c`"
				." ON `b`.`HotelCode`=`c`.`HotelCode`" 
				." WHERE"  
				." `a`.`book_published`='1'"     
				." AND DATE( `a`.`book_created` )='".$x_display."'"    
				; 	
				$day_count_result = $wpdb->get_results( $sql_day ); 
				
				if( $day_count_result ){
					foreach( $day_count_result as $value ){ 
						$month_number = $value->month;
						$total_amount = $value->total_amount; 
						$data[] = round($total_amount, 2);  
					} 
				} 
			}  
		}  
		$title_text = 'Monthly Booking Total Income From: '.$start_date.' To: '.$end_date;
    } else {
		$sql = "SELECT MONTH(`a`.`book_created`) month, sum(`b`.`bkd_total_amount`) as total_amount"
			." FROM `wp_cribsandtrips_booking` AS `a`"  
			." LEFT JOIN `wp_cribsandtrips_booking_details` AS `b`"
			." ON `a`.`book_id`=`b`.`book_id`" 
			." LEFT JOIN `wp_cribsandtrips_hotels` AS `c`"
			." ON `b`.`HotelCode`=`c`.`HotelCode`" 
			." WHERE"  
			." `a`.`book_published`='1'"    
			; 	
		 
		if( $month ){
			$sql .= " AND MONTH( `book_created`) = '".$month."' "; 
		}
		
		if( $year ){
			$sql .= " AND YEAR( `book_created` ) = '".$year."' "; 
		}
		
		if( $start_date && $end_date ){ 
			$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
			$end_date = date( "Y-m-d", strtotime( $end_date ) );  
			  
			$sql .= " AND ( DATE( `book_created` ) BETWEEN '".$start_date."' AND '".$end_date."')";    
		}
		 
		$sql .=" GROUP BY  MONTH( `book_created` )";
	 
		$result = $wpdb->get_results( $sql );
		 
		
		if( $result ){
			foreach( $result as $value ){ 
				$month_number = $value->month;
				$total_amount = $value->total_amount; 
				if( $month_number == '1' ){
					$month_name = 'Jan';
				} elseif( $month_number == '2' ){
					$month_name = 'Feb';
				} elseif( $month_number == '3' ){ 
					$month_name = 'Mar';
				} elseif( $month_number == '4' ){
					$month_name = 'Apr';
				} elseif( $month_number == '5' ){
					$month_name = 'May';
				} elseif( $month_number == '6' ){
					$month_name = 'Jun';
				} elseif( $month_number == '7' ){
					$month_name = 'Jul';
				} elseif( $month_number == '8' ){
					$month_name = 'Aug';
				} elseif( $month_number == '9' ){
					$month_name = 'Sep';
				} elseif( $month_number == '10' ){
					$month_name = 'Oct';
				} elseif( $month_number == '11' ){
					$month_name = 'Nov';
				} elseif( $month_number == '12' ){
					$month_name = 'Dec';
				}
				 
				$month_data[] = "'".$month_name."'"; 
				$data[] = round($total_amount, 2);  
			}
		}  
		$month_data_keys = array_keys($month_data);   
		$month_data_count = count( $month_data );

		$data_keys = array_keys($data);   
		$data_count = count( $data ); 
	}
	
	if( empty( $title_text )){
		if( $year ){
			$title_text = 'Monthly Booking Total Income for '.$year;
		} else {
			$title_text = 'Monthly Booking Total Income';
		}
		
	} 
	 
    ?> 
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script> 
	
	 
	<script type="text/javascript">
	jQuery(function () {
		jQuery('div#income_graph_container').highcharts({ 
			title: {
				text: '<?php echo $title_text; ?>',
				x: -20 //center
			}, 
			xAxis: {
				categories: [<?php echo join($month_data, ','); ?>]
			},
			yAxis: { 
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			}, 
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			}, 
			 tooltip: { 
                enabled: true,
                formatter: function() { 
                    return 'Amount Php: <b>'+ this.y.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +'</b>'; 
                }
            },
			series: [{
				name: 'Booking Amount',
				data: [<?php echo join($data, ','); ?>],
				 
			}] 
		});
	});
	</script>
    <div class="wrap"> 
		<div id="icon-users" class="icon32"><br/></div>
        <h2>Hotel Bookings Income Graph</h2>
        <ul class="subsubsub">  
			<li class="all"><a href="?page=iwp_hotel_bookings_list">Hotel Booking List</a></li>
			<li class="all">&nbsp;|&nbsp;<a href="?page=<?php echo $page;?>">Reset</a></li> 
		</ul> 
		<br /><br />
		<table>
			<tr>
				<td>
					<form id="income_month_year" method="get"> 
						<input type="hidden" value="<?php echo $page; ?>" name="page">
						<?php
						iwp_months_year_dropdown(); 
						?>
						<input type="submit" name="filter-months-year-graph-submit" class="button" value="Filter"/>
					</form>
				</td>
				<td>
					<form id="income_date_range" method="get"> 
						<input type="hidden" value="<?php echo $page; ?>" name="page">
						<?php 
						iwp_date_selection();
						?> 
						<input type="submit" name="filter-date-graph-submit" class="button" value="Filter"/>
					</form>  
				</td>
			</tr>
		</table> 
		<div id="income_graph_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
    <?php
}

function iwp_hotel_bookings_render_statistics_graph(){
  
	global $wpdb; //This is used only if making any database queries  
	$year_today = date('Y'); 
	$page = $_REQUEST['page'];  
	$status_type = $_GET['status_type'];  
	$filter_months_year = $_GET['filter-months-year-graph-submit'];  
	$filter_date = $_GET['filter-date-graph-submit'];  
	$month = $_GET['month'];   
	$get_year = $_GET['year'];
	$year = isset( $_GET['year'] )?$_GET['year']:$year_today;	
		
	$start_date = $_GET['start_date'];  
	$end_date = $_GET['end_date'];  
	
	$month_data = array();
	$data = array();
	$title_text = '';
	
	if( $month && $year && $filter_months_year == 'Filter'){ 
		$sql_month = "SELECT DAY(LAST_DAY(book_created)) as day_count"
			." FROM `wp_cribsandtrips_booking`"  
			." WHERE"  
			." `book_published`='1' "    
			." AND MONTH( `book_created`) = '".$month."' " 
			." AND YEAR( `book_created` ) = '".$year."' "; 
			;
			
		$month_count_result = $wpdb->get_row( $sql_month ); 
		$day_count = get_value( $month_count_result, 'day_count' );
		$day_count = (float)$day_count;
		for($i=1;$i<=$day_count;$i++){ 
			$x_display = $month.'/'.$i.'/'.$year;
			$month_data[] = "'".$i."'";
			$x_display = date( "Y-m-d", strtotime( $x_display ) );  
			$sql_day = "SELECT MONTH(`a`.`book_created`) month, COUNT(*) count"
				." FROM `wp_cribsandtrips_booking` AS `a`"  
				." LEFT JOIN `wp_cribsandtrips_booking_details` AS `b`"
				." ON `a`.`book_id`=`b`.`book_id`" 
				." LEFT JOIN `wp_cribsandtrips_hotels` AS `c`"
				." ON `b`.`HotelCode`=`c`.`HotelCode`" 
				." WHERE"  
				." `a`.`book_published`='1'"   
				." AND DATE( `a`.`book_created` )='".$x_display."'"    
				; 	
			$day_count_result = $wpdb->get_results( $sql_day ); 
			if( $day_count_result ){
				foreach( $day_count_result as $value ){  
					$data[] = $value->count; 
				} 
			}
			 
		}
		
		if( $month == '1' ){
			$month_name = 'Jan';
		} elseif( $month == '2' ){
			$month_name = 'Feb';
		} elseif( $month == '3' ){ 
			$month_name = 'Mar';
		} elseif( $month == '4' ){
			$month_name = 'Apr';
		} elseif( $month == '5' ){
			$month_name = 'May';
		} elseif( $month == '6' ){
			$month_name = 'Jun';
		} elseif( $month == '7' ){
			$month_name = 'Jul';
		} elseif( $month == '8' ){
			$month_name = 'Aug';
		} elseif( $month == '9' ){
			$month_name = 'Sep';
		} elseif( $month == '10' ){
			$month_name = 'Oct';
		} elseif( $month == '11' ){
			$month_name = 'Nov';
		} elseif( $month == '12' ){
			$month_name = 'Dec';
		}
		$title_text = 'Monthly Booking Average Count for '.$month_name.' '.$year;
	} elseif( $start_date && $end_date && $filter_date == 'Filter'){ 
	
		$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
		$end_date = date( "Y-m-d", strtotime( $end_date ) );
		
		$interval = new DateInterval('P1D'); 
		$realEnd = new DateTime($end_date);
		$realEnd->add($interval);

		$period = new DatePeriod(
			 new DateTime($start_date),
			 $interval,
			 $realEnd
		);
		if( $period ){ 
			foreach($period as $date) { 
				$x_display = $date->format('Y-m-d'); 
				$x_display_day = $date->format('d'); 
				$month_data[] = "'".$x_display_day."'";
				$x_display = date( "Y-m-d", strtotime( $x_display ) ); 
			  
				
				$sql_day = "SELECT MONTH(`a`.`book_created`) month, COUNT(*) count"
					." FROM `wp_cribsandtrips_booking` AS `a`"  
					." LEFT JOIN `wp_cribsandtrips_booking_details` AS `b`"
					." ON `a`.`book_id`=`b`.`book_id`" 
					." LEFT JOIN `wp_cribsandtrips_hotels` AS `c`"
					." ON `b`.`HotelCode`=`c`.`HotelCode`" 
					." WHERE"  
					." `a`.`book_published`='1'"   
				." AND DATE( `a`.`book_created` )='".$x_display."'"    
				; 	
				$day_count_result = $wpdb->get_results( $sql_day ); 
				 
				if( $day_count_result ){
					foreach( $day_count_result as $value ){  
						$data[]= $value->count; 
					} 
				} 
			}  
		}  
		$title_text = 'Monthly Booking Average Count From: '.$start_date.' To: '.$end_date;
	} else { 
		$sql = "SELECT MONTH(`a`.`book_created`) month, COUNT(*) count"
			." FROM `wp_cribsandtrips_booking` AS `a`"  
			." LEFT JOIN `wp_cribsandtrips_booking_details` AS `b`"
			." ON `a`.`book_id`=`b`.`book_id`" 
			." LEFT JOIN `wp_cribsandtrips_hotels` AS `c`"
			." ON `b`.`HotelCode`=`c`.`HotelCode`" 
			." WHERE"  
			." `a`.`book_published`='1'"   
		;
		  
		if( $month ){
			$sql .= " AND MONTH( `book_created`) = '".$month."' "; 
		}
		
		if( $year ){
			$sql .= " AND YEAR( `book_created` ) = '".$year."' "; 
		}
		
		if( $start_date && $end_date ){ 
			$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
			$end_date = date( "Y-m-d", strtotime( $end_date ) );  
			  
			$sql .= " AND ( DATE( `book_created` ) BETWEEN '".$start_date."' AND '".$end_date."')";   
			 
		}
		$sql .=" GROUP BY  MONTH( `book_created` )"; 
		$result = $wpdb->get_results( $sql );
		
		if( $result ){
			foreach( $result as $value ){ 
				$month_number = $value->month;
				$month_count = $value->count;  
				 
				if( $month_number == 1 ){
					$month_name = 'Jan';
				} elseif( $month_number == 2 ){
					$month_name = 'Feb';
				} elseif( $month_number == 3 ){ 
					$month_name = 'Mar';
				} elseif( $month_number == 4 ){
					$month_name = 'Apr';
				} elseif( $month_number == 5 ){
					$month_name = 'May';
				} elseif( $month_number == 6 ){
					$month_name = 'Jun';
				} elseif( $month_number == 7 ){
					$month_name = 'Jul';
				} elseif( $month_number == 8 ){
					$month_name = 'Aug';
				} elseif( $month_number == 9 ){
					$month_name = 'Sep';
				} elseif( $month_number == 10 ){
					$month_name = 'Oct';
				} elseif( $month_number == 11 ){
					$month_name = 'Nov';
				} elseif( $month_number == 12 ){
					$month_name = 'Dec';
				}
				 
				$month_data[] = "'".$month_name."'"; 
				$data[] = $month_count; 
			}
		}
	} 
	$month_data_keys = array_keys($month_data);   
	$month_data_count = count( $month_data );

	$data_keys = array_keys($data);   
	$data_count = count( $data ); 
	 
	if( empty( $title_text )){
		if( $year ){
			$title_text = 'Monthly Booking Average Count for '.$year;
		} else {
			$title_text = 'Monthly Booking Average Count';
		}
		
	} 
    ?> 
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script> 
	
	 
	<script type="text/javascript">
	jQuery(function () {
		jQuery('#statistics_graph_container').highcharts({ 
			title: {
				text: '<?php echo $title_text; ?>',
				x: -20 //center
			}, 
			xAxis: {
				categories: [<?php echo join( $month_data, ','); ?>]
			},
			yAxis: { 
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			}, 
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [{
				name: 'Booking',
				data: [<?php echo join($data, ','); ?>]
			} ]
		});
	});
	</script>
    <div class="wrap"> 
		<div id="icon-users" class="icon32"><br/></div>
        <h2>Hotel Bookings Statistics Graph</h2>
		<ul class="subsubsub">  
			<li class="all"><a href="?page=iwp_hotel_bookings_list">Hotel Booking List</a></li>
			<li class="all">&nbsp;|&nbsp;<a href="?page=<?php echo $page;?>">Reset</a></li> 
		</ul> 
		<br /><br />
		<table>
			<tr>
				<td>
					<form id="statistics_month_year" method="get"> 
						<input type="hidden" value="<?php echo $page; ?>" name="page">
						<?php
						iwp_months_year_dropdown(); 
						?>
						<input type="submit" name="filter-months-year-graph-submit" class="button" value="Filter"/>
					</form>
				</td>
				<td>
					<form id="statistics_date_range" method="get"> 
						<input type="hidden" value="<?php echo $page; ?>" name="page">
						<?php 
						iwp_date_selection();
						?> 
						<input type="submit" name="filter-date-graph-submit" class="button" value="Filter"/>
					</form> 
					
				</td>
			</tr>
		</table>
		<div id="statistics_graph_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
    <?php
}


function iwp_get_hotel_details( $hotel_code ){
	$apiKey = "dv7anwskyfw2m23ebbkjbz5r";
	$sharedSecret = "uaQfvy8rDc"; 
	$signature = hash("sha256", $apiKey.$sharedSecret.time());
	  
	$endpoint = 'https://api.hotelbeds.com/hotel-content-api/1.0/hotels/'.$hotel_code.'?language=ENG';
  
	$headerfields = array(
		'Api-Key:'.$apiKey, 
		'X-Signature:'.$signature, 
		'Accept:application/json',
		'Content-Type:application/json',
	); 

	$ch = curl_init($endpoint);
		assert(curl_setopt_array($ch,
			array(
				CURLOPT_AUTOREFERER => true,
				CURLOPT_BINARYTRANSFER => true,
				CURLOPT_COOKIESESSION => true,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_FORBID_REUSE => false,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false, 
				CURLOPT_HTTPHEADER=>$headerfields,  
				CURLOPT_CUSTOMREQUEST=>'GET',
				CURLOPT_HEADER=>false, 
				CURLINFO_HEADER_OUT=>false,    
		)));  
		 
	$response = curl_exec($ch);
	
	$decoded_curl_result = html_entity_decode( $response );
	$json_data = json_decode($decoded_curl_result, true);
	 
	curl_close($ch);   
	return $json_data;  
}

function iwp_get_boooking_information( $referrence_number ){ 	
	  
	global $wpdb;  
	global $theme_dir;   
	global $session; 
	global $bedsonline_username;
	global $bedsonline_password;
	$booking_api = new BookingAPI(); 
	
	
	
	// Your API Key and secret
	$apiKey = "dv7anwskyfw2m23ebbkjbz5r";
	$sharedSecret = "uaQfvy8rDc";

	// Signature is generated by SHA256 (Api-Key + Shared Secret + Timestamp (in seconds))
	$signature = hash("sha256", $apiKey.$sharedSecret.time());
	  
	$endpoint = "https://api.hotelbeds.com/hotel-api/1.0/bookings/$referrence_number";   
 
 
	$headerfields = array(
		'Api-Key:'.$apiKey, 
		'X-Signature:'.$signature, 
		'Accept:application/json',
		'Content-Type:application/json',
	);
	
	
	/* Referr https://developer.hotelbeds.com/io-docs for data structure */
	$fields = json_encode( $post_fields );

	$ch = curl_init($endpoint);
	assert(curl_setopt_array($ch,
		array(
			CURLOPT_AUTOREFERER => true,
			CURLOPT_BINARYTRANSFER => true,
			CURLOPT_COOKIESESSION => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_FORBID_REUSE => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false, 
			CURLOPT_HTTPHEADER=>$headerfields,  
			CURLOPT_CUSTOMREQUEST=>'GET',
			CURLOPT_HEADER=>false, 
			CURLINFO_HEADER_OUT=>false
	)));  
	 
	$response = curl_exec($ch);
	$decoded_curl_result = html_entity_decode( $response );
	$json_data = json_decode($decoded_curl_result, true);
	 
	curl_close($ch); 
	return $json_data;
	
}

function iwp_get_boooking_cancellation( $referrence_number ){ 	
	  
	global $wpdb;  
	global $theme_dir;   
	global $session; 
	global $bedsonline_username;
	global $bedsonline_password;
	$booking_api = new BookingAPI(); 
	
	
	
	// Your API Key and secret
	$apiKey = "dv7anwskyfw2m23ebbkjbz5r";
	$sharedSecret = "uaQfvy8rDc";

	// Signature is generated by SHA256 (Api-Key + Shared Secret + Timestamp (in seconds))
	$signature = hash("sha256", $apiKey.$sharedSecret.time());
	   
	$endpoint = 'https://api.hotelbeds.com/hotel-api/1.0/bookings/'.$referrence_number.'?cancellationFlag=CANCELLATION';    
	 
	$headerfields = array(
		'Api-Key:'.$apiKey, 
		'X-Signature:'.$signature, 
		'Accept:application/json',
		'Content-Type:application/json',
	);
	 

	$ch = curl_init($endpoint);
	assert(curl_setopt_array($ch,
		array(
			CURLOPT_AUTOREFERER => true,
			CURLOPT_BINARYTRANSFER => true,
			CURLOPT_COOKIESESSION => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_FORBID_REUSE => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false, 
			CURLOPT_HTTPHEADER=>$headerfields,  
			CURLOPT_CUSTOMREQUEST=>'DELETE',
			CURLOPT_HEADER=>false, 
			CURLINFO_HEADER_OUT=>false
	)));  
	 
	$response = curl_exec($ch);
	$decoded_curl_result = html_entity_decode( $response );
	$json_data = json_decode($decoded_curl_result, true); 
	curl_close($ch); 
	return $json_data;
	
}